Phonyx
======

Phonyx is a small and simple finite element code written in pure Python. The only dependency is numpy, with optional support for sparse matrixes from scipy and plotting with matplotlib if these are installed. Phonyx has the same Python programming API as FEniCS and the name reflects this as it is a play on phony (meaning not genuine) and FEniCS.

Phonyx supports:

* 1D and 2D domains (simplices only)
* Continuous (CG) and discontinuous (DG) Lagrangian elements of low order (constant, linear, quadratic)
* Lagrange multiplicators, mixed function spaces, vector function spaces, trace spaces
* VTK (*.vtu) and Paraview (*.pvd) output for CG/DG and trace spaces
* Normal CPython and PyPy (with numpypy)
* No parallelisation

The main goal of Phonyx is to enable experimentation with new FEM methods which will be (hopefully) easier in a small and simple code base such as Phonyx than it is in a large code base such as FEniCS.


Compatibility
-------------

It is a goal when developing Phonyx that programs can be tested in both Phonyx and FEniCS by substituting

.. code:: python

    from phonyx import *

with

.. code:: python

    from dolfin import *

in the problem definition Python-script. This makes code verification much easier. Phonyx does not support advanced uses of the FEniCS programming interface.


Example
-------

Assemble the mass and stiffness matrices

.. code:: python

    from phonyx import *
    #from dolfin import *
    
    mesh = IntervalMesh(3, 0, 1)
    V = FunctionSpace(mesh, 'CG', 1)
    u = TrialFunction(V)
    v = TestFunction(V)
    
    M = assemble(u*v*dx)
    print M.array()

    A = assemble(u.dx(0)*v.dx(0)*dx)
    print A.array()
    
More complete examples can be found in the demo directory in the repository. A good place to start is the solution of the Poisson equation in the ``poisson.py`` file. The code for this is also 100% compatible with FEniCS (dolfin), but gives slightly better results due to using quadrature directly on Expressions instead of interpolating them to a function space as FEniCS does.


Installation
------------

Phonyx is a pure Python package. Put it on the Python search path somewhere and it should just work. Prerequsites are NumPy and optionally SciPy. To run the unit tests you need to have nosetests installed.


Documentation
-------------

The documentation at http://fenicsproject.org/ can be used to some degree since Phonyx is implementing the FEniCS Python API. This documentation should be applicable to the (small) part of the FEniCS API implemented by Phonyx. Which part this might be is unfortunately not documented anywhere except in the source code of Phonyx... 

If you do not already know how to program with FEniCS in Python then you are most likely not in the target user base for Phonyx. In that case you should use FEniCS instead. The installation is a bit more cumbersome, but the power, quality and number of features of FEniCS is extremely much larger.


Licensing
---------

The license of the Phonyx code is BSD/MIT/Python (PSFL)/(L)GPL (2+)/Apache 2

Phonyx is written by Tormod Landet.
