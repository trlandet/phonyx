# encoding: utf8
"""
Solve the Poisson equation

  -∇⋅∇u = f

with analytical solution

  u = sin(π x) sin(π y)

and hence

  f = 2 π² sin(π x) sin(π y)

Pure Dirichlet boundary conditions

Discontinuous Galerkin elements with LDG method
"""
from __future__ import division
import sys
if 'dolfin' in sys.argv[1:]:
    from dolfin import *
    parameters['reorder_dofs_serial'] = False
else:
    from phonyx import *
import numpy
import time


P = 1
Ns = [4, 8, 12, 16, 20, 24][:3]


results = []
for N in Ns:
    t1 = time.time()
    
    # The computational domain, function spaces, trial and test functions
    mesh = UnitSquareMesh(N, N, 'right')
    n = FacetNormal(mesh) 
    Z = VectorFunctionSpace(mesh, 'DG', P)
    V = FunctionSpace(mesh, 'DG', P)
    W = MixedFunctionSpace([Z, V])
    sigma, u = TrialFunctions(W)
    tau, v = TestFunctions(W)
    
    # Define the right hand side and the analytical solutions
    f = Expression('2*pi*pi*sin(pi*x[0])*sin(pi*x[1])')
    ue = Expression('sin(pi*x[0])*sin(pi*x[1])')
    f = interpolate(f, V)
    ue = interpolate(ue, V)
    
    # Calculate LDG penalties
    h = mesh.hmin()
    C11 = 1/h
    print 'N = %5d C11 = %5.1f' % (N, C11)
    C11 = Constant(C11)
    C12 = Constant([0, 0])
    
    jdot = lambda x, y: jump(dot(x, y))
    
    # Bilinear forms
    af = lambda sigma, tau: dot(sigma, tau)*dx
    bf = lambda u, tau: u*div(tau)*dx - (avg(u) + dot(C12, jump(u*n)))*jdot(tau, n)*dS
    cf = lambda u, v: C11*dot(jump(u*n), jump(v*n))*dS + 2*C11*u*v*ds
    
    # Linear forms
    ff = lambda tau: ue*dot(tau, n)*ds
    gf = lambda v: f*v*dx + 2*C11*ue*v*ds
    
    # The final forms
    a = af(sigma, tau) + bf(u, tau) - bf(v, sigma) + cf(u, v)
    L = ff(tau) + gf(v)
    
    sol = Function(W)
    # Assemble and solve equation
    A, b = assemble_system(a, L)
    solve(A, sol.vector(), b, 'lu')
    solsigma, solu = sol.split(deepcopy=True)
        
    # Calculate and store the error
    eu = errornorm(ue, solu, degree_rise=0)
    
    results.append('%5d & %15.3e & %15.3e' % (N, eu, 0))
    if N == Ns[0]:
        results[-1] += '&       - &       - \\\\'
    else:
        Nfac = numpy.log(N/Nprev)
        results[-1] += '& %7.2f & %7.2f \\\\' % (numpy.log(eu_prev/eu)/Nfac, 0)
    
    # Save for next iteration
    Nprev = N
    eu_prev = eu
    
    results[-1] += ' %% %10.3f seconds' % (time.time() - t1)


# Print results
print '\n'.join(results)
list_timings(TimingClear_keep, [TimingType_wall])
