# encoding: utf8
"""
Solve the Poisson equation

  -∆u = f in Ω
    u = g on ∂Ω

With Ω = [0,1] and analytical solution 

    u = sin(π x)

we get 

    f = π² sin(π x)
    g = 0

The weak form is as follows when we do not apply essential boundary conditions
and hence do not set v = 0 on ∂Ω

  (∇u, ∇v) - <∇u⋅n, v> = (f, v)
  
The round parentheses denote inner products over the domain Ω while the angle
brackets denote inner products over the domain boundary ∂Ω
 
When using the Nitsche method for applying weak boundary conditions we get an
extra symmetry term <∇v⋅n, u - g>  and a stabilization term 
λ/h <u - g, v> where λ is a small number. Both terms include (u - g) which
should be zero on ∂Ω for the true solution

  (∇u, ∇v) - <∇u⋅n, v> - <∇v⋅n, u - g> + λ/h <u - g, v> = (f, v)

Whith this dormulation the boundary conditions are applied in a weak way and
we do not need to add any DirichletBC to the problem
"""
import numpy
from matplotlib import pyplot

import sys
if 'dolfin' in sys.argv[1:]:
    from dolfin import *
    print 'DOLFIN'
else:
    from phonyx import *
    print 'PHONYX'

parameters['reorder_dofs_serial'] = False

N = 5
offset = 0.0

mesh = UnitIntervalMesh(N)
V = FunctionSpace(mesh, 'CG', 1)
u = TrialFunction(V)
v = TestFunction(V)

n = FacetNormal(mesh)
h = Circumradius(mesh)
f = Expression('pi*pi*sin(pi*x[0])')
g = Constant(offset)
lam = Constant(1.0)

eq = u.dx(0)*v.dx(0)*dx
eq -= u.dx(0)*n[0]*v*ds
eq -= v.dx(0)*n[0]*(u - g)*ds
eq += lam/h*(u - g)*v*ds
eq -= f*v*dx

# Assemble matrices and apply boundary conditions
a, L = system(eq)
A = assemble(a)
b = assemble(L)

# Solve the linear system
ufunc = Function(V)
solve(A, ufunc.vector(), b)
U = ufunc.vector().array()

# The analytical solution
x = mesh.coordinates()[:,0]
y = numpy.sin(numpy.pi*x)+offset

if N < 15:
    print A.array()
    print b.array()
    print U.array()

error = ((U - y)**2).sum()**0.5/N
print 'N:', N
print 'Error: %.2e' % error

# Plot the solutions 
pyplot.plot(x, U, label='FEM')
pyplot.plot(x, y, '--k', label='Exact')
pyplot.legend()
pyplot.show()
