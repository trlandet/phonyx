# encoding: utf8
"""
Solve the stationary Navier-Stokes equation

  cw⋅∇u - k∇⋅∇u + ∇p = f
               ∇⋅u = 0

with analytical solution

  u = [sin(π y),  cos(π x)]
  w = u
  p = sin(2 π x)

and hence

  f = [ c π cos(π x)cos(π y) + k π² sin(π y) + 2 π cos(2 π x),
       -c π sin(π x)sin(π y) + k π² cos(π x)]

Pure Dirichlet boundary conditions

Continuous Galerkin elements
"""
from __future__ import division
import sys
if 'dolfin' in sys.argv:
    from dolfin import *
    parameters['reorder_dofs_serial'] = False
else:
    from phonyx import *
import numpy
import time


def dirichlet_region(x, on_boundary):
    return on_boundary #and x[1]*(1 - x[1]) < 1e-8


convection_c = 100
diffusion_k = 1
Pu, Pp = 2, 1
Ns = [4, 8, 12, 16, 20, 24][:4]


results = []
for N in Ns:
    print 'Calculating for N=%d' % N
    t1 = time.time()
    
    # The computational domain, function spaces, trial and test functions
    mesh = UnitSquareMesh(N, N, 'right')
    n = FacetNormal(mesh)
    V = VectorFunctionSpace(mesh, 'CG', Pu)
    Q = FunctionSpace(mesh, 'CG', Pp)
    Vlm = FunctionSpace(mesh, 'R', 0)
    W = MixedFunctionSpace([V, Q, Vlm])
    u, p, lm_trial = TrialFunctions(W)
    v, q, lm_test = TestFunctions(W)
    
    # Define the right hand side and the analytical solutions
    c = Constant(convection_c)
    k = Constant(diffusion_k)
    f = Expression([' c*pi*cos(pi*x[0])*cos(pi*x[1]) + k*pi*pi*sin(pi*x[1]) + 2*pi*cos(2*pi*x[0])',
                    '-c*pi*sin(pi*x[0])*sin(pi*x[1]) + k*pi*pi*cos(pi*x[0])'], c=c, k=k)
    ue = Expression(['sin(pi*x[1])', 'cos(pi*x[0])'])
    pe = Expression('sin(2*pi*x[0])')
    
    f = interpolate(f, V)
    ue = interpolate(ue, V)
    pe = interpolate(pe, Q)
    
    # Define boundary conditions
    bc_u = DirichletBC(W.sub(0), ue, dirichlet_region)

    # Convective velocity
    w = ue
    z = u # project(ue, V)
    
    # Define, assemble and solve equation
    a  = c*dot(dot(w, nabla_grad(z)), v)*dx
    a += k*inner(nabla_grad(u), nabla_grad(v))*dx
    a += -nabla_div(u)*q*dx
    a += -nabla_div(v)*p*dx + p*inner(v, n)*ds
    a += (p*lm_test + q*lm_trial)*dx
    L = inner(f, v)*dx
    sol = Function(W)
    A, b = assemble_system(a, L, bc_u)
    solve(A, sol.vector(), b, 'lu')
    solu, solp, sollm = sol.split()
    
    # Calculate and store the error
    eu = errornorm(ue, solu, degree_rise=0)
    ep = errornorm(pe, solp, degree_rise=0)
    results.append('%5d & %15.3e & %15.3e' % (N, eu, ep))
    if N == Ns[0]:
        results[-1] += '&       - &       - \\\\'
    else:
        Nfac = numpy.log(N/Nprev)
        results[-1] += '& %7.2f & %7.2f \\\\' % (numpy.log(eu_prev/eu)/Nfac,
                                                 numpy.log(ep_prev/ep)/Nfac)
    
    # Save for next iteration
    Nprev = N
    eu_prev = eu
    ep_prev = ep
    
    results[-1] += ' %% %10.3f seconds' % (time.time() - t1)


# Print results
print '\n'.join(results)
list_timings()
