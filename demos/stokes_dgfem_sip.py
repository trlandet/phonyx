# encoding: utf8
"""
Solve Stokes equation

  -∇⋅∇u + ∇p = f
         ∇⋅u = 0

With analytical solution

  u = [sin(π y),  cos(π x)]
  p = sin(2 π x)

and hence

  f = [π² sin(π y) + 2 π cos(2 π x),
       π² cos(π x)]

Pure Dirichlet boundary conditions

Discontinuous Galerkin elements with symmetric interior penalty method
"""
from __future__ import division
import numpy
import sys
if 'dolfin' in sys.argv:
    from dolfin import *
else:
    from phonyx import *
    parameters['linear_algebra_backend'] = 'numpy'
import time


Pu, Pp = 2, 1
Ns = [4, 8, 12, 16, 20, 24]


class DirichletRegion(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary


def define_penalty(mesh, P, k_min, k_max, boost_factor=3, exponent=1):
    """
    Define the penalty parameter used in the Poisson equations
    
    Arguments:
        mesh: the mesh used in the simulation
        P: the polynomial degree of the unknown
        k_min: the minimum diffusion coefficient
        k_max: the maximum diffusion coefficient
        boost_factor: the penalty is multiplied by this factor
        exponent: set this to greater than 1 for superpenalisation
    """
    assert k_max >= k_min
    ndim = mesh.geometry().dim()
    # Calculate geometrical factor used in the penalty
    geom_fac = 0
    for cell in cells(mesh):
        vol = cell.volume()
        area = sum(cell.facet_area(i) for i in range(ndim + 1))
        gf = area/vol
        geom_fac = max(geom_fac, gf)
    
    penalty = boost_factor * k_max**2/k_min * (P + 1)*(P + ndim)/ndim * geom_fac**exponent
    return penalty


results = []
for N in Ns:
    t1 = time.time()
    
    # The computational domain, function spaces, trial and test functions
    mesh = UnitSquareMesh(N, N, 'right')
    n = FacetNormal(mesh)
    V = VectorFunctionSpace(mesh, 'DG', Pu)
    Q = FunctionSpace(mesh, 'DG', Pp)
    Vlm = FunctionSpace(mesh, 'R', 0)
    W = MixedFunctionSpace([V, Q, Vlm])
    u, p, lm_trial = TrialFunctions(W)
    v, q, lm_test = TestFunctions(W)
    
    # Mark boundary
    marker = FacetFunction("size_t", mesh)
    dr = DirichletRegion()
    dr.mark(marker, 1)
    ds2 = Measure('ds')(subdomain_data=marker)
    
    # Define the right hand side and the analytical solutions
    f = Expression(['pi*pi*sin(pi*x[1]) + 2*pi*cos(2*pi*x[0])',
                    'pi*pi*cos(pi*x[0])'])
    ue = Expression(['sin(pi*x[1])', 'cos(pi*x[0])'])
    pe = Expression('sin(2*pi*x[0])')
    
    # To ease comparison with dolfin
    f = interpolate(f, V)
    ue = interpolate(ue, V)
    pe = interpolate(pe, Q)
    
    # Calculate SIPG penalty 
    penalty1 = define_penalty(mesh, Pu, 1, 1, boost_factor=3, exponent=1.0)
    penalty2 = define_penalty(mesh, Pu, 1, 1, boost_factor=6, exponent=1.0)
    penalty_dS = Constant(penalty1)
    penalty_ds = Constant(penalty2)
    print 'N: %d, penalty:  dS %.1f  ds %.1f' % (N, penalty1, penalty2)
    
    # RHS
    eq = -dot(f, v)*dx
    
    # Lagrange multiplicator to remove the pressure null space
    # ∫ p dx = 0
    eq += (p*lm_test + q*lm_trial)*dx
    
    # Momentum equations
    for d in range(2):
        # Divergence free criterion
        # ∇⋅u = 0
        eq -= u[d].dx(d)*q*dx
        eq += avg(q)*jump(u[d])*n[d]('+')*dS
        
        # Diffusion:
        # -∇⋅∇u
        eq += dot(grad(u[d]), grad(v[d]))*dx
        
        # Symmetric Interior Penalty method for -∇⋅μ∇u
        eq -= dot(n('+'), avg(grad(u[d])))*jump(v[d])*dS
        eq -= dot(n('+'), avg(grad(v[d])))*jump(u[d])*dS
        
        # Symmetric Interior Penalty coercivity term
        eq += penalty_dS*jump(u[d])*jump(v[d])*dS
        
        # Pressure
        # ∇p
        eq -= v[d].dx(d)*p*dx
        eq += avg(p)*jump(v[d])*n[d]('+')*dS
        
        # Dirichlet boundary
        for dval, dds in ((ue, ds2(1)), ):
            # Divergence free criterion
            eq += q*(u[d] - dval[d])*n[d]*dds
            
            # SIPG for -∇⋅μ∇u 
            eq -= dot(n, grad(u[d]))*v[d]*dds
            eq -= dot(n, grad(v[d]))*u[d]*dds
            eq += dot(n, grad(v[d]))*dval[d]*dds
            
            # Weak Dirichlet
            eq += penalty_ds*(u[d] - dval[d])*v[d]*dds
            
            # Pressure
            eq += p*v[d]*n[d]*dds
    
    a, L = system(eq)
    
    # Assemble and solve equation
    A, b = assemble_system(a, L)
    sol = Function(W)
    solve(A, sol.vector(), b, 'lu')
    solu, solp, sollm = sol.split()
    
    # Calculate and store the error
    eu = errornorm(ue, solu, degree_rise=0)
    ep = errornorm(pe, solp, degree_rise=0)
    results.append('%5d & %15.3e & %15.3e' % (N, eu, ep))
    if N == Ns[0]:
        results[-1] += '&       - &       - \\\\'
    else:
        Nfac = numpy.log(N/Nprev)
        results[-1] += '& %7.2f & %7.2f \\\\' % (numpy.log(eu_prev/eu)/Nfac,
                                                 numpy.log(ep_prev/ep)/Nfac)
    
    # Save for next iteration
    Nprev = N
    eu_prev = eu
    ep_prev = ep
    
    results[-1] += ' %% %10.3f seconds' % (time.time() - t1)


# Print results
print '\n'.join(results)
list_timings(TimingClear_keep, [TimingType_wall])
