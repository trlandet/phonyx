# encoding: utf8
"""
Solve the Poisson equation

  -∇⋅∇u = f

with analytical solution

  u = sin(π x) sin(π y)

and hence

  f = 2 π² sin(π x) sin(π y)

Pure Dirichlet boundary conditions

Discontinuous Galerkin elements with SIP or LDG methods
"""
from __future__ import division
import sys
if 'dolfin' in sys.argv[1:]:
    from dolfin import *
    #parameters['reorder_dofs_serial'] = False
else:
    from phonyx import *
import numpy
import time

if 'sip' in sys.argv[1:]:
    method = 'sip'
else:
    method = 'ldg'


P = 1
Ns = [4, 8, 12, 16, 20, 24][:3]


def define_penalty(mesh, P, k_min, k_max, boost_factor=3, exponent=1):
    """
    Define the penalty parameter used in the Poisson equations
    
    Arguments:
        mesh: the mesh used in the simulation
        P: the polynomial degree of the unknown
        k_min: the minimum diffusion coefficient
        k_max: the maximum diffusion coefficient
        boost_factor: the penalty is multiplied by this factor
        exponent: set this to greater than 1 for superpenalisation
    """
    assert k_max >= k_min
    ndim = mesh.geometry().dim()
    
    # Calculate geometrical factor used in the penalty
    geom_fac = 0
    for cell in cells(mesh):
        vol = cell.volume()
        area = sum(cell.facet_area(i) for i in range(ndim + 1))
        gf = area/vol
        geom_fac = max(geom_fac, gf)
    
    penalty = boost_factor * k_max**2/k_min * (P + 1)*(P + ndim)/ndim * geom_fac**exponent
    return penalty


results = []
for N in Ns:
    t1 = time.time()
    
    # The computational domain, outwards normals and function space for the unknown u
    mesh = UnitSquareMesh(N, N, 'right')
    n = FacetNormal(mesh) 
    V = FunctionSpace(mesh, 'DG', P)
    
    # Define the right hand side and the analytical solutions
    f = Expression('2*pi*pi*sin(pi*x[0])*sin(pi*x[1])')
    ue = Expression('sin(pi*x[0])*sin(pi*x[1])')
    f = interpolate(f, V)
    ue = interpolate(ue, V)
    
    ###########################################################################
    # Symmetric Interior Penalty method
    if method == 'sip':
        u = TrialFunction(V)
        v = TestFunction(V)
    
        # Calculate SIPG penalty
        penalty1 = define_penalty(mesh, P, 1, 1, boost_factor=3, exponent=1.0)
        penalty2 = penalty1*2
        penalty_dS = Constant(penalty1)
        penalty_ds = Constant(penalty2)
        print 'N: %3d, penalty: %.1f, matrix size %d' % (N, penalty1, V.dim())
        
        # Define bilinear form    
        a = dot(grad(u), grad(v))*dx
    
        # Symmetric Interior Penalty method for -∇⋅μ∇u
        a -= dot(n('+'), avg(grad(u)))*jump(v)*dS
        a -= dot(n('+'), avg(grad(v)))*jump(u)*dS
        
        # Symmetric Interior Penalty coercivity term
        a += penalty_dS*jump(u)*jump(v)*dS
        a -= dot(n, grad(u))*v*ds
        a -= dot(n, grad(v))*u*ds
        a += penalty_ds*u*v*ds
        
        # Define linear form
        L = f*v*dx
        L -= dot(n, grad(v))*ue*ds
        L += penalty_ds*ue*v*ds
        
        # Assemble and solve equation
        A, b = assemble_system(a, L)
        sol = Function(V)
        solve(A, sol.vector(), b, 'lu')
        solu = sol
    
    ###########################################################################
    # Local Discontinuous Galerkin method
    else:
        Z = VectorFunctionSpace(mesh, 'DG', P)
        W = MixedFunctionSpace([Z, V])
        sigma, u = TrialFunctions(W)
        tau, v = TestFunctions(W)
        
        # Define the right hand side and the analytical solutions
        f = Expression('2*pi*pi*sin(pi*x[0])*sin(pi*x[1])')
        ue = Expression('sin(pi*x[0])*sin(pi*x[1])')
        f = interpolate(f, V)
        ue = interpolate(ue, V)
        
        # Calculate LDG penalties
        h = mesh.hmin()
        penalty1 = 1/h
        penalty2 = penalty1*2
        penalty_dS = Constant(penalty1)
        penalty_ds = Constant(penalty2)
        print 'N: %3d, penalty: %.1f, matrix size %d' % (N, penalty1, W.dim())
        
        jdot = lambda x, y: jump(dot(x, y))
        
        # Bilinear forms
        af = lambda sigma, tau: dot(sigma, tau)*dx
        bf = lambda u, tau: u*div(tau)*dx - avg(u)*jump(tau, n)*dS
        cf = lambda u, v: penalty_dS*dot(jump(u, n), jump(v, n))*dS + penalty_ds*u*v*ds
        
        # Linear forms
        ff = lambda tau: ue*dot(tau, n)*ds
        gf = lambda v: f*v*dx + penalty_ds*ue*v*ds
        
        # The final forms
        a = af(sigma, tau) + bf(u, tau) - bf(v, sigma) + cf(u, v)
        L = ff(tau) + gf(v)
        
        # Assemble and solve equation
        A, b = assemble_system(a, L)
        sol = Function(W)
        solve(A, sol.vector(), b, 'lu')
        solsigma, solu = sol.split(deepcopy=True)
    
    # Calculate and store the error
    eu = errornorm(ue, solu, degree_rise=0)
    
    results.append('%5d & %15.3e' % (N, eu))
    if N == Ns[0]:
        results[-1] += '&       - \\\\'
    else:
        Nfac = numpy.log(N/Nprev)
        results[-1] += '& %7.2f \\\\' % (numpy.log(eu_prev/eu)/Nfac,)
    
    # Save for next iteration
    Nprev = N
    eu_prev = eu
    
    results[-1] += ' %% %10.3f seconds' % (time.time() - t1)


# Print results
print '\n'.join(results)
list_timings(TimingClear_keep, [TimingType_wall])
