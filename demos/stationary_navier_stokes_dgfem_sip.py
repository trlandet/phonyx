# encoding: utf8
"""
Solve the stationary Navier-Stokes equation

  cw⋅∇u - k∇⋅∇u + ∇p = f
               ∇⋅u = 0

with analytical solution

  u = [sin(π y),  cos(π x)]
  w = u
  p = sin(2 π x)

and hence

  f = [ c π cos(π x)cos(π y) + k π² sin(π y) + 2 π cos(2 π x),
       -c π sin(π x)sin(π y) + k π² cos(π x)]

Pure Dirichlet boundary conditions

Discontinuous Galerkin elements with symmetric interior penalty method
"""
from __future__ import division
import sys
if 'dolfin' in sys.argv[1:]:
    from dolfin import *
    #parameters['reorder_dofs_serial'] = False
else:
    from phonyx import *
postprocess = 'pp' in sys.argv[1:]

import numpy
import time
from convert_to_dgt import convert_to_dgt


convection_c = 100
diffusion_k = 0.5
Pu, Pp = 2, 1
Ns = [4, 8, 12, 16, 20, 24][:3]


class DirichletRegion(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary #and x[1]*(1 - x[1]) < 1e-8


def define_penalty(mesh, P, k_min, k_max, boost_factor=3, exponent=1):
    """
    Define the penalty parameter used in the Poisson equations
    
    Arguments:
        mesh: the mesh used in the simulation
        P: the polynomial degree of the unknown
        k_min: the minimum diffusion coefficient
        k_max: the maximum diffusion coefficient
        boost_factor: the penalty is multiplied by this factor
        exponent: set this to greater than 1 for superpenalisation
    """
    assert k_max >= k_min
    ndim = mesh.geometry().dim()
    
    # Calculate geometrical factor used in the penalty
    geom_fac = 0
    for cell in cells(mesh):
        vol = cell.volume()
        area = sum(cell.facet_area(i) for i in range(ndim + 1))
        gf = area/vol
        geom_fac = max(geom_fac, gf)
    
    penalty = boost_factor * k_max**2/k_min * (P + 1)*(P + ndim)/ndim * geom_fac**exponent
    return penalty


def bdm_projection(w, mesh):
    """
    Implement equation 4a and 4b in "Two new techniques for generating exactly
    incompressible approximate velocities" by Bernardo Cocburn (2009)
    
    For each element K in the mesh:
    
        <u⋅n, φ> = <û⋅n, φ>  ∀ ϕ ∈ P_{k}(F) for any face F ∈ ∂K
        (u, ϕ) = (w, ϕ)      ∀ φ ∈ P_{k-2}(K)^2
        (u, ϕ) = (w, ϕ)      ∀ φ ∈ {ϕ ∈ P_{k}(K)^2 : ∇⋅ϕ = 0 in K, ϕ⋅n = 0 on ∂K}  
        
    Here w is the input velocity function in DG2 space and û is the flux at
    each face (upwind value of the input velocity w). P_{x} is the space of
    polynomials of order k
    """
    ue = w.function_space().ufl_element()
    k = 2
    assert ue.family() == 'Discontinuous Lagrange'
    assert ue.degree() == k 
    assert ue.num_sub_elements() == 2
    
    V = VectorFunctionSpace(mesh, 'DG', k)
    n = FacetNormal(mesh)
    
    # The mixed function space of the projection test functions
    V1 = FunctionSpace(mesh, 'DGT', k)
    V2 = VectorFunctionSpace(mesh, 'DG', k-2)
    V3 = FunctionSpace(mesh, 'Bubble', 3)
    W = MixedFunctionSpace([V1, V2, V3])
    v1, v2, v3b = TestFunctions(W)
    u = TrialFunction(V)
    
    # Calculate the upwind normal velocity, u_hat = w⋅n⁺, on all facets
    V_hat = VectorFunctionSpace(mesh, 'DGT', k)
    u_hat = Function(V_hat)
    convert_to_dgt(w, u_hat)
    
    # Equation 1 - flux through the sides
    a = dot(u, n)*v1*ds
    L = dot(u_hat, n)*v1*ds
    for R in '+-':
        a += dot(u(R), n(R))*v1(R)*dS
        L += dot(u_hat(R), n(R))*v1(R)*dS 
    
    # Equation 2 - internal shape
    a += dot(u, v2)*dx
    L += dot(w, v2)*dx
    
    # Equation 3 - BDM Phi
    v3 = as_vector([v3b.dx(1), -v3b.dx(0)]) # Curl of [0, 0, v3b]
    a += dot(u, v3)*dx
    L += dot(w, v3)*dx
    
    # Find the projected velocity
    ls = LocalSolver(a, L)
    U = Function(V)
    ls.solve_local_rhs(U)
    
    w.vector()[:] = U.vector()

results = []
for N in Ns:
    t1 = time.time()
    
    # The computational domain, function spaces, trial and test functions
    mesh = UnitSquareMesh(N, N, 'right')
    n = FacetNormal(mesh)
    V = VectorFunctionSpace(mesh, 'DG', Pu)
    Q = FunctionSpace(mesh, 'DG', Pp)
    Vlm = FunctionSpace(mesh, 'R', 0)
    W = MixedFunctionSpace([V, Q, Vlm])
    u, p, lm_trial = TrialFunctions(W)
    v, q, lm_test = TestFunctions(W)
    
    # Mark boundary
    marker = FacetFunction("size_t", mesh)
    dr = DirichletRegion()
    dr.mark(marker, 1)
    ds2 = Measure('ds')(subdomain_data=marker)
    
    # Define the right hand side and the analytical solutions
    c = Constant(convection_c)
    k = Constant(diffusion_k)
    f = Expression([' c*pi*cos(pi*x[0])*cos(pi*x[1]) + k*pi*pi*sin(pi*x[1]) + 2*pi*cos(2*pi*x[0])',
                    '-c*pi*sin(pi*x[0])*sin(pi*x[1]) + k*pi*pi*cos(pi*x[0])'], c=c, k=k)
    ue = Expression(['sin(pi*x[1])', 'cos(pi*x[0])'])
    pe = Expression('sin(2*pi*x[0])')
    
    f = interpolate(f, V)
    ue = interpolate(ue, V)
    pe = interpolate(pe, Q)
    
    # Convective velocity
    w = Function(V)
    w.vector()[:] = 0
    #w.assign(ue)
    
    w_nU = (dot(w, n) + abs(dot(w, n)))/2.0
    w_nD = (dot(w, n) - abs(dot(w, n)))/2.0
    
    # Calculate SIPG penalty 
    penalty1 = define_penalty(mesh, Pu, diffusion_k, diffusion_k, boost_factor=3, exponent=1.0)
    penalty2 = penalty1*2
    penalty_dS = Constant(penalty1)
    penalty_ds = Constant(penalty2)
    print 'N: %d, penalty:  dS %.1f  ds %.1f' % (N, penalty1, penalty2)
    
    # RHS
    eq = -dot(f, v)*dx
    
    # Lagrange multiplicator to remove the pressure null space
    # ∫ p dx = 0
    eq += (p*lm_test + q*lm_trial)*dx
    
    # Momentum equations
    for d in range(2):
        # Divergence free criterion
        # ∇⋅u = 0
        eq -= u[d]*q.dx(d)*dx
        eq += avg(u[d])*jump(q)*n[d]('+')*dS
        
        # Convection:
        # -w⋅∇u    
        flux_nU = u[d]*w_nU
        flux = jump(flux_nU)
        eq -= c*u[d]*div(v[d]*w)*dx
        eq += c*flux*jump(v[d])*dS
        eq += c*flux_nU*v[d]*ds
        
        # Diffusion:
        # -∇⋅∇u
        eq += k*dot(grad(u[d]), grad(v[d]))*dx
        
        # Symmetric Interior Penalty method for -∇⋅μ∇u
        eq -= k*dot(n('+'), avg(grad(u[d])))*jump(v[d])*dS
        eq -= k*dot(n('+'), avg(grad(v[d])))*jump(u[d])*dS
        
        # Symmetric Interior Penalty coercivity term
        eq += penalty_dS*jump(u[d])*jump(v[d])*dS
        
        # Pressure
        # ∇p
        eq -= v[d].dx(d)*p*dx
        eq += avg(p)*jump(v[d])*n[d]('+')*dS
        
        # Dirichlet boundary
        for dval, dds in ((ue, ds2(1)), ):
            # Divergence free criterion
            eq += q*u[d]*n[d]*dds
            
            # Convection
            eq += c*w_nD*dval[d]*v[d]*dds
            
            # SIPG for -∇⋅μ∇u 
            eq -= k*dot(n, grad(u[d]))*v[d]*dds
            eq -= k*dot(n, grad(v[d]))*u[d]*dds
            eq += k*dot(n, grad(v[d]))*dval[d]*dds
            
            # Weak Dirichlet
            eq += penalty_ds*(u[d] - dval[d])*v[d]*dds
            
            # Pressure
            eq += p*v[d]*n[d]*dds
    
    a, L = system(eq)
    sol = Function(W)
    
    picard_iteration = 0
    picard_difference = 1e10
    while picard_difference > 1e-4:
        picard_iteration += 1
        if picard_iteration > 10:
            print 'PICARD ITERATIONS NOT CONVERGED AFTER 10 ITERATIONS'
            exit()
            break
        
        # Assemble and solve equation
        A, b = assemble_system(a, L)
        solve(A, sol.vector(), b, 'lu')
        solu, solp, sollm = sol.split(deepcopy=True)
        
        if postprocess:
            bdm_projection(solu, mesh)
        
        # Calculate and store the error
        eu = errornorm(ue, solu, degree_rise=0)
        ep = errornorm(pe, solp, degree_rise=0)
        picard_difference = errornorm(w, solu, degree_rise=0)
        #print '%4d  %15.8e  %15.8e ' % (picard_iteration, eu, ep),
        #print '%15.8e  %15.8e  %15.8e' % (norm(w), norm(solu), picard_difference)
        print '%4d  %15.8e  %15.8e ' % (picard_iteration, picard_difference, eu) 
        
        w.assign(solu)
    
    results.append('%5d & %15.3e & %15.3e' % (N, eu, ep))
    if N == Ns[0]:
        results[-1] += '&       - &       - \\\\'
    else:
        Nfac = numpy.log(N/Nprev)
        results[-1] += '& %7.2f & %7.2f \\\\' % (numpy.log(eu_prev/eu)/Nfac,
                                                 numpy.log(ep_prev/ep)/Nfac)
    
    # Save for next iteration
    Nprev = N
    eu_prev = eu
    ep_prev = ep
    
    results[-1] += ' %% %10.3f seconds' % (time.time() - t1)


print 'RESULTS:'
print '\n'.join(results)
#list_timings(TimingClear_keep, [TimingType_wall])

