# encoding: utf8
"""
Solve the Stokes equation

  -∇⋅∇u + ∇p = f
         ∇⋅u = 0

with analytical solution

  u = [sin(π y),  cos(π x)]
  w = u
  p = sin(2 π x)

and hence

  f = [π² sin(π y) + 2 π cos(2 π x),
       π² cos(π x)]

Pure Dirichlet boundary conditions

Discontinuous Galerkin elements with LDG method
"""
from __future__ import division
import sys
if 'dolfin' in sys.argv[1:]:
    from dolfin import *
    parameters['reorder_dofs_serial'] = False
else:
    from phonyx import *
import numpy
import time


Pu, Pp = 2, 1
Ns = [4, 8, 12, 16, 20, 24][:4]


results = []
for N in Ns:
    t1 = time.time()
    
    # The computational domain, function spaces, trial and test functions
    mesh = UnitSquareMesh(N, N, 'right')
    n = FacetNormal(mesh)
    Z = TensorFunctionSpace(mesh, 'DG', Pu) 
    V = VectorFunctionSpace(mesh, 'DG', Pu)
    Q = FunctionSpace(mesh, 'DG', Pp)
    Vlm = FunctionSpace(mesh, 'R', 0)
    W = MixedFunctionSpace([Z, V, Q, Vlm])
    sigma, u, p, lm_trial = TrialFunctions(W)
    tau, v, q, lm_test = TestFunctions(W)
    
    # Define the right hand side and the analytical solutions
    f = Expression(['pi*pi*sin(pi*x[1]) + 2*pi*cos(2*pi*x[0])',
                    'pi*pi*cos(pi*x[0])'])
    ue = Expression(['sin(pi*x[1])', 'cos(pi*x[0])'])
    pe = Expression('sin(2*pi*x[0])')
    
    f = interpolate(f, V)
    ue = interpolate(ue, V)
    pe = interpolate(pe, Q)
    
    # Calculate LDG penalties
    h = mesh.hmin()
    C11 = 1/h
    D11 = 0
    C12 = D12 = Constant([1, 1])
    print 'N = %5d C11 = %5.1f  - D11 = %5.1f' % (N, C11, D11)
    C11 = Constant(C11)
    D11 = Constant(D11)
    
    # Lagrange multiplicator to remove the pressure null space
    # ∫ p dx = 0
    lm_form = (p*lm_test + q*lm_trial)*dx
    
    # The following forms are from Cockburn, Kanschat, Shötzau and Schwab,
    # Local discontinuous Galerkin methods for the Stokes system (2002)
    
    ojump = lambda x, n: outer(x, n)('+') + outer(x, n)('-')
    
    # Bilinear forms  
    af = lambda sigma, tau: inner(sigma, tau)*dx
    bf = lambda u, tau: dot(u, div(tau))*dx - dot(avg(u) + dot(ojump(u, n), C12), jump(tau, n))*dS
    cf = lambda u, v: C11*inner(ojump(u, n), ojump(v, n))*dS + C11*inner(outer(u, n), outer(v, n))*ds
    df = lambda v, p: -p*div(v)*dx + (avg(p) - dot(D12, jump(p, n)))*jump(v, n)*dS + p*dot(v, n)*ds
    ef = lambda p, q: D11*dot(jump(p, n), jump(q, n))*dS
    
    # Linear forms
    ff = lambda tau: dot(ue, dot(tau, n))*ds
    gf = lambda v: dot(f, v)*dx + C11*inner(outer(ue, n), outer(v, n))*ds
    hf = lambda q: - dot(ue, n)*q*ds
    
    # The final forms
    a = af(sigma, tau) + bf(u, tau) - bf(v, sigma) + cf(u, v) + df(v, p) - df(u, q) + ef(p, q) + lm_form
    L = ff(tau) + gf(v) + hf(q)
    
    sol = Function(W)
    # Assemble and solve equation
    A, b = assemble_system(a, L)
    solve(A, sol.vector(), b, 'lu')
    solsigma, solu, solp, sollm = sol.split(deepcopy=True)
        
    # Calculate and store the error
    eu = errornorm(ue, solu, degree_rise=0)
    ep = errornorm(pe, solp, degree_rise=0)
    
    results.append('%5d & %15.3e & %15.3e' % (N, eu, ep))
    if N == Ns[0]:
        results[-1] += '&       - &       - \\\\'
    else:
        Nfac = numpy.log(N/Nprev)
        results[-1] += '& %7.2f & %7.2f \\\\' % (numpy.log(eu_prev/eu)/Nfac,
                                                 numpy.log(ep_prev/ep)/Nfac)
    
    # Save for next iteration
    Nprev = N
    eu_prev = eu
    ep_prev = ep
    
    results[-1] += ' %% %10.3f seconds' % (time.time() - t1)


# Print results
print '\n'.join(results)
list_timings(TimingClear_keep, [TimingType_wall])

