# encoding: utf8
"""
Solve the Poisson equation

  -∆u = f in Ω
    u = g on ∂Ω

With Ω = [0,1] and analytical solution 

    u = sin(π x)

we get 

    f = π² sin(π x)
    g = 0
    
The weak form of the problem is

  (∇u, ∇v) = (f, v)

The round parentheses denote inner products over the domain Ω.
"""
import numpy
from matplotlib import pyplot

from phonyx import *
#from dolfin import *

parameters['reorder_dofs_serial'] = False

N = 5
mesh = UnitIntervalMesh(N)
V = FunctionSpace(mesh, 'CG', 1)
u = TrialFunction(V)
v = TestFunction(V)

def boundary(x, on_boundary):
    return on_boundary
bc = DirichletBC(V, Constant(0), boundary)

f = Expression('pi*pi*sin(pi*x[0])')

a = u.dx(0)*v.dx(0)*dx
L = f*v*dx

# Assemble matrices and apply boundary conditions
A = assemble(a)
b = assemble(L)
bc.apply(A, b)

# Solve the linear system
ufunc = Function(V)
solve(A, ufunc.vector(), b)
U = ufunc.vector().array()

# The analytical solution
x = mesh.coordinates()[:,0]
y = numpy.sin(numpy.pi*x)

if N < 15:
    print A.array()
    print b.array()
    print U.array()

error = ((U - y)**2).sum()**0.5/N
print 'N:', N
print 'Error: %.2e' % error

# Plot the solutions 
pyplot.plot(x, U, label='FEM')
pyplot.plot(x, y, '--k', label='Exact')
pyplot.legend()
pyplot.show()
