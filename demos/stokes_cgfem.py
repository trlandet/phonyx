# encoding: utf8
"""
Solve Stokes equation

  -∇⋅∇u + ∇p = f
         ∇⋅u = 0

With analytical solution

  u = [sin(π y),  cos(π x)]
  p = sin(2 π x)

and hence

  f = [π² sin(π y) + 2 π cos(2 π x),
       π² cos(π x)]

Pure Dirichlet boundary conditions

Continuous Galerkin elements
"""
from __future__ import division
import numpy
import sys
if 'dolfin' in sys.argv:
    from dolfin import *
else:
    from phonyx import *
import time


def dirichlet_region(x, on_boundary):
    return on_boundary


Pu, Pp = 2, 1
Ns = [4, 8, 12, 16] #, 20, 24]


results = []
for N in Ns:
    t1 = time.time()
    
    # The computational domain, function spaces, trial and test functions
    mesh = UnitSquareMesh(N, N, 'right')
    n = FacetNormal(mesh)
    V = VectorFunctionSpace(mesh, 'CG', Pu)
    Q = FunctionSpace(mesh, 'CG', Pp)
    Vlm = FunctionSpace(mesh, 'R', 0)
    W = MixedFunctionSpace([V, Q, Vlm])
    u, p, lm_trial = TrialFunctions(W)
    v, q, lm_test = TestFunctions(W)
    
    # Define the right hand side and the analytical solutions
    f = Expression(['pi*pi*sin(pi*x[1]) + 2*pi*cos(2*pi*x[0])',
                    'pi*pi*cos(pi*x[0])'])
    ue = Expression(['sin(pi*x[1])', 'cos(pi*x[0])'])
    pe = Expression('sin(2*pi*x[0])')
    
    # Define boundary conditions
    bc_u = DirichletBC(W.sub(0), ue, dirichlet_region)
    
    # Define, assemble and solve equation
    a = inner(nabla_grad(u), nabla_grad(v))*dx
    a += -nabla_div(u)*q*dx
    a += -nabla_div(v)*p*dx + p*inner(v, n)*ds
    a += (p*lm_test + q*lm_trial)*dx
    L = inner(f, v)*dx
    sol = Function(W)
    A, b = assemble_system(a, L)
    bc_u.apply(A, b)
    solve(A, sol.vector(), b, 'lu')
    solu, solp, sollm = sol.split(deepcopy=True)
    
    # Calculate and store the error
    eu = errornorm(ue, solu, degree_rise=0)
    ep = errornorm(pe, solp, degree_rise=0)
    results.append('%5d & %15.3e & %15.3e' % (N, eu, ep))
    if N == Ns[0]:
        results[-1] += '&       - &       - \\\\'
    else:
        Nfac = numpy.log(N/Nprev)
        results[-1] += '& %7.2f & %7.2f \\\\' % (numpy.log(eu_prev/eu)/Nfac,
                                                 numpy.log(ep_prev/ep)/Nfac)
    
    # Save for next iteration
    Nprev = N
    eu_prev = eu
    ep_prev = ep
    
    results[-1] += ' %% %10.3f seconds' % (time.time() - t1)


# Print results
print '\n'.join(results)
list_timings()
