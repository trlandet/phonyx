from phonyx import Function


VTK_LINE = 3
VTK_QUADRATIC_EDGE = 21
VTK_TRIANGLE = 5
VTK_QUADRATIC_TRIANGLE = 22
FILE_WRITERS = {}

 
class PvdFile(object):
    def __init__(self, file_name):
        self.file_name = file_name
        self.timesteps = []
        
    def __lshift__(self, obj):
        t = n = len(self.timesteps)
        if isinstance(obj, tuple):
            obj, t = obj
        assert isinstance(obj, Function)
        
        # Inspect function space to find suitable writer
        V = obj.function_space()
        name = V.name
        gdim = V.mesh().geometry().dim()
        P = V.polynomial_degree
        key = (name, gdim, P)
        if key in FILE_WRITERS:
            writer = FILE_WRITERS[key]
        else:
            raise NotImplementedError('Do not know how to output %r' % (key,))
        
        # Write VTU
        vtu_file_name = '%s%06d.vtu' % (self.file_name[:-4], n)
        writer(vtu_file_name, obj)
        
        # Write PVD
        self.timesteps.append((t, vtu_file_name))
        write_pvd(self.file_name, self.timesteps)


def write_pvd(file_name, timesteps):
    with open(file_name, 'wt') as f:
        f.write('<?xml version="1.0"?>\n')
        f.write('<VTKFile type="Collection" version="0.1">\n')
        f.write('  <Collection>\n')
        for timestep, file_name in timesteps:
            f.write('    <DataSet timestep="%r" part="0" file="%s" />\n' % (timestep, file_name))
        f.write('  </Collection>\n')
        f.write('</VTKFile>\n')


def write_Lagrange_2D_function(file_name, function):
    """
    Write Lagrange spaces as VTK triangle cells VTK_TRIANGLE or 
    VTK_QUADRATIC_TRIANGLE (depending on the elements polynomial degree).
    """
    V = function.function_space()
    mesh = V.mesh()
    gdim = mesh.geometry().dim()
    P = V.polynomial_degree
    assert gdim == 2
    if P in (0, 1):
        verts_per_cell = 3
        vtk_cell_type = VTK_TRIANGLE
    else:
        verts_per_cell = 6
        vtk_cell_type = VTK_QUADRATIC_TRIANGLE
    
    topo = mesh.topology()
    ncells = len(topo.cell_to_vertex)
    nverts = ncells*verts_per_cell
    
    iv = 0
    connectivity = []
    
    with open(file_name, 'wt') as f:
        f.write('<?xml version="1.0"?>\n')
        f.write('<VTKFile type="UnstructuredGrid" version="0.1"  >\n')
        f.write('<UnstructuredGrid>\n')
        f.write('<Piece NumberOfPoints="%d" NumberOfCells="%d">\n' % (nverts, ncells))
        
        # Vertex information
        f.write('<Points>\n')
        f.write('<DataArray type="Float64" NumberOfComponents="3" format="ascii">')
        for cell in mesh.cells:
            verts = cell.vertices
            f.write(' '.join('%g %g 0' % tuple(v) for v in verts))
            if P in (0, 1):
                vertices = [iv, iv+1, iv+2]
                f.write(' ')
            else:
                vertices = [iv, iv+1, iv+2, iv+5, iv+3, iv+4]
                f.write(' %g %g 0' %  ((verts[1][0] + verts[2][0])/2, (verts[1][1] + verts[2][1])/2))
                f.write(' %g %g 0' %  ((verts[0][0] + verts[2][0])/2, (verts[0][1] + verts[2][1])/2))
                f.write(' %g %g 0 ' % ((verts[0][0] + verts[1][0])/2, (verts[0][1] + verts[1][1])/2))
            iv += len(vertices)
            connectivity.append(tuple(vertices))
        f.write('</DataArray>\n')
        f.write('</Points>\n')
        
        # Cell information
        f.write('<Cells>\n')
        f.write('<DataArray type="UInt32" Name="connectivity" format="ascii">')
        for ic in range(ncells):
            f.write(('%d '*verts_per_cell) % connectivity[ic])
        f.write('</DataArray>\n')
        
        f.write('<DataArray type="UInt32" Name="offsets" format="ascii">')
        for ic in range(ncells):
            f.write('%d ' % ((ic+1)*verts_per_cell))
        f.write('</DataArray>\n')
        
        f.write('<DataArray type="UInt8" Name="types" format="ascii">%s</DataArray>\n' % (('%d ' % vtk_cell_type) * ncells))
        f.write('</Cells>\n')
        
        # Function data
        if function.shape == ():
            # Output scalar function
            if P == 0:
                # Cell values
                f.write('<CellData Scalars="%s">\n' % function.name())
                f.write('<DataArray type="Float64" Name="%s" format="ascii">' % function.name())
                vec = function.vector()
                for ic in range(ncells):
                    cell_dofs = V.dofmap().cell_dofs(ic)
                    f.write(' '.join('%.16e ' % vec[dof] for dof in cell_dofs))
                    
                f.write('</DataArray>\n')
                f.write('</CellData>\n')
            else:
                # Vertex values
                f.write('<PointData Scalars="%s">\n' % function.name())
                f.write('<DataArray type="Float64" Name="%s" format="ascii">' % function.name())
                vec = function.vector()
                for ic in range(ncells):
                    cell_dofs = V.dofmap().cell_dofs(ic)
                    f.write(' '.join('%.16e ' % vec[dof] for dof in cell_dofs))
                f.write('</DataArray>\n')
                f.write('</PointData>\n')
        else:
            # Output vector function
            if P == 0:
                # Cell values
                f.write('<CellData Vectors="%s">\n' % function.name())
                f.write('<DataArray type="Float64" Name="%s" NumberOfComponents="3" format="ascii">' % function.name())
                vec = function.vector()
                for ic in range(ncells):
                    cd = []
                    for i in range(2):
                        cd.append(V.sub(i).dofmap().cell_dofs(ic))
                    for i in range(len(cd[0])):
                        f.write('%.16e ' % vec[cd[0][i]])
                        f.write('%.16e ' % vec[cd[1][i]])
                        f.write('%.16e ' % 0)
                f.write('</DataArray>\n')
                f.write('</CellData>\n')
            
            else:
                # Vertex values
                f.write('<PointData Vectors="%s">\n' % function.name())
                f.write('<DataArray type="Float64" Name="%s" NumberOfComponents="3" format="ascii">' % function.name())
                vec = function.vector()
                for ic in range(ncells):
                    cd = []
                    for i in range(2):
                        cd.append(V.sub(i).dofmap().cell_dofs(ic))
                    for i in range(len(cd[0])):
                        f.write('%.16e ' % vec[cd[0][i]])
                        f.write('%.16e ' % vec[cd[1][i]])
                        f.write('%.16e ' % 0)
                f.write('</DataArray>\n')
                f.write('</PointData>\n')
        f.write('</Piece>\n')
        f.write('</UnstructuredGrid>\n')
        f.write('</VTKFile>\n')


# Register Lagrange 2D writer
for name in ('Lagrange', 'Discontinuous Lagrange'):
    for degree in (0, 1, 2):
        FILE_WRITERS[(name, 2, degree)] = write_Lagrange_2D_function


def write_LagrangeTrace_2D_function(file_name, function):
    """
    Write trace spaces as VTK line cells VTK_LINE or VTK_QUADRATIC_EDGE
    (depending on the elements polynomial degree). 
    """
    V = function.function_space()
    mesh = V.mesh()
    P = V.polynomial_degree
    
    if P in (0, 1):
        verts_per_facet = 2
        vtk_cell_type = VTK_LINE
    else:
        verts_per_facet = 3
        vtk_cell_type = VTK_QUADRATIC_EDGE
    
    topo = mesh.topology()
    ncells = len(topo.cell_to_vertex)
    nfacets = len(topo.facet_to_vertex)
    nverts = nfacets*verts_per_facet
    
    iv = 0
    connectivity = []
    
    with open(file_name, 'wt') as f:
        f.write('<?xml version="1.0"?>\n')
        f.write('<VTKFile type="UnstructuredGrid" version="0.1"  >\n')
        f.write('<UnstructuredGrid>\n')
        f.write('<Piece NumberOfPoints="%d" NumberOfCells="%d">\n' % (nverts, nfacets))
        
        # Vertex information
        f.write('<Points>\n')
        f.write('<DataArray type="Float64" NumberOfComponents="3" format="ascii">')
        for facet in mesh.facets(internal=True, external=True):
            verts = facet.vertices
            f.write(' '.join('%g %g 0' % tuple(v) for v in verts))
            if P in (0, 1):
                vertices = [iv, iv+1]
                f.write(' ')
            else:
                vertices = [iv, iv+1, iv+2]
                f.write(' %g %g 0 ' % ((verts[0][0] + verts[1][0])/2, (verts[0][1] + verts[1][1])/2))
            iv += len(vertices)
            connectivity.append(tuple(vertices))
        f.write('</DataArray>\n')
        f.write('</Points>\n')
        
        # Cell information
        f.write('<Cells>\n')
        f.write('<DataArray type="UInt32" Name="connectivity" format="ascii">')
        for ifac in range(nfacets):
            f.write(('%d '*verts_per_facet) % connectivity[ifac])
        f.write('</DataArray>\n')
        
        f.write('<DataArray type="UInt32" Name="offsets" format="ascii">')
        for ifac in range(nfacets):
            f.write('%d ' % ((ifac+1)*verts_per_facet))
        f.write('</DataArray>\n')
        
        f.write('<DataArray type="UInt8" Name="types" format="ascii">%s</DataArray>\n' % (('%d ' % vtk_cell_type) * nfacets))
        f.write('</Cells>\n')
        
        # Find facet degrees of freedom
        if function.shape == ():
            dms = [V.dofmap()]
            is_scalar = True
        else:
            dms = [V.sub(0).dofmap(), V.sub(1).dofmap()]
            is_scalar = False
        
        vec = function.vector()
        values = []
        for ifac in range(nfacets):
            cell = mesh.cell(topo.facet_to_cell[ifac][0])
            i_loc = topo.cell_to_facet[cell.id].index(ifac)
            dofs = [d.cell_dofs(cell.id) for d in dms]
            for j in range(P+1):
                if is_scalar:
                    values.append(vec[dofs[0][i_loc*(P+1) + j]])
                else:
                    vx = vec[dofs[0][i_loc*(P+1) + j]]
                    vy = vec[dofs[1][i_loc*(P+1) + j]]
                    values.append((vx, vy))
        
        # Function data
        vtk_data_type = 'CellData' if P == 0 else 'PointData' 
        if is_scalar:
            # Output scalar function
            f.write('<%s Scalars="%s">\n' % (vtk_data_type, function.name()))
            f.write('<DataArray type="Float64" Name="%s" format="ascii">' % function.name())
            f.write(' '.join('%.16e ' % v for v in values))
            f.write('</DataArray>\n')
            f.write('</%s>\n' % vtk_data_type)
        
        else:
            # Output vector function
            f.write('<%s Vectors="%s">\n' % (vtk_data_type, function.name()))
            f.write('<DataArray type="Float64" Name="%s" NumberOfComponents="3" format="ascii">' % function.name())
            f.write(' '.join('%.16e %.16e 0' % (vx, vy) for vx, vy in values))
            f.write('</DataArray>\n')
            f.write('</%s>\n' % vtk_data_type)
        
        f.write('</Piece>\n')
        f.write('</UnstructuredGrid>\n')
        f.write('</VTKFile>\n')

for degree in (0, 1, 2):
    FILE_WRITERS[('Discontinuous Lagrange Trace', 2, degree)] = write_LagrangeTrace_2D_function
