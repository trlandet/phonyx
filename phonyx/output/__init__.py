from .pvd_file import PvdFile

def File(file_name):
    """
    Create an output file object for the file type
    specified by the extension in the file name.
    
    Currently only *.pvd files are supported  
    """
    if file_name.endswith('.pvd'):
        return PvdFile(file_name)
    else:
        raise NotImplementedError('Unsupported file type: %s' % file_name)
