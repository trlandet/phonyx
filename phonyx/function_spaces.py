"""
Function spaces

A function space links mesh entities with global degrees of freedom
"""
import numpy
from phonyx import elements, Timer


SYNONYMS = {'CG': 'Lagrange',
            'DG': 'Discontinuous Lagrange',
            'DGT': 'Discontinuous Lagrange Trace',
            'R': 'Real'}


class FunctionSpace(object):
    def __init__(self, mesh, name, polynomial_degree, parent_space=None):
        name = SYNONYMS.get(name, name)
        self._dofmap = None
        self._mesh = mesh
        self.name = name
        self.polynomial_degree = polynomial_degree
        self.parent_space = parent_space
            
        if name == 'Real':
            assert polynomial_degree == 0
            self.element = elements.Element_Real()
            
        elif name == 'Bubble':
            if mesh.dim != 2:
                raise NotImplementedError('Bubble only implemented in 2D')
            if polynomial_degree != 3:
                raise NotImplementedError('Bubble implemented only for order 3')
            self.element = elements.Element_Bubble_dim2_deg3()
        
        elif name == 'Discontinuous Lagrange Trace':
            if mesh.dim != 2:
                raise NotImplementedError('Discontinuous Lagrange Trace only implemented in 2D')
            
            if polynomial_degree == 0:
                self.element = elements.Element_LagrangeTrace_dim2_deg0()
            elif polynomial_degree == 1:
                self.element = elements.Element_LagrangeTrace_dim2_deg1()
            elif polynomial_degree == 2:
                self.element = elements.Element_LagrangeTrace_dim2_deg2()
            else:
                raise NotImplementedError('Discontinuous Lagrange Trace implemented only up to order 2')
            
        elif name == 'BDM Phi':
            assert mesh.dim == 2, 'BDM Phi only supports 2D meshes'
            assert polynomial_degree == 2, 'BDM Phi only supports degree 2 polynomials'
            self.element = elements.Element_BDM_Phi_dim2_deg2()
        
        elif name in ('Lagrange', 'Discontinuous Lagrange'):
            assert mesh.dim in (1, 2), 'Only 1D and 2D meshes are supported'
            assert polynomial_degree in (0, 1, 2), 'Only degree 1 & 2 elements are supported'
            if polynomial_degree == 0:
                assert name == 'Discontinuous Lagrange', 'Cannot construct zero order continuous space'
            
            if mesh.dim == 1:
                if polynomial_degree == 0:
                    self.element = elements.Element_Lagrange_dim1_deg0()
                elif polynomial_degree == 1:
                    self.element = elements.Element_Lagrange_dim1_deg1()
                elif polynomial_degree == 2:
                    self.element = elements.Element_Lagrange_dim1_deg2()
            elif mesh.dim == 2:
                if polynomial_degree == 0:
                    self.element = elements.Element_Lagrange_dim2_deg0()
                elif polynomial_degree == 1:
                    self.element = elements.Element_Lagrange_dim2_deg1()
                elif polynomial_degree == 2:
                    self.element = elements.Element_Lagrange_dim2_deg2()
        
        else:
            raise NotImplementedError('Unsupported element type %r' % name)
    
    def _init_dofmap(self, start_at=None):
        if start_at is None and self.parent_space is not None:
            # Ask the parent to call us back with the correct start_at number
            self.parent_space._init_dofmap()
        else:
            # We have the start_at number (or no parent), lets initialize
            self._dofmap = Dofmap(self, start_at)
    
    def dofmap(self):
        if self._dofmap is None:
            self._init_dofmap()
        return self._dofmap
    
    def dim(self):
        if self._dofmap is None:
            self._init_dofmap()
        return self._dofmap.dim

    
    def mesh(self):
        return self._mesh

    def num_sub_spaces(self):
        return 0
    
    def ufl_element(self):
        return elements.UflElement(self.name, self.polynomial_degree)
    
    def __repr__(self):
        return 'FunctionSpace(%r, %r)' % (self.name, self.polynomial_degree) 


class Dofmap(object):
    def __init__(self, function_space, start_at=0):
        self.function_space = function_space
        self.coordinate_dofmap = {}
        self.cell_dofmap = {}
        self.start_at = 0 if start_at is None else start_at
        
        timer = Timer('Init dofmap')
        if function_space.name == 'Real':
            self.create_dofmap_real()
        elif function_space.name == 'Discontinuous Lagrange':
            self.create_dofmap_discontinuous_lagrange()
        elif function_space.name == 'Discontinuous Lagrange Trace':
            self.create_dofmap_discontinuous_lagrange_trace()
        elif function_space.name == 'Lagrange':
            self.create_dofmap_lagrange()
        else:
            self.create_dofmap_generic()
        timer.stop()
    
    def create_dofmap_real(self):
        """
        Generic dofmap numbering routine for Lagrange multiplier (Real) element
        """
        mesh = self.function_space.mesh()
        
        dof = self.start_at
        for cell in mesh.cells:
            self.cell_dofmap[cell.id] = (dof,)
        
        # Like dolfin, lets use the midpoint of the last cell ...
        coord = cell.midpoint().coordinates()
        self.coordinate_dofmap[tuple(coord)] = (dof,)
        self.dim = 1
        
    def create_dofmap_discontinuous_lagrange(self):
        """
        Create dofmap for discontinuous lagrange function spaces
        """
        dof = self.start_at
        
        mesh = self.function_space.mesh()
        element = self.function_space.element
        
        # Find all degrees of freedom
        for cell in mesh.cells:
            cell_dofs = [] 
            for i in range(element.num_basis_functions):
                coord = element.get_coordinate_for_basis_function(i, cell)
                self.coordinate_dofmap.setdefault(tuple(coord), []).append(dof)
                cell_dofs.append(dof)
                dof += 1
            self.cell_dofmap[cell.id] = tuple(cell_dofs)
        
        self.dim = dof - self.start_at
    
    def create_dofmap_discontinuous_lagrange_trace(self):
        """
        Create dofmap for discontinuous lagrange trace function spaces
        """
        next_dof = self.start_at
        
        mesh = self.function_space.mesh()
        element = self.function_space.element
        N_basis = element.num_basis_functions
        P = element.order
        topo = mesh.topology()
        
        tmp = {}
        
        # Find all degrees of freedom
        for cell in mesh.cells:
            cell_dofs = [] 
            cell_facets = topo.cell_to_facet[cell.id]
            
            for i in range(N_basis): 
                i_facet_local = i // (P+1)
                i_facet = cell_facets[i_facet_local]
                coord = element.get_coordinate_for_basis_function(i, cell)
                tcoord = tuple(coord)
                
                # Get the dof number
                key = (i_facet, tcoord)
                if key not in tmp:
    
    
                    tmp[key] = next_dof
                    next_dof += 1
                dof = tmp[key]
                
                self.coordinate_dofmap.setdefault(tcoord, []).append(dof)
                cell_dofs.append(dof)
            
            self.cell_dofmap[cell.id] = tuple(cell_dofs)
        
        self.dim = next_dof - self.start_at
    
    def create_dofmap_lagrange(self):
        """
        Create a dofmap where dof_number == vertex_number
        which is compatible with Lagrange order 1 elements
        in dolfin. Second order dofs (mid-facet) are 
        numbered after the first order
        """
        offset = self.start_at
        mesh = self.function_space.mesh()
        
        # Number first order dofs (same as mesh vertex number of linked mesh vertex)
        for cell in mesh.cells:
            cid = cell.id
            dofs = [d + offset for d in mesh.topology().cell_to_vertex[cid]]
            self.cell_dofmap[cid] = dofs
            
        verts = mesh.vertices
        for i, v in enumerate(verts):
            self.coordinate_dofmap[tuple(v)] = (i + offset,)
            
        self.dim = len(verts)
        
        if self.function_space.polynomial_degree == 1:
            return
        
        # Number second order dofs (not linked to mesh vertices)
        assert self.function_space.polynomial_degree == 2
        
        element = self.function_space.element
        next_dof = self.dim + offset
        if mesh.dim == 1:
            for cell in mesh.cells:
                coord = element.get_coordinate_for_basis_function(2, cell)
                self.cell_dofmap[cell.id].append(next_dof)
                self.coordinate_dofmap[tuple(coord)] = (next_dof,)
                next_dof += 1
        
        elif mesh.dim == 2:
            for cell in mesh.cells:
                for i in range(3):
                    coord = element.get_coordinate_for_basis_function(3 + i, cell)
                    key = tuple(coord)
                    if not key in self.coordinate_dofmap:
                        self.coordinate_dofmap[key] = (next_dof,)
                        next_dof += 1
                    dof = self.coordinate_dofmap[key][0]
                    self.cell_dofmap[cell.id].append(dof)
        
        self.dim = next_dof - offset
    
    def create_dofmap_generic(self):
        """
        Generic dofmap numbering routine
        """
        dof = self.start_at
        
        mesh = self.function_space.mesh()
        element = self.function_space.element
        
        # Find all degrees of freedom
        for cell in mesh.cells:
            cell_dofs = [] 
            for i in range(element.num_basis_functions):
                coord = element.get_coordinate_for_basis_function(i, cell)
                key = tuple(coord)
                if key not in self.coordinate_dofmap:
                    self.coordinate_dofmap[key] = (dof,)
                    dof += 1
                cell_dofs.append(self.coordinate_dofmap[key][0])
            self.cell_dofmap[cell.id] = tuple(cell_dofs)
        
        self.dim = dof - self.start_at
    
    def cell_dofs(self, cell_id):
        """
        Get the DOFs that are associated with the given cell
        """
        return self.cell_dofmap[cell_id]
    
    def tabulate_all_coordinates(self, mesh=None):
        """
        Return coordinates of all dofs in flat array (x0, y0, x1, y1, . . .)
        """
        if mesh is None:
            mesh = self.function_space.mesh()
        gdim = mesh.dim
        coords = numpy.zeros(self.dim*gdim)
        for coord, dofs in self.coordinate_dofmap.items():
            for dof in dofs:
                d = dof - self.start_at
                coords[d*gdim:(d+1)*gdim] = coord
        return coords
    
    def tabulate_all_coordinates_with_cell_ids(self, mesh=None):
        """
        Return tuples of cell ids and coordinate of all dofs
        For dofs involving multiple cells a random one is returned
        """
        if mesh is None:
            mesh = self.function_space.mesh()
        
        # Find one cell for each dof (we do not care which cell)    
        cell_id_for_dof = {}
        for cell_id, dofs in self.cell_dofmap.items():
            for dof in dofs:
                cell_id_for_dof[dof] =  cell_id
        
        # Find cell id and coordinate for each dof
        ret = [None]*self.dim
        for coord, dofs in self.coordinate_dofmap.items():
            for dof in dofs:
                d = dof - self.start_at
                ret[d] = (cell_id_for_dof[dof], coord)
        return ret


class MixedFunctionSpace(object):
    def __init__(self, funcspaces, parent_space=None):
        self._dofmap = None
        self._mesh = get_single_mesh(funcspaces)
        self.polynomial_degree = get_max_degree(funcspaces)
        self.parent_space = parent_space
        self.sub_spaces = []
        
        for V in funcspaces:
            if isinstance(V, VectorFunctionSpace):
                Vsub = V.sub_spaces[0]
                V2 = VectorFunctionSpace(Vsub.mesh(), Vsub.name, Vsub.polynomial_degree, parent_space=self)
            else: 
                V2 = FunctionSpace(V.mesh(), V.name, V.polynomial_degree, parent_space=self)
            
            self.sub_spaces.append(V2)
    
    def _init_dofmap(self, start_at=None):
        if start_at is None and self.parent_space is not None: 
            # Ask the parent to call us back with the correct start_at number
            self.parent_space._init_dofmap()
        else:
            # We have the start_at number (or no parent), lets initialize
            self._dofmap = MixedDofmap(self, start_at)
            
            for V in self.sub_spaces:
                start_at = self._dofmap.next_id
                V._init_dofmap(start_at)
                self._dofmap.next_id += V.dim()
    
    def num_sub_spaces(self):
        return len(self.sub_spaces)
    
    def sub(self, i):
        return self.sub_spaces[i]

    def dofmap(self):
        if self._dofmap is None:
            self._init_dofmap()
        return self._dofmap
    
    def mesh(self):
        return self._mesh
    
    def dim(self):
        if self._dofmap is None:
            self._init_dofmap()
        return self._dofmap.dim


class VectorFunctionSpace(MixedFunctionSpace):
    def __init__(self, mesh, name, polynomial_degree, parent_space=None):
        """
        A mixed function space with identical sub spaces for each geometrical
        dimmension in the mesh. The test and trial functions vector valued.
        """
        name = SYNONYMS.get(name, name)
        subspaces = [FunctionSpace(mesh, name, polynomial_degree) for _ in range(mesh.dim)]
        super(VectorFunctionSpace, self).__init__(subspaces, parent_space=parent_space)
        self.name = name
        self.polynomial_degree = polynomial_degree
    
    def ufl_element(self):
        Nse = len(self.sub_spaces) 
        shape = (Nse,)
        V = self.sub_spaces[0]
        return elements.UflElement(V.name, V.polynomial_degree, shape=shape, num_sub_elements=Nse)
    
    def __repr__(self):
        return 'VectorFunctionSpace(%r, %r)' % (self.sub_spaces[0].name, self.polynomial_degree) 


class MixedDofmap(object):
    def __init__(self, function_space, start_at=0):
        self.function_space = function_space
        self.start_at = 0 if start_at is None else start_at
        self.next_id = self.start_at
    
    @property
    def dim(self):
        return self.next_id - self.start_at
    
    def cell_dofs(self, cell_id):
        """
        Get the DOFs that are associated with the given cell
        """
        dofs = []
        V = self.function_space
        for Vs in V.sub_spaces:
            dofs.extend(Vs.dofmap().cell_dofs(cell_id))
        return dofs


def leaf_function_spaces(func_space, max_depth=10000):
    """
    Return a list of leaf (non-mixed) function spaces in the 
    (possibly hirarchy of mixed) input function space
    """
    if isinstance(func_space, FunctionSpace) or max_depth == 0:
        # This is a leaf (non-mixed)
        return [func_space]
    else:
        # Gather leafs from children
        return sum((leaf_function_spaces(V, max_depth-1) for V in func_space.sub_spaces), [])


def get_single_mesh(funcspaces):
    """
    Assert that the mesh of all sub function spaces are the same
    """
    meshes = set()
    for V in funcspaces:
        for Vsub in leaf_function_spaces(V):
            meshes.add(Vsub.mesh())
    assert len(meshes) == 1
    return meshes.pop()


def get_max_degree(funcspaces):
    """
    Look through sub function spaces and find the maximum degree 
    """
    degree = 0
    for V in funcspaces:
        for Vsub in leaf_function_spaces(V):
            degree = max(degree, Vsub.polynomial_degree)
    return degree


def get_function_space_with_first_dof_at_0(V):
    """
    Return a copy of V such that the copy's dofmap starts at 0
    which it will not generally do if it is a part of a mixed form
    """
    if V.dofmap().start_at == 0:
        return V
    elif isinstance(V, FunctionSpace):
        return FunctionSpace(V.mesh(), V.name, V.polynomial_degree)
    elif  isinstance(V, VectorFunctionSpace):
        Vsub = V.sub_spaces[0]
        return VectorFunctionSpace(Vsub.mesh(), Vsub.name, Vsub.polynomial_degree)
    else:
        raise NotImplementedError('get_function_space_with_first_dof_at_0 does not work (yet) if called with a general mixed function space')
