from __future__ import division
import numpy
from phonyx import *
from .helpers import print_nice_matrix


def test_facet_marks():
    class AllBoundaries(SubDomain):
        def inside(self, x, on_boundary):
            #print x, on_boundary
            return on_boundary
    
    class LeftBoundary(SubDomain):
        def inside(self, x, on_boundary):
            #print x, on_boundary
            return on_boundary and near(x[0], 0)
    
    all_boundaries = AllBoundaries()
    left_boundary = LeftBoundary()
    
    # 1D ###########################################
    N = 4
    mesh = UnitIntervalMesh(N)
    func = FacetFunction('size_t', mesh)
    
    all_boundaries.mark(func, 3)
    marks = func.array()
    correct = numpy.array([3, 0, 0, 0, 3], int)
    print 'dolfin:', print_nice_matrix(correct, show=False)
    print 'phonyx:', print_nice_matrix(marks, correct, show=False)
    assert (marks == correct).all()
    
    func.set_all(0)
    left_boundary.mark(func, 5)
    marks = func.array()
    correct = numpy.array([5, 0, 0, 0, 0], int)
    print 'dolfin:', print_nice_matrix(correct, show=False)
    print 'phonyx:', print_nice_matrix(marks, correct, show=False)
    assert (marks == correct).all()
    
    # 2D ###########################################
    N = 2
    mesh = UnitSquareMesh(N, N, 'right')
    func = FacetFunction('size_t', mesh)
    
    all_boundaries.mark(func, 3)
    marks = func.array()
    correct = numpy.array([0, 0, 3, 0, 3, 3, 0, 3, 0, 0, 0, 3, 3, 3, 0, 3], int)
    print 'dolfin:', print_nice_matrix(correct, show=False)
    print 'phonyx:', print_nice_matrix(marks, correct, show=False)
    assert (marks == correct).all()
    
    func.set_all(0)
    left_boundary.mark(func, 4)
    marks = func.array()
    correct = numpy.array([0, 0, 0, 0, 4, 0, 0, 0, 0, 0, 0, 0, 4, 0, 0, 0], int)
    print 'dolfin:', print_nice_matrix(correct, show=False)
    print 'phonyx:', print_nice_matrix(marks, correct, show=False)
    assert (marks == correct).all()


def test_subdomain_facet_integral():
    N = 2
    mesh = UnitSquareMesh(N, N, 'right')
    V = FunctionSpace(mesh, 'CG', 1)
    u = TrialFunction(V)
    v = TestFunction(V)
    
    class LeftTopBoundary(SubDomain):
        def inside(self, x, on_boundary):
            #print x, on_boundary
            return on_boundary and (near(x[0], 0) or near(x[1], 1))
    
    left_top_boundary = LeftTopBoundary()
    func = FacetFunction('size_t', mesh)
    left_top_boundary.mark(func, 1)
    ds = Measure('ds')[func](1)
    
    a = u*v*ds
    A = assemble(a).array()
    Aa = numpy.array([[ 1/6,    0,    0, 1/12,    0,    0,    0,    0,    0],
                      [   0,    0,    0,    0,    0,    0,    0,    0,    0],
                      [   0,    0,    0,    0,    0,    0,    0,    0,    0],
                      [1/12,    0,    0,  1/3,    0,    0, 1/12,    0,    0],
                      [   0,    0,    0,    0,    0,    0,    0,    0,    0],
                      [   0,    0,    0,    0,    0,    0,    0,    0,    0],
                      [   0,    0,    0, 1/12,    0,    0,  1/3, 1/12,    0],
                      [   0,    0,    0,    0,    0,    0, 1/12,  1/3, 1/12],
                      [   0,    0,    0,    0,    0,    0,    0, 1/12,  1/6]])
    error = abs(A-Aa).sum()
    
    print 'dolfin:\n', print_nice_matrix(Aa, show=False)
    print 'phonyx:\n', print_nice_matrix(A, Aa, show=False)
    print 'error:', error
    
    assert error < 1e-15
