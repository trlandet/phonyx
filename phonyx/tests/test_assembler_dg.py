from __future__ import division
import numpy
from phonyx import *
from .helpers import print_nice_matrix, save_test_data, load_test_data


def test_assemble_dS_1d():
    N = 3
    mesh = IntervalMesh(N, 0, 18)
    V = FunctionSpace(mesh, 'DG', 1)
    u = TrialFunction(V)
    v = TestFunction(V)
    
    # Check coordinates
    coords = V.dofmap().tabulate_all_coordinates(mesh)
    print coords
    assert  (coords == [0, 6, 6, 12, 12, 18]).all()
    
    # Check assembled matrix
    a = avg(u)*jump(v)*dS
    A = assemble(a).array()
    Aa = numpy.array([[  0,    0,    0,    0,    0,    0],
                      [  0,  1/2,  1/2,    0,    0,    0],
                      [  0, -1/2, -1/2,    0,    0,    0],
                      [  0,    0,    0,  1/2,  1/2,    0],
                      [  0,    0,    0, -1/2, -1/2,    0],
                      [  0,    0,    0,    0,    0,    0]])
    
    print 'Aa:\n', print_nice_matrix(Aa, show=False)
    print 'A:\n', print_nice_matrix(A, Aa, show=False)
    err_A = abs(A-Aa).sum()
    print 'err_A:\n', err_A
    assert err_A < 1e-15
    
    # Check assembled vector
    L = Constant(3)*jump(v)*dS
    b = assemble(L).array()
    ba = numpy.array([0, 3, -3, 3, -3, 0])
    
    print 'ba:\n', print_nice_matrix(ba, show=False)
    print 'b:\n', print_nice_matrix(b, ba, show=False)
    err_b = abs(b - ba).sum()
    print 'err_b:', err_b
    assert err_b < 1e-15


def test_assemble_dS_2d():
    N, Pu, Pp = 2, 2, 1
    
    class DirichletRegion(SubDomain):
        def inside(self, x, on_boundary):
            return on_boundary
    
    mesh = UnitSquareMesh(N, N, 'right')
    n = FacetNormal(mesh)
    V = VectorFunctionSpace(mesh, 'DG', Pu)
    Q = FunctionSpace(mesh, 'DG', Pp)
    Vlm = FunctionSpace(mesh, 'R', 0)
    W = MixedFunctionSpace([V, Q, Vlm])
    u, p, lm_trial = TrialFunctions(W)
    v, q, lm_test = TestFunctions(W)
    
    # Mark boundary
    marker = FacetFunction("size_t", mesh)
    dr = DirichletRegion()
    dr.mark(marker, 1)
    ds2 = Measure('ds')[marker]
    
    # Define the right hand side and the analytical solutions
    f = Expression(['pi*pi*sin(pi*x[1]) + 2*pi*cos(2*pi*x[0])',
                    'pi*pi*cos(pi*x[0])'])
    ue = Expression(['sin(pi*x[1])', 'cos(pi*x[0])'])
    pe = Expression('sin(2*pi*x[0])')
    
    # Interpolate to CG1 elements for easier comparison with dolfin
    # Normally Phonyx will be more accurate, but for comparison purposes
    # forcing compatibility by interpolation is easiest
    V1 = VectorFunctionSpace(mesh, 'CG', 2)
    f = interpolate(f, V1)
    ue = interpolate(ue, V1)
    
    # Arbitrary positive SIP penalty - we will not solve this system anyway 
    penalty_dS = Constant(1)
    penalty_ds = Constant(1)
    
    # RHS
    eq = -dot(f, v)*dx
    
    # Lagrange multiplicator to remove the pressure null space
    eq += (p*lm_test + q*lm_trial)*dx
    
    # Momentum equations
    for d in range(2):
        # Divergence free criterion
        eq -= u[d].dx(d)*q*dx
        eq += avg(q)*jump(u[d])*n[d]('+')*dS
        
        # Diffusion:
        eq += dot(grad(u[d]), grad(v[d]))*dx
        
        # Symmetric Interior Penalty symmetrisation terms
        eq -= dot(n('+'), avg(grad(u[d])))*jump(v[d])*dS
        eq -= dot(n('+'), avg(grad(v[d])))*jump(u[d])*dS
        
        # Symmetric Interior Penalty coercivity term
        eq += penalty_dS*jump(u[d])*jump(v[d])*dS
        
        # Pressure
        eq -= v[d].dx(d)*p*dx
        eq += avg(p)*jump(v[d])*n[d]('+')*dS
        
        # Dirichlet boundary
        for dval, dds in ((ue, ds2(1)), ):
            # Divergence free criterion
            eq += q*(u[d] - dval[d])*n[d]*dds
            
            # SIPG
            eq -= dot(n, grad(u[d]))*v[d]*dds
            eq -= dot(n, grad(v[d]))*u[d]*dds
            eq += dot(n, grad(v[d]))*dval[d]*dds
            
            # Weak Dirichlet
            eq += penalty_ds*(u[d] - dval[d])*v[d]*dds
            
            # Pressure
            eq += p*v[d]*n[d]*dds
    
    a, L = system(eq)
    
    A = assemble(a).array()
    b = assemble(L).array()
    
    # This matrix was to large to put directly in the test code 
    if assemble.__module__.startswith('dolfin'):
        save_test_data('test_assemble_dS_2d_A', A)
        save_test_data('test_assemble_dS_2d_b', b)
    Aa = load_test_data('test_assemble_dS_2d_A')
    ba = load_test_data('test_assemble_dS_2d_b')
    
    #print 'dolfin:\n', print_nice_matrix(Aa, show=False)
    #print 'phonyx:\n', print_nice_matrix(A, Aa, show=False)
    
    errorA = abs(A-Aa).sum()
    errorb = abs(b-ba).sum()
    print 'errorA:', errorA
    print 'errorb:', errorb
    assert errorA < 3e-12 and errorb < 2e-13
