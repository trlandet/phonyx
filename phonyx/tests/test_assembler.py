from __future__ import division
import numpy
from phonyx import IntervalMesh, FunctionSpace
from phonyx import TrialFunction, TestFunction, Expression, dx, ds, FacetNormal
from phonyx import assemble


def test_assemble_mass_matrix_1D():
    mesh = IntervalMesh(3, 0, 1)
    V = FunctionSpace(mesh, 'CG', 1)
    u = TrialFunction(V)
    v = TestFunction(V)
    form = u*v*dx
    
    A = assemble(form)
    print 'A:\n', A.array()
    
    # Check matrix vs the analytical answer
    Aa = numpy.array([[ 1.0/9, 1.0/18,      0,      0],
                      [1.0/18,  2.0/9, 1.0/18,      0],
                      [     0, 1.0/18,  2.0/9, 1.0/18],
                      [     0,      0, 1.0/18,  1.0/9]])
    print 'Aa:\n', Aa
    abs_err = abs(A.array()-Aa)
    print 'error:\n', abs_err
    
    assert abs_err.sum() < 1e-14


def test_assemble_stiffness_matrix_1D():
    mesh = IntervalMesh(3, 0, 1)
    V = FunctionSpace(mesh, 'CG', 1)
    u = TrialFunction(V)
    v = TestFunction(V)
    form = u.dx(0)*v.dx(0)*dx
    
    A = assemble(form)
    print 'A:\n', A.array()
    
    # Check matrix vs the analytical answer
    Aa = numpy.array([[ 3.0, -3.0,  0.0,  0.],
                      [-3.0,  6.0, -3.0,  0.],
                      [ 0.0, -3.0,  6.0, -3.],
                      [ 0.0,  0.0, -3.0,  3.]])
    print 'Aa:\n', Aa
    abs_err = abs(A.array()-Aa)
    print 'error:\n', abs_err
    
    assert abs_err.sum() < 1e-14


def test_assemble_vector_with_expression():
    N = 11
    
    # Construct analytical solution
    x = numpy.linspace(0, 1, N+1)
    y = numpy.cos(numpy.pi*x)[1:-1]/N

    mesh = IntervalMesh(N, 0, 1)
    V = FunctionSpace(mesh, 'CG', 1)
    v = TestFunction(V)
    
    e = Expression('cos(pi*x[0])', degree=1)
    b = assemble(e*v*dx)
    print b.array()
    
    error = abs(b.array()[1:-1]-y).sum()
    print error
    
    assert error < 0.005


def test_assemble_ds():
    N = 3
    mesh = IntervalMesh(N, 0, 1)
    V = FunctionSpace(mesh, 'CG', 1)
    u = TrialFunction(V)
    v = TestFunction(V)
    n = FacetNormal(mesh)
    
    M = assemble(u*v.dx(0)*n[0]*ds)
    print 'M:\n', M.array()
    
    # Check matrix vs the analytical answer
    Ma = numpy.array([[ 3.0, 0.0, 0.0,  0.0],
                      [-3.0, 0.0, 0.0,  0.0],
                      [ 0.0, 0.0, 0.0, -3.0],
                      [ 0.0, 0.0, 0.0,  3.0]])
    
    print 'Ma:\n', Ma
    
    abs_err = abs(M.array() - Ma)
    print 'error:\n', abs_err
    assert abs_err.sum() < 1e-14
