from phonyx import *

def test_local_assembly_of_vectors():
    mesh = UnitSquareMesh(2, 2, 'right')
    V = FunctionSpace(mesh, 'DG', 1)
    v = TestFunction(V)
    
    a = v*dx + v*ds + avg(v)*dS
    bg = assemble(a, use_local_assembler=False).array()
    bl = assemble(a, use_local_assembler=True).array()
    error = abs(bg-bl).sum()
    print bg
    print bl
    print error
    assert error < 1e-15
