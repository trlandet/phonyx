import os
import numpy
try:
    import sympy
    has_sympy = True 
except ImportError:
    has_sympy = False


def prettify_number(x):
    """
    Return pretty string representation of number 
    """
    rounded = '%.4g' % x
    if not has_sympy:
        return rounded
    simplified = str(sympy.nsimplify(x))
    return simplified if len(simplified) <= len(rounded) else rounded  


def print_nice_matrix(A, A_target=None, show=True, eps=1e-8, round_zero=False):
    """
    Print the matrix A. Sympy is used to turn 0.33333333 into '1/3'
    
    If A_target is not None then the number A[i,j] is printed in 
    red colour if A[i,j] is not equal A_target[i,j]
    """
    COLOR_FAIL = '\033[91m'
    COLOR_ENDC = '\033[0m'
    
    A = numpy.asarray(A)
    if A.ndim == 1:
        A = A.reshape((1, A.shape[0]))
    
    if A_target is not None:
        A_target = numpy.asarray(A_target)
        if A_target.ndim == 1:
            A_target = A_target.reshape((1, A_target.shape[0])) 
    
    strings = []
    maxlen = [0]*A.shape[1]
    for row in A:
        strings.append([])
        for i, num in enumerate(row):
            if round_zero and abs(num) < eps:
                num = 0
            nicenum = prettify_number(num)
            maxlen[i] = max(maxlen[i], len(nicenum))
            strings[-1].append(nicenum)
    
    # Mark differences and format entries to correct length
    fixlen = ['%%%ds' % m for m in maxlen]
    for i, row in enumerate(A):
        for j, num in enumerate(row):
            s = fixlen[j] % strings[i][j]
            if A_target is not None and abs(num - A_target[i,j]) > eps:
                s = COLOR_FAIL + s + COLOR_ENDC
            strings[i][j] = s
    
    
    if A.shape[0] > 1:
        res = '\n '.join('[%s],' % ', '.join(row) for row in strings)[:-1]
        res = '[' + res + ']'
    else:
        res = '\n'.join('[%s],' % ', '.join(row) for row in strings)[:-1]
    
    if show:
        print res
    
    return res
        

def save_test_data(key, value):
    """
    Save matrix or vector to file
    
    Some matrices and vectors are too large to comfortably put in the
    Python code for the unit tests. These will be saved to an ASCII file
    which contains a very simple data base indexed by the data name
    """
    assert key.split()[0] == key, 'Key must not contain spaces'
    
    # Read the allready present data
    datafile = os.path.join(os.path.dirname(__file__), 'test_data.txt')
    with open(datafile, 'rt') as f:
        lines = f.readlines()
        
    def myrepr(number):
        #return '%25.17e' % number
        number = repr(number)
        if number[-2:] == '.0':
            return number[:-2]
        else:
            return number
    
    # Write data back to file
    # Remove any existing data with same name and add the new data to the end
    with open(datafile, 'wt') as f:
        for line in lines:
            if line[:200].split()[0] == key:
                continue
            else:
                f.write(line)
        
        # Add the new data
        txt_shape = '_'.join('%d' % v for v in value.shape)
        txt_data = ' '.join(myrepr(v) for v in numpy.ravel(value))
        line = '%s %s %s\n' % (key, txt_shape, txt_data)
        f.write(line)


def load_test_data(key):
    """
    Load matrix or vector from file
    
    Some matrices and vectors are too large to comfortably put in the
    Python code for the unit tests. These will be saved to an ASCII file
    which contains a very simple data base indexed by the data name
    """
    assert key.split()[0] == key, 'Key must not contain spaces'
    
    # Read the allready present data
    datafile = os.path.join(os.path.dirname(__file__), 'test_data.txt')
    for line in open(datafile):
        if line[:200].split()[0] == key:
            words = line.split()
            shape = tuple(int(v) for v in words[1].split('_'))
            return numpy.array([float(v) for v in words[2:]]).reshape(shape)


# These are not tests
save_test_data.__test__ = False
load_test_data.__test__ = False
