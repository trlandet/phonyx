from __future__ import division
import numpy
from phonyx import *
from .helpers import print_nice_matrix


def test_assemble_dgt_2D_matrix():
    N = 1
    mesh = UnitSquareMesh(N, N, 'right')
    
    ###############################################################################################################################
    V = FunctionSpace(mesh, 'DGT', 0)
    u = TrialFunction(V)
    v = TestFunction(V)
    a_m = u('+')*v('+')*dS + u('-')*v('-')*dS + u*v*ds
    M = assemble(a_m).array()
    Md = numpy.array([[1,     0, 0, 0, 0],
                      [0, 2.828, 0, 0, 0],
                      [0,     0, 1, 0, 0],
                      [0,     0, 0, 1, 0],
                      [0,     0, 0, 0, 1]])
    print_nice_matrix(M, Md, eps=1e-4)
    err_M = abs(M - Md).sum()
    print 'err_M', err_M
    assert err_M  < 5e-4 # high due to imprecise dS entry (1,1) in Md matrix
    
    
    ###############################################################################################################################
    V = FunctionSpace(mesh, 'DGT', 1)
    u = TrialFunction(V)
    v = TestFunction(V)
    a_m = u('+')*v('+')*dS + u('-')*v('-')*dS + u*v*ds
    M = assemble(a_m).array()
    Md = numpy.array([[1/3, 1/6,      0,      0,   0,   0,   0,   0,   0,   0],
                      [1/6, 1/3,      0,      0,   0,   0,   0,   0,   0,   0],
                      [  0,   0, 0.9428, 0.4714,   0,   0,   0,   0,   0,   0],
                      [  0,   0, 0.4714, 0.9428,   0,   0,   0,   0,   0,   0],
                      [  0,   0,      0,      0, 1/3, 1/6,   0,   0,   0,   0],
                      [  0,   0,      0,      0, 1/6, 1/3,   0,   0,   0,   0],
                      [  0,   0,      0,      0,   0,   0, 1/3, 1/6,   0,   0],
                      [  0,   0,      0,      0,   0,   0, 1/6, 1/3,   0,   0],
                      [  0,   0,      0,      0,   0,   0,   0,   0, 1/3, 1/6],
                      [  0,   0,      0,      0,   0,   0,   0,   0, 1/6, 1/3]])
    print_nice_matrix(M, Md, eps=1e-4)
    err_M = abs(M - Md).sum()
    print 'err_M', err_M
    assert err_M  < 3e-5 # high due to imprecise dS block in Md matrix
    
    
    ###############################################################################################################################
    V = FunctionSpace(mesh, 'DGT', 2)
    u = TrialFunction(V)
    v = TestFunction(V)
    a_m = u('+')*v('+')*dS + u('-')*v('-')*dS + u*v*ds
    M = assemble(a_m).array()
    Md = numpy.array([[ 2/15, -1/30, 1/15,        0,        0,      0,     0,     0,    0,     0,     0,    0,     0,     0,    0],
                      [-1/30,  2/15, 1/15,        0,        0,      0,     0,     0,    0,     0,     0,    0,     0,     0,    0],
                      [ 1/15,  1/15, 8/15,        0,        0,      0,     0,     0,    0,     0,     0,    0,     0,     0,    0],
                      [    0,     0,    0,   0.3771, -0.09428, 0.1886,     0,     0,    0,     0,     0,    0,     0,     0,    0],
                      [    0,     0,    0, -0.09428,   0.3771, 0.1886,     0,     0,    0,     0,     0,    0,     0,     0,    0],
                      [    0,     0,    0,   0.1886,   0.1886, 1.5085,     0,     0,    0,     0,     0,    0,     0,     0,    0],
                      [    0,     0,    0,        0,        0,      0,  2/15, -1/30, 1/15,     0,     0,    0,     0,     0,    0],
                      [    0,     0,    0,        0,        0,      0, -1/30,  2/15, 1/15,     0,     0,    0,     0,     0,    0],
                      [    0,     0,    0,        0,        0,      0,  1/15,  1/15, 8/15,     0,     0,    0,     0,     0,    0],
                      [    0,     0,    0,        0,        0,      0,     0,     0,    0,  2/15, -1/30, 1/15,     0,     0,    0],
                      [    0,     0,    0,        0,        0,      0,     0,     0,    0, -1/30,  2/15, 1/15,     0,     0,    0],
                      [    0,     0,    0,        0,        0,      0,     0,     0,    0,  1/15,  1/15, 8/15,     0,     0,    0],
                      [    0,     0,    0,        0,        0,      0,     0,     0,    0,     0,     0,    0,  2/15, -1/30, 1/15],
                      [    0,     0,    0,        0,        0,      0,     0,     0,    0,     0,     0,    0, -1/30,  2/15, 1/15],
                      [    0,     0,    0,        0,        0,      0,     0,     0,    0,     0,     0,    0,  1/15,  1/15, 8/15]])
    print_nice_matrix(M, Md, eps=1e-4)
    err_M = abs(M - Md).sum()
    print 'err_M', err_M
    assert err_M  < 3e-4 # high due to imprecise dS block in Md matrix


def test_assemble_dgt_2D_vector():
    N = 1
    mesh = UnitSquareMesh(N, N, 'right')
    V = VectorFunctionSpace(mesh, 'DGT', 0)
    v = TestFunction(V)
    
    # Create checkerboard fucntion that is parallel to internal facets
    # The vector field is [1, 1] and [2, 2] in alternating cells
    Vdg = VectorFunctionSpace(mesh, 'DG', V.ufl_element().degree())
    w = Function(Vdg)
    for cell in cells(mesh):
        idx = cell.index()
        cell_value = 1 + (idx % 2)
        for dof in Vdg.dofmap().cell_dofs(idx):
            w.vector()[dof] = cell_value
    
    n = FacetNormal(mesh)
    flux_uw = (dot(w, n) + abs(dot(w, n)))/2*n
    flux = flux_uw('+') + flux_uw('-')
    
    L = inner(flux, v('+'))*dS
    b = assemble(L).array()
    
    bd = numpy.zeros_like(b)
    print_nice_matrix(b, bd)
    err_b = abs(b-bd).sum()
    print 'err_b', err_b
    assert err_b  < 1e-15
