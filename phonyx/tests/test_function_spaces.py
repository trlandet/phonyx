from phonyx import IntervalMesh, FunctionSpace

def test_function_space_dimension():
    mesh = IntervalMesh(10, 0, 1)
    V = FunctionSpace(mesh, 'CG', 1)
    print V.dim()
    assert V.dim() == 11
