from phonyx.tests.helpers import print_nice_matrix
from phonyx import *
import numpy

def test_BDM_projection():
    mesh = UnitSquareMesh(1, 1)

    V = VectorFunctionSpace(mesh, 'DG', 2)
    u = TrialFunction(V)
    
    # The mixed function space of the BDM test functions
    V1 = FunctionSpace(mesh, 'DGT', 2)
    V2 = VectorFunctionSpace(mesh, 'DG', 0)
    V3 = FunctionSpace(mesh, 'BDM Phi', 2) 
    W = MixedFunctionSpace([V1, V2, V3])
    v1, v2, v3 = TestFunctions(W)
    
    # Target - which allready fulfills the requirements of continuous normal fluxes
    w = interpolate(Expression(['2', '1']), V)
        
    # Equation 1 - flux through the sides
    n = FacetNormal(mesh)
    a = dot(u, n)*v1*ds
    L = dot(w, n)*v1*ds
    for R in '+-':
        a += dot(u(R), n(R))*v1(R)*dS
        L += dot(w(R), n(R))*v1(R)*dS
    
    # Equation 2 - internal shape
    a += dot(u, v2)*dx
    L += dot(w, v2)*dx
    
    # Equation 3 - BDM Phi
    a += dot(u, v3)*dx
    L += dot(w, v3)*dx
    
    A = assemble(a)
    shape = A.array().shape
    rank = numpy.linalg.matrix_rank(A.array())
    cond = numpy.linalg.cond(A.array())
    print 'Shape %r, rank: %r, condition number: %.3e' % (shape, rank, cond)
    
    
    # Local solver for each element
    ls = LocalSolver(Form(a), Form(L))
    U = Function(V)
    ls.solve_local_rhs(U)
    
    print 'w:'
    print_nice_matrix(w.vector().array())
    print 'U:'
    print_nice_matrix(U.vector().array())
    
    error = errornorm(w, U, degree_rise=0)
    print 'Modification norm:', error
    assert error < 1e-15
