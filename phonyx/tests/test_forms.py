from nose.tools import assert_raises
from phonyx import IntervalMesh, FunctionSpace
from phonyx import TrialFunction, TestFunction, Constant, dx, dS, FormError


def test_valid_forms():
    mesh = IntervalMesh(3, 0, 1)
    V = FunctionSpace(mesh, 'CG', 1)
    u = TrialFunction(V)
    v = TestFunction(V)
    c = Constant(2.0)
    
    print 'Bilinear form with one measure'
    args = (1 + c)*u*v
    print args
    form = args*dx
    print form
    
    print 'Bilinear form with three measures'
    form = 1*u*v*dx + 2*u*v*dx + 3*u*v*dx
    print form


def test_invalid_forms():
    mesh = IntervalMesh(3, 0, 1)
    V = FunctionSpace(mesh, 'CG', 1)
    u = TrialFunction(V)
    v = TestFunction(V)
    
    def mul(arg, measure):
        return arg*measure
    
    print 'Missing test function'
    assert_raises(FormError, mul, u, dx)
    
    print 'Nonlinear function'
    assert_raises(FormError, mul, u*u*v, dx)
    
    print 'Double test function'
    assert_raises(FormError, mul, u*v*v, dx)


def test_matrix_dimension():
    mesh = IntervalMesh(3, 0, 1)
    V = FunctionSpace(mesh, 'CG', 1)
    u = TrialFunction(V)
    v = TestFunction(V)
    c = Constant(2.0)
    
    form = (1 + c)*u*v*dx
    assert form.dim == 4


def test_polynomial_degree():
    mesh = IntervalMesh(3, 0, 1)
    V = FunctionSpace(mesh, 'CG', 1)
    u = TrialFunction(V)
    v = TestFunction(V)
    c = Constant(2.0)
    
    arg = (1 + c)*u*v
    assert arg.polynomial_degree == 2
    
    arg = (1 + c + u)*u*u*v
    assert arg.polynomial_degree == 4


def test_dg_forms():
    mesh = IntervalMesh(3, 0, 1)
    V = FunctionSpace(mesh, 'CG', 1)
    u = TrialFunction(V)
    v = TestFunction(V)
    c = Constant(2.0)
    
    def mul(arg, measure):
        return arg*measure
    
    # Valid forms
    form = u('+')*v('+')*dS
    form += v('+')*dS
    form += c('+')*dS
    
    # Double restrictions
    assert_raises(FormError, lambda: v('+')('-'))
    assert_raises(FormError, lambda: (u('+')*v('+'))('-'))
    
    # Invalid restriction
    assert_raises(FormError, lambda: v(''))
    
    # Lacking restriction
    assert_raises(FormError, mul, v, dS)
    assert_raises(FormError, mul, u('+')*v, dS)
    
    # Restricted form in non-dS integral
    assert_raises(FormError, mul, u('+')*v('-'), dx)
