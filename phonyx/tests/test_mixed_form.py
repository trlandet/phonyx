from __future__ import division
import numpy
from phonyx import *

def test_mixed_form_size():
    N = 1
    mesh = UnitSquareMesh(N, N, 'right')
    
    # Stokes mixed form with P2P1 elements
    Pu, Pp = 2, 1
    V = VectorFunctionSpace(mesh, 'CG', Pu)
    Q = FunctionSpace(mesh, 'CG', Pp)
    Vlm = FunctionSpace(mesh, 'R', 0)
    W = MixedFunctionSpace([V, Q, Vlm])
    u, p, lm_trial = TrialFunctions(W)
    v, q, lm_test = TestFunctions(W)
    
    # Test  that the shapes of the sub-dofmaps and the final mixed dofmap are correct
    from phonyx.form_language import get_system_dim    
    for xname in 'u[0], u[1], p, lm_trial, v[0], v[1], q, lm_test'.split(', '):
        print xname
        x = eval(xname)
        dm = x.function_space().dofmap()
        N = dm.dim
        M = get_system_dim(x)
        print dm.cell_dofmap.values()
        print
        
        if '[' in xname:
            assert N == 9
        elif 'lm' in xname:
            assert N == 1
        else:
            assert N == 4
        
        assert M == 23
    
    A = assemble(dot(u, v)*dx)
    print A
    assert A.shape == (M, M)
    
    A = assemble(dot(p, q)*dx)
    print A
    assert A.shape == (M, M)
    
    A = assemble(dot(lm_trial, lm_test)*dx)
    print A
    assert A.shape == (M, M)


def test_lagrange_multiplier():
    N = 3
    mesh = UnitIntervalMesh(N)
    Q = FunctionSpace(mesh, 'CG', 1)
    Vlm = FunctionSpace(mesh, 'R', 0)
    W = MixedFunctionSpace([Q, Vlm])
    p, lm_trial = TrialFunctions(W)
    q, lm_test = TestFunctions(W)
    
    # Define weak form
    a = inner(p, q)*dx
    a += inner(nabla_grad(p), nabla_grad(q))*dx
    a += (p*lm_test + q*lm_trial)*dx
    
    M = assemble(a).array()
    Ma = numpy.array([[  28/9, -53/18,      0,      0,    1/6],
                      [-53/18,   56/9, -53/18,      0,    1/3],
                      [     0, -53/18,   56/9, -53/18,    1/3],
                      [     0,      0, -53/18,   28/9,    1/6],
                      [   1/6,    1/3,    1/3,    1/6,      0]])
    
    print 'M:\n', M.round(6)
    print 'Ma:\n', Ma
    err_M = abs(M-Ma)
    print 'err_M:\n', err_M.sum()
    
    assert err_M.sum() < 3e-15


def test_stokes_1d_with_BC():
    N = 3
    mesh = UnitIntervalMesh(N)
    
    n = FacetNormal(mesh)
    V = VectorFunctionSpace(mesh, 'CG', 2)
    Q = FunctionSpace(mesh, 'CG', 1)
    Vlm = FunctionSpace(mesh, 'R', 0)
    W = MixedFunctionSpace([V, Q, Vlm])
    u, p, lm_trial = TrialFunctions(W)
    v, q, lm_test = TestFunctions(W)
    
    f = Expression(['pi*pi*sin(pi*x[1]) + 2*pi*cos(2*pi*x[0])',
                    'pi*pi*cos(pi*x[0])'][0:1])
    ue = Expression(['sin(pi*x[1])', 'cos(pi*x[0])'][0:1])
    
    a = inner(nabla_grad(u), nabla_grad(v))*dx
    a += -nabla_div(u)*q*dx
    a += -nabla_div(v)*p*dx + p*inner(v, n)*ds
    a += (p*lm_test + q*lm_trial)*dx
    L = inner(f, v)*dx
    
    bc_u = DirichletBC(W.sub(0), ue, lambda x, on_boundary: on_boundary)
    A, b = assemble_system(a, L)
    bc_u.apply(A, b)
    A, b = A.array(), b.array()
    
    Aa = numpy.array([[  1,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0],
                      [  1,   14,    1,    0,   -8,   -8,    0, -1/6,    0,  1/6,    0,    0],
                      [  0,    1,   14,    1,    0,   -8,   -8,    0, -1/6,    0,  1/6,    0],
                      [  0,    0,    0,    1,    0,    0,    0,    0,    0,    0,    0,    0],
                      [ -8,   -8,    0,    0,   16,    0,    0, -2/3,  2/3,    0,    0,    0],
                      [  0,   -8,   -8,    0,    0,   16,    0,    0, -2/3,  2/3,    0,    0],
                      [  0,    0,   -8,   -8,    0,    0,   16,    0,    0, -2/3,  2/3,    0],
                      [5/6, -1/6,    0,    0, -2/3,    0,    0,    0,    0,    0,    0,  1/6],
                      [1/6,    0, -1/6,    0,  2/3, -2/3,    0,    0,    0,    0,    0,  1/3],
                      [  0,  1/6,    0, -1/6,    0,  2/3, -2/3,    0,    0,    0,    0,  1/3],
                      [  0,    0,  1/6, -5/6,    0,    0,  2/3,    0,    0,    0,    0,  1/6],
                      [  0,    0,    0,    0,    0,    0,    0,  1/6,  1/3,  1/3,  1/6,    0]])
    
    print 'A:\n', A.round(6)
    print 'Aa:\n', Aa
    err_A = abs(A-Aa)
    print 'err_A:\n', err_A.sum()
    
    assert err_A.sum() < 3e-14
