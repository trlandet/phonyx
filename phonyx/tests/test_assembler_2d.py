from __future__ import division
import numpy
from phonyx import *
from .helpers import print_nice_matrix

def test_assemble_mass_and_stiffness_matrices_2D_polydeg1():
    N = 1
    mesh = UnitSquareMesh(N, N, 'right')
    
    V = FunctionSpace(mesh, 'CG', 1)
    u = TrialFunction(V)
    v = TestFunction(V)
    
    a_M = u*v*dx
    a_A = dot(grad(u), grad(v))*dx
    M = assemble(a_M).array()
    A = assemble(a_A).array()
    
    
    Ma = numpy.array([[ 1/6, 1/24, 1/24, 1/12],
                      [1/24, 1/12,    0, 1/24],
                      [1/24,    0, 1/12, 1/24],
                      [1/12, 1/24, 1/24, 1/6]])
    Aa = numpy.array([[   1,  -0.5, -0.5,     0],
                      [-0.5,     1,    0,  -0.5],
                      [-0.5,     0,    1,  -0.5],
                      [   0,  -0.5, -0.5,     1]])
    
    print 'M:\n', M
    print 'Ma:\n', Ma
    err_M = abs(M-Ma)
    print 'err_M:\n', err_M, 
    
    print 'A:\n', A
    print 'Aa:\n', Aa
    err_A = abs(A-Aa)
    print 'err_A:\n', err_A
    
    assert err_M.sum() < 1e-15 and err_A.sum() < 1e-15 


def test_assemble_mass_and_stiffness_matrices_2D_polydeg2():
    N = 1
    mesh = UnitSquareMesh(N, N, 'right')
    
    V = FunctionSpace(mesh, 'CG', 2)
    u = TrialFunction(V)
    v = TestFunction(V)
    
    n = FacetNormal(mesh)
    a_M = u*v*dx
    a_A = dot(grad(u), grad(v))*dx
    L = Expression("x[0]*x[1]", degree=2)*dot(as_vector([1, 1]), n)*v*ds(domain=mesh)
    
    # Arrays from dolfin printed with 6 digits (using "print repr(M.array().round(6))")
    Ma = numpy.array([[ 0.033333, -0.002778, -0.002778, -0.005556, -0.011111,  0.      ,  0.      , -0.011111,  0.      ],
                      [-0.002778,  0.016667,  0.      , -0.002778,  0.      , -0.011111,  0.      ,  0.      ,  0.      ],
                      [-0.002778,  0.      ,  0.016667, -0.002778,  0.      , -0.011111,  0.      ,  0.      ,  0.      ],
                      [-0.005556, -0.002778, -0.002778,  0.033333,  0.      ,  0.      , -0.011111,  0.      , -0.011111],
                      [-0.011111,  0.      ,  0.      ,  0.      ,  0.088889,  0.044444,  0.044444,  0.      ,  0.      ],
                      [ 0.      , -0.011111, -0.011111,  0.      ,  0.044444,  0.177778,  0.044444,  0.044444,  0.044444],
                      [ 0.      ,  0.      ,  0.      , -0.011111,  0.044444,  0.044444,  0.088889,  0.      ,  0.      ],
                      [-0.011111,  0.      ,  0.      ,  0.      ,  0.      ,  0.044444,  0.      ,  0.088889,  0.044444],
                      [ 0.      ,  0.      ,  0.      , -0.011111,  0.      ,  0.044444,  0.      ,  0.044444,  0.088889]])
    Aa = numpy.array([[ 1.      ,  0.166667,  0.166667, -0.      ,  0.      ,  0.      , -0.666667,  0.      , -0.666667],
                      [ 0.166667,  1.      ,  0.      ,  0.166667, -0.666667,  0.      , -0.666667,  0.      ,  0.      ],
                      [ 0.166667,  0.      ,  1.      ,  0.166667,  0.      ,  0.      ,  0.      , -0.666667, -0.666667],
                      [-0.      ,  0.166667,  0.166667,  1.      , -0.666667,  0.      ,  0.      , -0.666667,  0.      ],
                      [ 0.      , -0.666667,  0.      , -0.666667,  2.666667, -1.333333,  0.      ,  0.      ,  0.      ],
                      [ 0.      ,  0.      ,  0.      ,  0.      , -1.333333,  5.333333, -1.333333, -1.333333, -1.333333],
                      [-0.666667, -0.666667,  0.      ,  0.      ,  0.      , -1.333333,  2.666667,  0.      ,  0.      ],
                      [ 0.      ,  0.      , -0.666667, -0.666667,  0.      , -1.333333,  0.      ,  2.666667,  0.      ],
                      [-0.666667,  0.      , -0.666667,  0.      ,  0.      , -1.333333,  0.      ,  0.      ,  2.666667]])
    ba = numpy.array([ 0.      ,  0.      ,  0.      ,  0.333333,  0.333333,  0.      ,  0.      ,  0.333333,  0.      ])

    M = assemble(a_M).array()
    A = assemble(a_A).array()
    b = assemble(L).array()
    
    numpy.set_printoptions(linewidth=200)
    print 'M:\n', M.round(6)
    print 'Ma:\n', Ma
    err_M = abs(M-Ma)
    print 'err_M:\n', err_M.sum()
    
    print 'A:\n', A.round(6)
    print 'Aa:\n', Aa
    err_A = abs(A-Aa)
    print 'err_A:\n', err_A.sum()
    
    print 'b:\n', b.round(6)
    print 'ba:\n', ba
    err_b = abs(b-ba)
    print 'err_b:\n', err_b.sum()
    
    # Low criteria due to rounded "analytical" (dolfin) results
    assert err_M.sum() < 2e-5 and err_A.sum() < 2e-5 and err_b.sum() < 2e-6
    
    
def test_assemble_convection_2D_polydeg2():
    N = 1
    mesh = UnitSquareMesh(N, N, 'right')
    
    V = VectorFunctionSpace(mesh, 'CG', 2)
    u = TrialFunction(V)
    v = TestFunction(V)
    
    w = Expression(['1', '2'])
    a = dot(dot(w, nabla_grad(u)), v)*dx
    
    A99 = assemble(a).array()[:9,:9]
    Aa99 = numpy.array([[-0.2,  1/30, -1/30,  -0.1,      -1/30,       0.3, -1/30, -1/15,      2/15],
                        [1/30, -1/15,     0, -1/15,        1/6,     -1/30, -1/30,     0,         0],
                        [1/15,     0,  1/15, -1/30,          0,      1/30,     0,  1/30,      -1/6],
                        [ 0.1,  1/30, -1/30,   0.2,      -2/15,      -0.3,  1/15,  1/30,      1/30],
                        [1/30,  -0.1,     0,   0.2,       4/15,  1.18e-15,  -0.4,     0,         0],
                        [-0.3,  1/30, -1/30,   0.3, -3.872e-15, 2.442e-15,  -0.4,   0.4, 5.759e-16],
                        [-0.1,  -0.1,     0, -1/15,        0.4,       0.4, -8/15,     0,         0],
                        [1/15,     0,   0.1,   0.1,          0,      -0.4,     0,  8/15,      -0.4],
                        [-0.2,     0,   0.1, -1/30,          0, 9.159e-16,     0,   0.4,     -4/15]])
    
    
    print_nice_matrix(A99, Aa99)
    err = abs(A99 - Aa99).sum()
    print 'Error:', err
    assert err < 3e-14


def test_bubble():
    N = 1
    mesh = UnitSquareMesh(N, N, 'right')
    V = FunctionSpace(mesh, 'Bubble', 3)
    u = TrialFunction(V)
    v = TestFunction(V)
    
    M = assemble(u*v*dx).array()
    A = assemble(dot(grad(u), grad(v))*dx).array()
    
    Ma = numpy.array([[81/560,      0],
                      [     0, 81/560]])
    
    Aa = numpy.array([[8.1,      0],
                      [     0, 8.1]])
    
    err_M = abs(M - Ma).sum()
    print 'Error M:', err_M
    
    err_A = abs(A - Aa).sum()
    print 'Error A:', err_A
    
    assert err_M < 1e-15
    assert err_A < 5e-14
