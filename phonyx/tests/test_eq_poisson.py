"""
Integration test of Phonyx

Run the whole machinery like a normal user would to solve Poissons equation
and verify that the solution is close to the analytical solution
"""
import numpy
from phonyx import *


def test_poisson_1d():
    N = 10
    mesh = IntervalMesh(N, 0, 1)
    V = FunctionSpace(mesh, 'CG', 1)
    u = TrialFunction(V)
    v = TestFunction(V)
    
    def boundary(x, on_boundary):
        return on_boundary
    bc = DirichletBC(V, Constant(0), boundary)
    
    f = Expression('pi*pi*sin(pi*x[0])')
    
    a = u.dx(0)*v.dx(0)*dx
    L = f*v*dx
    
    # Assemble matrices and apply boundary conditions
    A = assemble(a)
    b = assemble(L)
    bc.apply(A, b)
    
    # Solve the linear system
    ufunc = Function(V)
    solve(A, ufunc.vector(), b)
    
    # The analytical solution
    x = mesh.coordinates()[:,0]
    y = numpy.sin(numpy.pi*x)
    
    U = ufunc.vector()
    error = ((U - y)**2).sum()/(y**2).sum()
    print 'Error: %.2e' % error
    
    assert error < 1e-10


def test_poisson_1d_nitsche():
    N = 10
    offset = -1.0
    
    mesh = IntervalMesh(N, 0, 1)
    V = FunctionSpace(mesh, 'CG', 1)
    u = TrialFunction(V)
    v = TestFunction(V)
    
    n = FacetNormal(mesh)
    h = Circumradius(mesh)
    f = Expression('pi*pi*sin(pi*x[0])')
    g = Constant(offset)
    lam = Constant(1.0)
    
    eq = u.dx(0)*v.dx(0)*dx
    eq -= u.dx(0)*n[0]*v*ds
    eq -= v.dx(0)*n[0]*(u - g)*ds
    eq += lam/h*(u - g)*v*ds
    eq -= f*v*dx
    
    # Assemble matrices and apply boundary conditions
    a, L = system(eq)
    A = assemble(a)
    b = assemble(L)
    
    # Solve the linear system
    ufunc = Function(V)
    solve(A, ufunc.vector(), b)
    
    # The analytical solution
    x = mesh.coordinates()[:,0]
    y = numpy.sin(numpy.pi*x)+offset
    
    U = ufunc.vector()
    print U.array()
    error = ((U - y)**2).sum()/(y**2).sum()
    print 'Error: %.2e' % error
    
    assert error < 2e-5
