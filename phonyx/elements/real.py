class Element_Real(object):
    def __init__(self):
        """
        Real element, used in Lagrange multipliers 
        """
        self.order = 0
        self.num_basis_functions = 1
        self.shape = ()
    
    def get_coordinate_for_basis_function(self, i, cell):
        raise NotImplementedError('Real elements does not support get_coordinate_for_basis_function')
    
    def evaluate_basis_function(self, i, x, cell, component):
        """
        Evaluate basis function "i" at local position x for a cell with the
        given vertex coordinates
        """
        return 1.0
    
    def evaluate_basis_function_derivative(self, i, x, cell, d, component):
        """
        Evaluate the derivative in direction x[d] of basis function "i" at
        local position x for a cell with the given vertex coordinates
        """
        return 0.0
