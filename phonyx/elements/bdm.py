import numpy

class Element_BDM_Phi_dim2_deg2(object):
    def __init__(self):
        """
        BDM Phi element
        
        A quadratic 2D element defined as the curl of a vector [0, 0, b] where
        b is L1*L2*L3, the product of the barycentric coordinates (a bubble
        function).
        
        Used by projections into the BDM (Brezzi, Douglas, Marini) function space 
        """
        self.order = 2
        self.num_basis_functions = 1
        self.shape = (2,)
    
    def get_coordinate_for_basis_function(self, i, cell):
        """
        Get the coordinates of the node associated with basis function "i" at
        for a cell with the given vertex coordinates
        """
        return numpy.mean(cell.vertices, axis=0)
    
    def evaluate_basis_function(self, i, x, cell, component):
        """
        Evaluate basis function "i" at local position x for a cell with the
        given vertex coordinates
        """
        vertices = cell.vertices
        x1, y1 = vertices[0]
        x2, y2 = vertices[1]
        x3, y3 = vertices[2]
        
        if True:
            L1, L2 = x[0], x[1]
            x0 = L1*x1 - L1*x3 + L2*x2 - L2*x3 + x3
            y0 = L1*y1 - L1*y3 + L2*y2 - L2*y3 + y3
            
            if component == 0:
                return (x1 - x3)*((x0 - x3)*(y2 - y3) + (-x2 + x3)*(y0 - y3))*(-((x0 - x3)*(-y1 + y3) + (x1 - x3)*(y0 - y3))/((x1 - x3)*(y2 - y3) + (-x2 + x3)*(y1 - y3)) - ((x0 - x3)*(y2 - y3) + (-x2 + x3)*(y0 - y3))/((x1 - x3)*(y2 - y3) + (-x2 + x3)*(y1 - y3)) + 1)/pow((x1 - x3)*(y2 - y3) + (-x2 + x3)*(y1 - y3), 2) + (-x2 + x3)*((x0 - x3)*(-y1 + y3) + (x1 - x3)*(y0 - y3))*(-((x0 - x3)*(-y1 + y3) + (x1 - x3)*(y0 - y3))/((x1 - x3)*(y2 - y3) + (-x2 + x3)*(y1 - y3)) - ((x0 - x3)*(y2 - y3) + (-x2 + x3)*(y0 - y3))/((x1 - x3)*(y2 - y3) + (-x2 + x3)*(y1 - y3)) + 1)/pow((x1 - x3)*(y2 - y3) + (-x2 + x3)*(y1 - y3), 2) + ((x0 - x3)*(-y1 + y3) + (x1 - x3)*(y0 - y3))*((x0 - x3)*(y2 - y3) + (-x2 + x3)*(y0 - y3))*(-(x1 - x3)/((x1 - x3)*(y2 - y3) + (-x2 + x3)*(y1 - y3)) - (-x2 + x3)/((x1 - x3)*(y2 - y3) + (-x2 + x3)*(y1 - y3)))/pow((x1 - x3)*(y2 - y3) + (-x2 + x3)*(y1 - y3), 2)
            elif component == 1:
                return -(-y1 + y3)*((x0 - x3)*(y2 - y3) + (-x2 + x3)*(y0 - y3))*(-((x0 - x3)*(-y1 + y3) + (x1 - x3)*(y0 - y3))/((x1 - x3)*(y2 - y3) + (-x2 + x3)*(y1 - y3)) - ((x0 - x3)*(y2 - y3) + (-x2 + x3)*(y0 - y3))/((x1 - x3)*(y2 - y3) + (-x2 + x3)*(y1 - y3)) + 1)/pow((x1 - x3)*(y2 - y3) + (-x2 + x3)*(y1 - y3), 2) - (y2 - y3)*((x0 - x3)*(-y1 + y3) + (x1 - x3)*(y0 - y3))*(-((x0 - x3)*(-y1 + y3) + (x1 - x3)*(y0 - y3))/((x1 - x3)*(y2 - y3) + (-x2 + x3)*(y1 - y3)) - ((x0 - x3)*(y2 - y3) + (-x2 + x3)*(y0 - y3))/((x1 - x3)*(y2 - y3) + (-x2 + x3)*(y1 - y3)) + 1)/pow((x1 - x3)*(y2 - y3) + (-x2 + x3)*(y1 - y3), 2) - ((x0 - x3)*(-y1 + y3) + (x1 - x3)*(y0 - y3))*((x0 - x3)*(y2 - y3) + (-x2 + x3)*(y0 - y3))*(-(-y1 + y3)/((x1 - x3)*(y2 - y3) + (-x2 + x3)*(y1 - y3)) - (y2 - y3)/((x1 - x3)*(y2 - y3) + (-x2 + x3)*(y1 - y3)))/pow((x1 - x3)*(y2 - y3) + (-x2 + x3)*(y1 - y3), 2)
        
        else:
            L1, L2, L3 = x[0], x[1], 1 - x[0] - x[1]
            D = (y2 - y3)*(x1 - x3) + (x3 - x2)*(y1 - y3) 
            if component == 0:
                ddy_L1 = (x3 - x2)/D
                ddy_L2 = (x1 - x3)/D
                ddy_L3 = -(x3 - x2)/D - (x1 - x3)/D
                return L1*L2*ddy_L3 + L1*ddy_L2*L3 + ddy_L1*L2*L3
            
            elif component == 1:
                ddx_L1 = (y2 - y3)/D
                ddx_L2 = (y3 - y1)/D
                ddx_L3 = -(y2 - y3)/D - (y3 - y1)/D
                return L1*L2*ddx_L3 + L1*ddx_L2*L3 + ddx_L1*L2*L3 
    
    def evaluate_basis_function_derivative(self, i, x, cell, d, component):
        """
        Evaluate the derivative in direction x[d] of basis function "i" at
        local position x for a cell with the given vertex coordinates
        """
        raise NotImplementedError('Taking derivative of BDM Phi is not supported yet')
