import numpy

class Element_Bubble_dim2_deg3(object):
    def __init__(self):
        """
        A 2D bubble element
        
        The basis function is L1*L2*L3, the product of the barycentric coordinates
        """
        self.order = 3
        self.num_basis_functions = 1
        self.shape = ()
    
    def get_coordinate_for_basis_function(self, i, cell):
        """
        Get the coordinates of the node associated with basis function "i" at
        for a cell with the given vertex coordinates
        """
        return numpy.mean(cell.vertices, axis=0)
    
    def evaluate_basis_function(self, i, x, cell, component):
        """
        Evaluate basis function "i" at local position x for a cell with the
        given vertex coordinates
        """
        L1, L2, L3 = x[0], x[1], 1 - x[0] - x[1]
        return L1*L2*L3*27
    
    def evaluate_basis_function_derivative(self, i, x, cell, d, component):
        """
        Evaluate the derivative in direction x[d] of basis function "i" at
        local position x for a cell with the given vertex coordinates
        """
        vertices = cell.vertices
        x1, y1 = vertices[0]
        x2, y2 = vertices[1]
        x3, y3 = vertices[2]
        
        L1, L2, L3 = x[0], x[1], 1 - x[0] - x[1]
        D = (y2 - y3)*(x1 - x3) + (x3 - x2)*(y1 - y3) 
        if d == 0:
            ddx_L1 = (y2 - y3)/D
            ddx_L2 = (y3 - y1)/D
            ddx_L3 = -(y2 - y3)/D - (y3 - y1)/D
            return (L1*L2*ddx_L3 + L1*ddx_L2*L3 + ddx_L1*L2*L3)*27
        
        elif d == 1:
            ddy_L1 = (x3 - x2)/D
            ddy_L2 = (x1 - x3)/D
            ddy_L3 = -(x3 - x2)/D - (x1 - x3)/D
            return (L1*L2*ddy_L3 + L1*ddy_L2*L3 + ddy_L1*L2*L3)*27
