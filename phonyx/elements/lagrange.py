import numpy


class Element_Lagrange_dim1_deg0(object):
    def __init__(self):
        """
        A constant 1D Lagrange element
        """
        self.order = 0
        self.num_basis_functions = 1
        self.shape = ()
    
    def get_coordinate_for_basis_function(self, i, cell):
        """
        Get the coordinates of the node associated with basis function "i" at
        for a cell with the given vertex coordinates
        """
        return (cell.vertices[0] + cell.vertices[1])/2 
    
    def evaluate_basis_function(self, i, x, cell, component):
        """
        Evaluate basis function "i" at local position x for a cell with the
        given vertex coordinates
        """
        return 1
    
    def evaluate_basis_function_derivative(self, i, x, cell, d, component):
        """
        Evaluate the derivative in direction x[d] of basis function "i" at
        local position x for a cell with the given vertex coordinates
        """
        return 0


class Element_Lagrange_dim1_deg1(object):
    def __init__(self):
        """
        A linear 1D Lagrange element
        """
        self.order = 1
        self.num_basis_functions = 2
        self.shape = ()
    
    def get_coordinate_for_basis_function(self, i, cell):
        """
        Get the coordinates of the node associated with basis function "i" at
        for a cell with the given vertex coordinates
        """
        return cell.vertices[i]
    
    def evaluate_basis_function(self, i, x, cell, component):
        """
        Evaluate basis function "i" at local position x for a cell with the
        given vertex coordinates
        """
        if i == 0:
            return (1 - x[0])*0.5
        elif i == 1:
            return (1 + x[0])*0.5
    
    def evaluate_basis_function_derivative(self, i, x, cell, d, component):
        """
        Evaluate the derivative in direction x[d] of basis function "i" at
        local position x for a cell with the given vertex coordinates
        """
        vertices = cell.vertices
        scale = 2/abs(vertices[1] - vertices[0])
        if i == 0:
            return -0.5*scale
        elif i == 1:
            return 0.5*scale


class Element_Lagrange_dim1_deg2(object):
    def __init__(self):
        """
        A quadratic 1D Lagrange element
        """
        self.order = 1
        self.num_basis_functions = 3
        self.shape = ()
    
    def get_coordinate_for_basis_function(self, i, cell):
        """
        Get the coordinates of the node associated with basis function "i" at
        for a cell with the given vertex coordinates
        """
        vertices = cell.vertices
        if i < 2:
            return vertices[i]
        else:
            return (vertices[0] + vertices[1]) / 2
    
    def evaluate_basis_function(self, i, x, cell, component):
        """
        Evaluate basis function "i" at local position x for a cell with the
        given vertex coordinates
        """
        if i == 0:
            return 0.5*x[0]**2 - 0.5*x[0]
        elif i == 1:
            return 0.5*x[0]**2 + 0.5*x[0]
        elif i == 2:
            return -x[0]**2 + 1
    
    def evaluate_basis_function_derivative(self, i, x, cell, d, component):
        """
        Evaluate the derivative in direction x[d] of basis function "i" at
        local position x for a cell with the given vertex coordinates
        """
        vertices = cell.vertices
        scale = 2/abs(vertices[1] - vertices[0])
        if i == 0:
            return (x[0] - 0.5)*scale
        elif i == 1:
            return (x[0] + 0.5)*scale
        elif i == 2:
            return -2*x[0]*scale


class Element_Lagrange_dim2_deg0(object):
    def __init__(self):
        """
        A constant 2D Lagrange element
        """
        self.order = 0
        self.num_basis_functions = 1
        self.shape = ()
    
    def get_coordinate_for_basis_function(self, i, cell):
        """
        Get the coordinates of the node associated with basis function "i" at
        for a cell with the given vertex coordinates
        """
        return numpy.mean(cell.vertices, axis=0)
    
    def evaluate_basis_function(self, i, x, cell, component):
        """
        Evaluate basis function "i" at local position x for a cell with the
        given vertex coordinates
        """
        return 1
    
    def evaluate_basis_function_derivative(self, i, x, cell, d, component):
        """
        Evaluate the derivative in direction x[d] of basis function "i" at
        local position x for a cell with the given vertex coordinates
        """
        return 0


class Element_Lagrange_dim2_deg1(object):
    def __init__(self):
        """
        A linear 2D Lagrange element
        """
        self.order = 1
        self.num_basis_functions = 3
        self.shape = ()
    
    def get_coordinate_for_basis_function(self, i, cell):
        """
        Get the coordinates of the node associated with basis function "i" at
        for a cell with the given vertex coordinates
        """
        return cell.vertices[i]
    
    def evaluate_basis_function(self, i, x, cell, component):
        """
        Evaluate basis function "i" at local position x for a cell with the
        given vertex coordinates
        """
        if i == 0:
            return x[0]
        elif i == 1:
            return x[1]
        else:
            return 1 - x[0] - x[1]
    
    def evaluate_basis_function_derivative(self, i, x, cell, d, component):
        """
        Evaluate the derivative in direction x[d] of basis function "i" at
        local position x for a cell with the given vertex coordinates
        """
        vertices = cell.vertices
        x1, y1 = vertices[0]
        x2, y2 = vertices[1]
        x3, y3 = vertices[2]
        
        D = (y2 - y3)*(x1 - x3) + (x3 - x2)*(y1 - y3)
        if i == 0:
            if d == 0:
                return (y2 - y3)/D
            elif d == 1:
                return (x3 - x2)/D
        
        elif i == 1:
            if d == 0:
                return (y3 - y1)/D
            elif d == 1:
                return (x1 - x3)/D
        
        elif i == 2:
            if d == 0:
                return -(y2 - y3)/D - (y3 - y1)/D
            elif d == 1:
                return -(x3 - x2)/D - (x1 - x3)/D


class Element_Lagrange_dim2_deg2(object):
    def __init__(self):
        """
        A quadratic 2D Lagrange element
        """
        self.order = 1
        self.num_basis_functions = 6
        self.shape = ()
    
    def get_coordinate_for_basis_function(self, i, cell):
        """
        Get the coordinates of the node associated with basis function "i" at
        for a cell with the given vertex coordinates
        """
        vertices = cell.vertices
        if i < 3:
            return vertices[i]
        elif i == 3:
            return ((vertices[1][0] + vertices[2][0])/2,
                    (vertices[1][1] + vertices[2][1])/2)
        elif i == 4:
            return ((vertices[0][0] + vertices[2][0])/2,
                    (vertices[0][1] + vertices[2][1])/2)
        elif i == 5:
            return ((vertices[1][0] + vertices[0][0])/2,
                    (vertices[1][1] + vertices[0][1])/2)
    
    def evaluate_basis_function(self, i, x, cell, component):
        """
        Evaluate basis function "i" at local position x for a cell with the
        given vertex coordinates
        """
        L0, L1, L2 = x[0], x[1], 1 - x[0] - x[1]
        
        if i == 0:
            return 2*L0*(L0 - 0.5)
        elif i == 1:
            return 2*L1*(L1 - 0.5)
        elif i == 2:
            return 2*L2*(L2 - 0.5)
        elif i == 3:
            return 4*L1*L2
        elif i == 4:
            return 4*L2*L0
        elif i == 5:
            return 4*L0*L1
    
    def evaluate_basis_function_derivative(self, i, x, cell, d, component):
        """
        Evaluate the derivative in direction x[d] of basis function "i" at
        local position x for a cell with the given vertex coordinates
        """
        vertices = cell.vertices
        x1, y1 = vertices[0]
        x2, y2 = vertices[1]
        x3, y3 = vertices[2]
        
        L0, L1, L2 = x[0], x[1], 1 - x[0] - x[1]
        D = (y2 - y3)*(x1 - x3) + (x3 - x2)*(y1 - y3)
        
        if i == 0:
            if d == 0:
                return 2*(y2 - y3)/D*(2*L0 - 0.5)
            elif d == 1:
                return 2*(x3 - x2)/D*(2*L0 - 0.5)
        
        elif i == 1:
            if d == 0:
                return 2*(y3 - y1)/D*(2*L1 - 0.5)
            elif d == 1:
                return 2*(x1 - x3)/D*(2*L1 - 0.5)
        
        elif i == 2:    
            if d == 0:
                return 2*(-(y2 - y3)/D - (y3 - y1)/D)*(2*L2 - 0.5)
            elif d == 1:
                return 2*(-(x3 - x2)/D - (x1 - x3)/D)*(2*L2 - 0.5)
        
        elif i == 3:
            if d == 0:
                return 4*(-(y2 - y3)/D - (y3 - y1)/D)*L1 + 4*(y3 - y1)/D*L2
            elif d == 1:
                return 4*(-(x3 - x2)/D - (x1 - x3)/D)*L1 + 4*(x1 - x3)/D*L2
        
        elif i == 4:
            if d == 0:
                return 4*(-(y2 - y3)/D - (y3 - y1)/D)*L0 + 4*(y2 - y3)/D*L2
            elif d == 1:
                return 4*(-(x3 - x2)/D - (x1 - x3)/D)*L0 + 4*(x3 - x2)/D*L2

        elif i == 5:
            if d == 0:
                return 4*(y2 - y3)/D*L1 + 4*(y3 - y1)/D*L0
            elif d == 1:
                return 4*(x3 - x2)/D*L1 + 4*(x1 - x3)/D*L0
