class Element_LagrangeTrace_dim2_deg0(object):
    def __init__(self):
        """
        A constant 1D Lagrange trace element in a 2D mesh 
        """
        self.order = 0
        self.num_basis_functions = 3
        self.shape = ()
    
    def get_coordinate_for_basis_function(self, i, cell):
        """
        Get the coordinates of the node associated with basis function "i" at
        for a cell with the given vertex coordinates
        """
        vertices = cell.vertices
        if i == 0:
            return ((vertices[1][0] + vertices[2][0])/2,
                    (vertices[1][1] + vertices[2][1])/2)
        elif i == 1:
            return ((vertices[0][0] + vertices[2][0])/2,
                    (vertices[0][1] + vertices[2][1])/2)
        elif i == 2:
            return ((vertices[1][0] + vertices[0][0])/2,
                    (vertices[1][1] + vertices[0][1])/2)
    
    def evaluate_basis_function(self, i, x, cell, component):
        """
        Evaluate basis function "i" at local position x for a cell with the
        given vertex coordinates
        """
        if is_correct_facet(i, cell):
            return 1
        else:
            return 0
    
    def evaluate_basis_function_derivative(self, i, x, cell, d, component):
        """
        Evaluate the derivative in direction x[d] of basis function "i" at
        local position x for a cell with the given vertex coordinates
        """
        return 0


class Element_LagrangeTrace_dim2_deg1(object):
    def __init__(self):
        """
        A linear 1D Lagrange trace element in a 2D mesh 
        """
        self.order = 1
        self.num_basis_functions = 6
        self.shape = ()
    
    def get_coordinate_for_basis_function(self, i, cell):
        """
        Get the coordinates of the node associated with basis function "i" at
        for a cell with the given vertex coordinates
        """
        vertices = cell.vertices
        
        # Facet 0
        if i == 0:
            return vertices[1]
        elif i == 1:
            return vertices[2]
        
        # Facet 1
        elif i == 2:
            return vertices[0]
        elif i == 3:
            return vertices[2]
        
        # Facet 2
        elif i == 4:
            return vertices[0]
        elif i == 5:
            return vertices[1]
    
    def evaluate_basis_function(self, i, x, cell, component):
        """
        Evaluate basis function "i" at local position x for a cell with the
        given vertex coordinates
        """
        if not is_correct_facet(i//2, cell):
            return 0
        
        x0, x1 = x
        x2 = 1 - x0 - x1
        
        # Facet 0
        if i == 0:
            return x1
        elif i == 1:
            return x2
        
        # Facet 1
        elif i == 2:
            return x0
        elif i == 3:
            return x2
        
        # Facet 2
        elif i == 4:
            return x0
        elif i == 5:
            return x1
    
    def evaluate_basis_function_derivative(self, i, x, cell, d, component):
        """
        Evaluate the derivative in direction x[d] of basis function "i" at
        local position x for a cell with the given vertex coordinates
        """
        # Untested since this is not implemented in current FEniCS
        if not is_correct_facet(i//2, cell):
            return 0
        vertices = cell.vertices
        
        # Facet 0
        if i == 0:
            return vertices[1][d] - vertices[2][d]
        elif i == 1:
            return vertices[2][d] - vertices[1][d]
        
        # Facet 1
        elif i == 2:
            return vertices[0][d] - vertices[2][d] 
        elif i == 3:
            return vertices[2][d] - vertices[0][d]
        
        # Facet 2
        elif i == 4:
            return vertices[0][d] - vertices[1][d]
        elif i == 5:
            return vertices[1][d] - vertices[0][d]


class Element_LagrangeTrace_dim2_deg2(object):
    def __init__(self):
        """
        A quadratic 1D Lagrange trace element in a 2D mesh 
        """
        self.order = 2
        self.num_basis_functions = 9
        self.shape = ()
    
    def get_coordinate_for_basis_function(self, i, cell):
        """
        Get the coordinates of the node associated with basis function "i" at
        for a cell with the given vertex coordinates
        """
        v0, v1, v2 = cell.vertices
        
        # Vertex 0
        if i == 0:
            return v1
        elif i == 1:
            return v2
        elif i == 2:
            return ((v1[0] + v2[0])/2, (v1[1] + v2[1])/2)
        
        # Vertex 1
        elif i == 3:
            return v0
        elif i == 4:
            return v2
        elif i == 5:
            return ((v0[0] + v2[0])/2, (v0[1] + v2[1])/2)
        
        # Vertex 2
        elif i == 6:
            return v0
        elif i == 7:
            return v1
        elif i == 8:
            return ((v0[0] + v1[0])/2, (v0[1] + v1[1])/2)
    
    def evaluate_basis_function(self, i, x, cell, component):
        """
        Evaluate basis function "i" at local position x for a cell with the
        given vertex coordinates
        """
        if not is_correct_facet(i//3, cell):
            return 0
        
        x0, x1 = x
        x2 = 1 - x0 - x1
        
        # Facet 0
        if i == 0:
            return 2*(x2 - 0.5)*(x2 - 1)
        elif i == 1:
            return 2*(x1 - 0.5)*(x1 - 1)
        elif i == 2:
            return x1*x2*4
        
        # Facet 1
        elif i == 3:
            return 2*(x2 - 0.5)*(x2 - 1)
        elif i == 4:
            return 2*(x0 - 0.5)*(x0 - 1)            
        elif i == 5:
            return x0*x2*4
        
        # Facet 2
        elif i == 6:
            return 2*(x1 - 0.5)*(x1 - 1)
        elif i == 7:
            return 2*(x0 - 0.5)*(x0 - 1)
        elif i == 8:
            return x0*x1*4
    
    def evaluate_basis_function_derivative(self, i, x, cell, d, component):
        """
        Evaluate the derivative in direction x[d] of basis function "i" at
        local position x for a cell with the given vertex coordinates
        """
        raise NotImplementedError('Differentiation of DGT not implemented for order 2')


def is_correct_facet(i_facet_local, cell):
    assert cell.local_facet is not None, 'DG trace space only supports facet integrals'
    facet_ids = cell.mesh.topology().cell_to_facet[cell.id]
    return cell.local_facet.id == facet_ids[i_facet_local]
