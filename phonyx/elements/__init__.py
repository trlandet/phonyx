class UflElement(object):
    def __init__(self, family, degree, shape=(), num_sub_elements=0):
        """
        This class is just here for FEniCS API compatibility
        """
        self._family = family
        self._degree = degree
        self._shape = shape
        self._num_sub_elements = num_sub_elements
    
    def degree(self):
        return self._degree
    
    def family(self):
        return self._family
    
    def num_sub_elements(self):
        return self._num_sub_elements
    
    def value_shape(self):
        return self._shape

from .real import Element_Real
from .bubble import Element_Bubble_dim2_deg3
from .lagrange import Element_Lagrange_dim1_deg0, Element_Lagrange_dim1_deg1, Element_Lagrange_dim1_deg2
from .lagrange import Element_Lagrange_dim2_deg0, Element_Lagrange_dim2_deg1, Element_Lagrange_dim2_deg2
from .lagrange_trace import Element_LagrangeTrace_dim2_deg0, Element_LagrangeTrace_dim2_deg1, Element_LagrangeTrace_dim2_deg2
from .bdm import Element_BDM_Phi_dim2_deg2
