import time
from collections import OrderedDict


TimingClear_clear = True
TimingClear_keep = False
TimingType_wall = 0
TimingType_user = 1
TimingType_system = 2


class Timer(object):
    registry = OrderedDict()
    
    def __init__(self, name):
        self.name = name
        self.start()
    
    def start(self):
        self.start_time = time.time()
        
    def stop(self):
        elapsed = time.time() - self.start_time
        Timer.registry.setdefault(self.name, []).append(elapsed)


def list_timings(reset, ttypes=(TimingType_wall,)):
    """
    Show the time spent in the different components of phonyx
    
    Reset will delete the stored timings if specified as True
    
    The ttype can be TimingType_wall, TimingType_user or TimingType_system just
    like in FEniCS, but currently only wall clock time is supported
    """
    assert len(ttypes) == 1 and ttypes[0] == TimingType_wall
    
    maxlen = 18
    for key in Timer.registry:
        maxlen = max(maxlen, len(key))
    
    pattern1 = '%%-%ds  |  Average time  Total time  Reps' % maxlen 
    pattern2 = '%%-%ds  |   %%11.4f %%11.4f %%5d' % maxlen
    print pattern1 % 'Summary of timings'
    print '-' * (35 + maxlen)
    
    for key, value in Timer.registry.items():
        total = sum(value)
        count = len(value) 
        print pattern2 % (key, total/count, total, count)
