"""
To keep the plotting framework working with both FEniCS and Phonyx objects some
comaptibility utilities are needed
"""


def get_mod(obj):
    if obj.__class__.__module__.startswith('dolfin'):
        import dolfin as mod
    else:
        import phonyx as mod
    return mod


def itercells(mesh):
    "Dolfin compatibility wrapper for cells()"
    mod = get_mod(mesh)
    return mod.cells(mesh)


def iterfacets(mesh):
    "Dolfin compatibility wrapper for facets()"
    mod = get_mod(mesh)
    return mod.facets(mesh)


def get_shape(f):
    if f.__class__.__module__.startswith('dolfin'):
        return f.shape()
    else:
        return f.shape


def get_cell(mesh, i):
    "Dolfin compatibility wrapper for Cell(mesh, i)"
    mod = get_mod(mesh)
    return mod.Cell(mesh, i)
