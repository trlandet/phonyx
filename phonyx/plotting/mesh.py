from .compat import itercells

def plot_mesh(mesh, ax):
    from matplotlib.collections import PolyCollection
    
    assert mesh.geometry().dim() == 2
    
    coords = mesh.coordinates()
    vertices = []
    for cell in itercells(mesh):
        corners = cell.entities(0)
        xs = [coords[i,0] for i in corners]
        ys = [coords[i,1] for i in corners]
        vertices.append(zip(xs, ys))
    
    polys = PolyCollection(vertices, facecolors='white', edgecolors='black', linewidths=0.25)
    ax.add_collection(polys)
