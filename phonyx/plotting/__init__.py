from phonyx import Function
from phonyx.mesh import AbstractMesh
from .mesh import plot_mesh
from .dgt import plot_dtg


def plot(obj, title=None):
    from matplotlib import pyplot
    fig = pyplot.figure()
    ax = fig.add_subplot(111)
    
    # Support plotting of dolfin functions
    if obj.__class__.__module__.startswith('dolfin'):
        cname = obj.__class__.__name__
        is_mesh = cname == 'Mesh'
        is_function = cname == 'Function' 
    else:
        is_mesh = isinstance(obj, AbstractMesh)
        is_function = isinstance(obj, Function)
    
    if is_mesh:
        plot_mesh(obj, ax)
    
    else:
        assert is_function
        
        V = obj.function_space()
        mesh = V.mesh()
        plot_mesh(mesh, ax)
        
        family = V.ufl_element().family()
        if family == 'Discontinuous Lagrange Trace':
            plot_dtg(obj, ax)
        else:
            raise ValueError('Do not know how to plot %s functions' % V.name)
    
    ax.axis('equal')
    if title is not None:
        ax.set_title(title)
    fig.tight_layout()


def interactive():
    from matplotlib import pyplot
    pyplot.show()
