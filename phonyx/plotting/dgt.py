import numpy
from .compat import iterfacets, get_shape, get_cell


def plot_dtg(f, ax):
    V = f.function_space()
    mesh = V.mesh()
    topo = mesh.topology()
    P = V.ufl_element().degree()
    
    assert mesh.geometry().dim() == 2
    assert get_shape(f) == (2,)
    assert P in (0, 1, 2)
    
    vectors = []
    base_loc = []
    vals = f.vector()
    dms = [V.sub(0).dofmap(), V.sub(1).dofmap()]

    coords = mesh.coordinates()     
    for facet in iterfacets(mesh):
        # Location of the base of the vector
        verts = [coords[i] for i in facet.entities(0)]
        if P in (1, 2):
            base_loc.append(verts[0])
            base_loc.append(verts[1])
        if P in (0, 2):
            base_loc.append(numpy.mean(verts, axis=0))
        
        cell = get_cell(mesh, topo.facet_to_cell[facet.id][0])
        i_loc = topo.cell_to_facet[cell.id].index(facet.id)
        
        dofs = [d.cell_dofs(cell.id) for d in dms]
        for j in range(P+1):
            vx = vals[dofs[0][i_loc*(P+1) + j]]
            vy = vals[dofs[1][i_loc*(P+1) + j]]
            vectors.append((vx, vy))
    
    vectors = numpy.array(vectors)
    base_loc = numpy.array(base_loc, float)
    
    for b, v in zip(base_loc, vectors):
        print '%10.3f %10.3f -> %10.3f %10.3f' % (b[0], b[1], v[0], v[1])
    
    vx = vectors[:,0]
    vy = vectors[:,1]
    maxlen = (vx**2 + vy**2).max()**0.5
    target_maxlen = cell.diameter()*0.3
    if maxlen == 0:
        maxlen = 1
    
    ax.quiver(base_loc[:,0], base_loc[:,1], vx+0.001, vy+0.001,
              angles='xy', scale_units='xy', scale=maxlen/target_maxlen)
