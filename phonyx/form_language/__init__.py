"""
Form language
=============

The form classes serve two purposes:

- Specification of the weak form
- Evaluation of the weak form for given test and trial basis functions

Constants, expressions, test, trial and known functions are called arguments,
the integration domain specifier (dx etc) is called a measure. The form class
holds combination of arguments and measures and ensures that the arguments are
valid weak forms (in this case linear in the test function and with zero or one
trial function)
"""

class FormError(Exception):
    pass

from .vocabulary import TrialFunction, TestFunction, Function, Constant, Expression, Zero
from .vocabulary import TrialFunctions, TestFunctions
from .vocabulary import Circumradius, FacetNormal
from .vocabulary import as_vector, dot, grad, div, inner, outer, nabla_div, nabla_grad
from .vocabulary import jump, avg
from .form import Form, Measure, dx, ds, dS
from .algorithms import system, get_system_dim, get_topmost_function_space
from .algorithms import get_trial_function_restriction, get_test_function_restriction
