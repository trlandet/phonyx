from .. import Vector, MixedFunctionSpace, VectorFunctionSpace, FunctionSpace
from ..function_spaces import leaf_function_spaces, get_function_space_with_first_dof_at_0
from . import FormError 


###############################################################################
# ArgBase - everything in the weak form except the measure inherits ArgBase

class CanBeRestricted(object):
    def __call__(self, restriction):
        if restriction in ('+', '-'):
            return RestrictedArg(restriction, self)
        else:
            raise FormError('Unknown restriction %r' % restriction)


class ArgBase(CanBeRestricted):
    def __add__(self, other):
        other = to_base_arg(other)
        if not isinstance(other, ArgBase):
            return NotImplemented
        return Sum(self, other)
    
    def __radd__(self, other):
        other = to_base_arg(other)
        if not isinstance(other, ArgBase):
            return NotImplemented
        return Sum(other, self)
    
    def __sub__(self, other):
        return self + Constant(-1)*other
    
    def __rsub__(self, other):
        return other + Constant(-1)*self
    
    def __mul__(self, other):
        other = to_base_arg(other)
        if not isinstance(other, ArgBase):
            return NotImplemented
        return self._mul_helper(self, other)
    
    def __rmul__(self, other):
        other = to_base_arg(other)
        if not isinstance(other, ArgBase):
            return NotImplemented
        return self._mul_helper(other, self)
    
    def _mul_helper(self, arg1, arg2):
        s1 = arg1.shape 
        s2 = arg2.shape
        
        if s1 == s2 == ():         
            return Product(arg1, arg2)
        elif s1 == ():
            return FormVector(arg1*arg2[d] for d in range(s2[0]))
        elif s2 == ():
            return FormVector(arg1[d]*arg2 for d in range(s1[0]))
        else:
            raise FormError('Cannot use normal multiplication on shapes %r and %r' % (s1, s2))
    
    def __div__(self, other):
        other = to_base_arg(other)
        if not isinstance(other, ArgBase):
            return NotImplemented
        return Division(self, other)

    def __rdiv__(self, other):
        other = to_base_arg(other)
        if not isinstance(other, ArgBase):
            return NotImplemented
        return Division(other, self)
    
    # The Division class allways performs true division
    __truediv__ = __div__
    __rtruediv__ = __rdiv__
    
    def __neg__(self):
        return Product(Constant(-1), self)
    
    def __pow__(self, power):
        if power == 1:
            return self
        elif power == 2:
            return inner(self, self)
        else:
            raise FormError('Only power 1 and 2 are supported')
        
    def __abs__(self):
        return Abs(self)
    
    def __call__(self, restriction):
        if restriction in ('+', '-'):
            return RestrictedArg(restriction, self)
        else:
            raise FormError('Unknown restriction %r' % restriction)
    
    @property
    def has_trial_function(self):
        from .algorithms import get_trial_functions
        trial_functions = get_trial_functions(self)
        if len(trial_functions) > 1:
            raise FormError('Found %d trial functions' % len(trial_functions))
        return len(trial_functions) == 1
    
    @property
    def trial_function(self):
        from .algorithms import get_trial_functions
        trial_functions = get_trial_functions(self)
        if len(trial_functions) != 1:
            raise FormError('Found %d trial functions' % len(trial_functions))
        return trial_functions[0]
    
    @property
    def has_test_function(self):
        from .algorithms import get_test_functions
        test_functions = get_test_functions(self)
        if len(test_functions) > 1:
            raise FormError('Found %d test functions' % len(test_functions))
        return len(test_functions) == 1
    
    @property
    def test_function(self):
        from .algorithms import get_test_functions
        test_functions = get_test_functions(self)
        if len(test_functions) != 1:
            raise FormError('Found %d test functions' % len(test_functions))
        return test_functions[0]
    
    def print_tree(self):
        from .algorithms import print_form
        print_form(self)


class RestrictedArg(ArgBase):
    def __init__(self, restriction, arg):
        from .algorithms import get_restrictions
        previous_restrictions = get_restrictions(arg)
        if previous_restrictions:
            raise FormError('Double restriction of form argument')
        
        assert restriction in ('+', '-')
        self.restriction = restriction
        self.argument = arg
        
        self.shape = arg.shape
        self.polynomial_degree = arg.polynomial_degree
    
    def __repr__(self):
        return repr(self.argument)+'(%s)' % self.restriction
    
    def eval_cell(self, cell, x, iv=None, iu=None):
        if cell.ignore_restrictions:
            return self.argument.eval_cell(cell, x, iv, iu)
        
        if cell.local_cell_neighbour is None:
            raise FormError('Restricted argument found in non-dS form')
        
        if self.restriction == '-':
            # Swap cell and local coordinate
            x = cell.local_cell_neighbour_x
            cell = cell.local_cell_neighbour
        
        return self.argument.eval_cell(cell, x, iv, iu)
    
    def __getitem__(self, name):
        return RestrictedArg(self.restriction, self.argument[name])


###############################################################################
# Form arguments

class Constant(ArgBase):
    def __init__(self, value):
        self.value = value
        self.polynomial_degree = 0
        
        if isinstance(value, (int, float, long)):
            self.shape = ()
        else:
            self.shape = (len(value),) 
    
    def __repr__(self):
        return 'Constant(%r)' % self.value
    
    def eval_cell(self, cell, x, iv=None, iu=None):
        return self.value
    
    def dx(self, d):
        return Constant(0)
    
    def __getitem__(self, d):
        return Constant(self.value[d])


class Zero(ArgBase):
    def __init__(self, shape):
        """
        A zero tensor with the given shape
        """
        self.shape = shape


class Argument(ArgBase):
    def __init__(self, function_space, function_space_component=None):
        self._function_space = function_space
        self.function_space_component = function_space_component
        self.polynomial_degree = function_space.polynomial_degree
        self.derivative = None
        self.domain = function_space.mesh()
        self.shape = ()
        
    def copy(self, deep=False):
        c = self.__class__(self._function_space, self.function_space_component)
        return c
    
    def dx(self, d):
        du = self.copy()
        du.derivative = d
        return du
    
    def function_space(self):
        return self._function_space


class TrialFunctionImpl(Argument):
    def eval_cell(self, cell, x, iv=None, iu=None):
        element = self._function_space.element
        component = self.function_space_component
        if self.derivative is None:
            return element.evaluate_basis_function(iu, x, cell, component)
        else:
            return element.evaluate_basis_function_derivative(iu, x, cell, self.derivative, component)
        
    def __repr__(self):
        if self.derivative is not None:
            return 'TrialFunction(%r).dx(%d)' % (self._function_space, self.derivative)
        else:
            return 'TrialFunction(%r)' % self._function_space


class TestFunctionImpl(Argument):
    def eval_cell(self, cell, x, iv=None, iu=None):
        element = self._function_space.element
        component = self.function_space_component
        if self.derivative is None:
            return element.evaluate_basis_function(iv, x, cell, component)
        else:
            return element.evaluate_basis_function_derivative(iv, x, cell, self.derivative, component)
        
    def __repr__(self):
        if self.derivative is not None:
            return 'TestFunction(%r).dx(%d)' % (self._function_space, self.derivative)
        else:
            return 'TestFunction(%r)' % self._function_space


def TrialFunction(V):
    if isinstance(V, FunctionSpace):
        shape = V.element.shape
        if shape == ():
            return TrialFunctionImpl(V)
        else:
            funcs = [TrialFunctionImpl(V, comp) for comp in range(shape[0])]
            return as_vector(funcs)
    elif isinstance(V, VectorFunctionSpace):
        funcs = [TrialFunction(Vsub) for Vsub in V.sub_spaces]
        return as_vector(funcs)


def TestFunction(V):
    if isinstance(V, FunctionSpace):
        shape = V.element.shape
        if shape == ():
            return TestFunctionImpl(V)
        else:
            funcs = [TestFunctionImpl(V, comp) for comp in range(shape[0])]
            return as_vector(funcs)
    elif isinstance(V, VectorFunctionSpace):
        funcs = [TestFunction(Vsub) for Vsub in V.sub_spaces]
        return as_vector(funcs)


def TrialFunctions(mixed_space):
    return [TrialFunction(V) for V in mixed_space.sub_spaces]


def TestFunctions(mixed_space):
    return [TestFunction(V) for V in mixed_space.sub_spaces]


# Tell nosetests that these are not a unit tests
TestFunction.__test__ = False
TestFunctions.__test__ = False


###############################################################################
# Known Expressions and Functions

class GloballyEvaluatable(object):
    def eval(self, *args, **kwargs):
        """
        Evaluate this Function or Expression at a global coordinate
        
            # Return value for a global coordinate
            value1 = u.eval([3.14, -10])
            value2 = f.eval([3.14, -10], domain=mesh)
        """
        assert self.shape == (), 'Cannot evaluate non-scalar expressions yet'
        
        # Parse args
        if len(args) == 1:
            x_global = args[0]
        else:
            raise NotImplementedError('Eval with %d arguments not implemented yet' % len(args))
        
        # Parse kwargs
        domain = None
        for key, value in kwargs.items():
            if key == 'domain':
                domain = value
            else:
                raise ValueError('Unknown keyword argument %r' % key)
        
        # Get domain
        if domain is None:
            domain = self.domain
        if domain is None:
            raise ValueError('Cannot interpolate without a domain') 
        
        # Find cell and local coordinates in cell 
        cell = domain.cell_at_coordinate(x_global)
        x_local = cell.local_coordinates(x_global)
        
        return self.eval_cell(cell, x_local)


class Expression(ArgBase, GloballyEvaluatable):
    def __init__(self, code_string, degree=1, domain=None, **kwargs):
        self.code_string = code_string
        self.polynomial_degree = degree
        self.domain = domain
        
        self.eval_arguments = {}
        for key, value in kwargs.items():
            if isinstance(value, Constant):
                value = value.value
            if not isinstance(value, (int, long, float, list, tuple)):
                raise FormError('Unsupported expression parameter type %s=%r' % (key, value))
            self.eval_arguments[key] = value
    
    def eval_cell(self, cell, x, iv=None, iu=None):
        # Make sure x is list-like 
        if not hasattr(x, '__len__'):
            x = [x]
        
        # Get physical coordinates and ensure length 3
        x2 = cell.physical_coordinates(x)
        x3 = [0, 0, 0]
        x3[0:len(x2)] = x2
        
        # Make local namespace for eval
        code_locals = {'x': x3}
        import math
        for attr in dir(math):
            if not attr.startswith('_'):
                code_locals[attr] = getattr(math, attr)
        code_locals.update(self.eval_arguments)
        
        # Evaluate the user provided code string    
        assert self.shape == (), 'You must index/dot vector expressions before evaluating'
        try:
            val = eval(self.code_string, globals(), code_locals)
        except:
            print 'Evaluation of code failed'
            print 'Code:', self.code_string
            print 'x:', x3
            print 'Code arguments:', self.eval_arguments
            raise
        
        assert isinstance(val, (float, int, long)), 'Unexpected type: %r' % type(val)
        
        return val
    
    @property
    def shape(self):
        if isinstance(self.code_string, (list, tuple)):
            return (len(self.code_string),)
        else:
            return ()
    
    def __getitem__(self, index):
        indexed = Expression(self.code_string[index],
                             degree=self.polynomial_degree,
                             domain=self.domain,
                             **self.eval_arguments)
        return indexed
    
    def __len__(self):
        assert isinstance(self.code_string, (list, tuple))
        return len(self.code_string)
    
    def __repr__(self):
        return 'Expression(%r)' % self.code_string


class Function(Argument, GloballyEvaluatable):
    def __init__(self, function_space, function_space_component=None, name='unnamed'):
        super(Function, self).__init__(function_space, function_space_component)
        self._vector = Vector(function_space.dim())
        self._name = name
        
        self.has_nonscalar_function_space = False
        if isinstance(function_space, VectorFunctionSpace):
            self.shape = (len(function_space.sub_spaces), )
        elif isinstance(function_space, MixedFunctionSpace):
            self.shape = None
        elif self.function_space_component is not None:
            self.shape = ()
        else:
            self.shape = function_space.element.shape
            self.has_nonscalar_function_space = self.shape != ()
    
    def name(self):
        return self._name
    
    def rename(self, name, description=None):
        """
        Rename the variable. The description is not used and is just there for
        compatibility with dolfin
        """
        self._name = name
    
    def eval_cell(self, cell, x, iv=None, iu=None):
        assert not isinstance(self._function_space, MixedFunctionSpace)
        element = self._function_space.element
        component = self.function_space_component
        dofs = self._function_space.dofmap().cell_dofs(cell.id)
        val = 0
        for i, dof in enumerate(dofs):
            if self.derivative is None:
                val += self._vector[dof]*element.evaluate_basis_function(i, x, cell, component)
            else:
                val += self._vector[dof]*element.evaluate_basis_function_derivative(i, x, cell, self.derivative, component)
        return val
    
    def assign(self, other):
        """
        Copy the other function's vector values into this function
        """
        N1 = self._function_space.dim()
        N2 = len(self._vector)
        N3 = self._function_space.dim()
        N4 = len(other._vector)
        assert N1 == N2, 'Cannot assign to shallow copy'
        assert N3 == N4, 'Cannot assign from shallow copy'
        assert N1 == N3, 'Cannot assign from different function space'
        
        e0 = self._function_space.ufl_element()
        e1 = other._function_space.ufl_element()
        assert e0.family() == e1.family(), 'Cannot assign from different function space (degree)'
        assert e0.degree() == e1.degree(), 'Cannot assign from different function space (family)'
        assert e0.value_shape() == e1.value_shape(), 'Cannot assign from different function space (shape)'
        self._vector[:] = other._vector
    
    def vector(self):
        return self._vector
    
    def copy(self, deep=False):
        V = self._function_space
        name = self._name
        
        if deep:
            V_uncoupled = get_function_space_with_first_dof_at_0(V)
            c = Function(V_uncoupled, name=name)
            i_start = V.dofmap().start_at
            i_end = i_start + V.dim()
            c._vector[:] = self._vector[i_start:i_end]
        else:
            c = Function(V, name=name)
            c._vector = self._vector
        
        return c
    
    def split(self, deepcopy=False):
        """
        Split function into subfunctions 
        """
        assert self.function_space_component is None
        funcspaces = leaf_function_spaces(self._function_space, max_depth=1)
        res = []
        for i, V in enumerate(funcspaces):
            name = '%s[%d]' % (self._name, i)
            
            if deepcopy:
                V_uncoupled = get_function_space_with_first_dof_at_0(V)
                f = Function(V_uncoupled, name=name)
                i_start = V.dofmap().start_at
                i_end = i_start + V.dim()
                f._vector[:] = self._vector[i_start:i_end]
            else:
                f = Function(V, name=name)
                f._vector = self._vector
            
            res.append(f)
        return res
    
    def __getitem__(self, index):
        if self.has_nonscalar_function_space:
            assert self.function_space_component is None
            name = '%s[%d]' % (self._name, index)
            f = Function(self._function_space, index, name=name)
            f._vector[:] = self._vector
            return f
        else:
            return self.split()[index]
    
    def __repr__(self):
        return 'Function(%r, %r, %r)' % (self._function_space, self.function_space_component, self._name)


###############################################################################
# Special functions

class SpecialFunction(ArgBase):
    polynomial_degree = 0


class Circumradius(SpecialFunction):
    def __init__(self, mesh):
        self.mesh = mesh
        self.shape = ()
    
    def eval_cell(self, cell, x, iv=None, iu=None):
        return cell.diameter()/2

    def __repr__(self):
        return 'Circumradius()'


class FacetNormal(SpecialFunction):
    def __init__(self, mesh, component=None):
        self.mesh = mesh
        self.component = component
        self.shape = (mesh.dim,) if component is None else ()
    
    def eval_cell(self, cell, x, iv=None, iu=None):
        assert self.component is not None
        normal = cell.normal()
        return normal[self.component]
    
    def __getitem__(self, component):
        assert 0 <=  component < self.shape[0]
        return FacetNormal(self.mesh, component)
    
    def __repr__(self):
        if self.component is not None:
            return 'FacetNormal(component=%d)' % self.component
        else:
            return 'FacetNormal()'


###############################################################################
# Form operators

class Operator(ArgBase):
    def __repr__(self):
        return '%s(%r)' % (self.__class__.__name__, self.arguments)


class Sum(Operator):
    def __init__(self, *args):
        assert len(args) > 1
        self.arguments = []
        for arg in args:
            if isinstance(arg, Sum):
                self.arguments.extend(arg.arguments)
            else:
                self.arguments.append(arg)
                
    def dx(self, d):
        return Sum(*[a.dx(d) for a in self.arguments])
    
    @property
    def shape(self):
        shapes = set()
        for arg in self.arguments:
            shapes.add(arg.shape)
        
        if len(shapes) != 1:
            raise FormError('Inconsistent shapes in sum')
        
        return shapes.pop()
    
    def eval_cell(self, cell, x, iv=None, iu=None):
        return sum(arg.eval_cell(cell, x, iv, iu) for arg in self.arguments)
    
    @property
    def polynomial_degree(self):
        return max(arg.polynomial_degree for arg in self.arguments)
    
    def __getitem__(self, index):
        ret = Sum(None, None)
        ret.arguments = [arg[index] for arg in self.arguments]
        return ret


class Product(Operator):
    def __init__(self, *args):
        assert len(args) > 1
        self.arguments = []
        for arg in args:
            if isinstance(arg, Product):
                self.arguments.extend(arg.arguments)
            else:
                self.arguments.append(arg)
    
    def dx(self, d):
        first = self.arguments[0]
        if len(self.arguments) == 2:
            rest = self.arguments[1]
        else:
            rest = Product(*self.arguments[1:])
        
        return first.dx(d)*rest + first*rest.dx(d)
    
    @property
    def shape(self):
        shapes = set()
        found_non_scalar = False
        for arg in self.arguments:
            s = arg.shape
            shapes.add(s)
            if len(s):
                assert not found_non_scalar, 'Use inner or dot for non-scalar multiplication'
                found_non_scalar = True
        
        if len(shapes) == 1:
            shape = shapes.pop()
            assert shape == (), 'Use inner or dot for non-scalar multiplication'
            return shape
        
        elif len(shapes) == 2:
            shape1 = shapes.pop()
            shape2 = shapes.pop()
            if shape1 == ():
                return shape2
            if shape2 == ():
                return shape1
        
        raise FormError('Inconsistent shapes in product, user inner or dot for non-scalar multiplication')
    
    def eval_cell(self, cell, x, iv=None, iu=None):
        res =  1
        for args in self.arguments:
            res *= args.eval_cell(cell, x, iv, iu)
        return res
        
    @property
    def polynomial_degree(self):
        return sum(arg.polynomial_degree for arg in self.arguments)
    
    def __getitem__(self, index):
        assert len(self.shape) < 2
        ret = Product(None, None)
        ret.arguments = []
        for arg in self.arguments:
            if arg.shape == ():
                ret.arguments.append(arg)
            elif len(arg.shape) == 1:
                ret.arguments.append(arg[index])
            else:
                raise FormError('Inconsistent shapes in indexed form product')
        return ret


class Division(Operator):
    def __init__(self, arg1, arg2):
        if arg2.shape != ():
            raise FormError('Cannot divide by non-scalar. Shape of %r is %r' % (arg2, arg2.shape))
        self.arguments = [arg1, arg2]
    
    @property
    def shape(self):
        return self.arguments[0].shape
    
    def eval_cell(self, cell, x, iv=None, iu=None):
        numerator, denominator = self.arguments
        num_val = numerator.eval_cell(cell, x, iv, iu)
        dem_val = denominator.eval_cell(cell, x, iv, iu)
        # Ensure true division 
        return  num_val / float(dem_val)
    
    @property
    def polynomial_degree(self):
        # Guestimate
        return max(arg.polynomial_degree for arg in self.arguments)
    
    def __getitem__(self, index):
        assert len(self.shape) < 2
        return Division(self.arguments[0][index], self.arguments[1])


class Abs(Operator):
    def __init__(self, arg1):
        self.arguments = [arg1]
        self.shape = arg1.shape
        self.polynomial_degree = arg1.polynomial_degree 
    
    def eval_cell(self, cell, x, iv=None, iu=None):
        return abs(self.arguments[0].eval_cell(cell, x, iv, iu))
    
    def __getitem__(self, index):
        return Abs(self.arguments[0][index])


###############################################################################
# Vector calculus

class FormVector(list, CanBeRestricted):
    @property
    def shape(self):
        return (len(self),) + self[0].shape
    
    @property
    def polynomial_degree(self):
        deg = self[0].polynomial_degree
        for arg in self:
            assert arg.polynomial_degree == deg
        return deg

    @property
    def T(self):
        shape = self.shape
        rank = len(shape)
        
        if rank == 1:
            return self
        elif rank != 2:
            raise FormError('Transpose of tensor with rank %d not supported' % rank)
        
        ret = FormVector()
        for i in range(shape[1]):
            ret.append(FormVector(self[j][i] for j in range(shape[0])))
        return ret
    
    def __add__(self, other):
        assert other.shape == self.shape
        N = self.shape[0]
        return FormVector(self[i]+other[i] for i in range(N))
    
    __radd__ = __add__
    
    def __sub__(self, other):
        assert other.shape == self.shape
        N = self.shape[0]
        return FormVector(self[i] - other[i] for i in range(N))
    
    def __rsub__(self, other):
        assert other.shape == self.shape
        N = self.shape[0]
        return FormVector(other[i] - self[i] for i in range(N))
    
    def __mul__(self, other):
        other = to_base_arg(other)
        if not isinstance(other, ArgBase):
            return NotImplemented
        assert other.shape == (), 'Use inner or dot for non-scalar multiplication'
        return FormVector(arg*other for arg in self)
    
    def __div__(self, other):
        assert other.shape == (), 'Cannot divide by non-scalar'
        return FormVector(arg/other for arg in self)
    
    def __rdiv__(self, other):
        raise FormError('Cannot devide by a FormVector')
    
    def __abs__(self):
        return Abs(self)
    
    def __repr__(self):
        return 'FormVector(%s)' % list.__repr__(self)
    
    def print_tree(self):
        from .algorithms import print_form
        print_form(self)
    
    __rmul__ = __mul__


def as_vector(iterable):
    """
    Return a FormVector built from the input iterable
    """
    return FormVector(to_base_arg(arg) for arg in iterable) 


def dot(arg1, arg2):
    """
    Dot product of two FormVectors
    """
    s1 = arg1.shape
    s2 = arg2.shape
    
    if s1 == s2 == ():
        # Two scalars
        res = arg1*arg2
    
    elif len(s1) == len(s2) == 1:
        res = arg1[0]*arg2[0]
        for i in range(1, s1[0]):
            res += arg1[i]*arg2[i]
            
    elif len(s1) == 1 and len(s2) == 2:
        assert s1[0] == s2[0]
        res = FormVector()
        for i in range(s2[1]):
            col = FormVector([arg2[j][i] for j in range(s2[0])])
            res.append(dot(arg1, col))
    
    elif len(s1) == 2 and len(s2) == 1:
        assert s1[1] == s2[0]
        res = FormVector()
        for i in range(s1[0]):
            row = arg1[i]
            res.append(dot(row, arg2))
    
    else:
        raise FormError('Dot product between arguments with shapes %r and %r not supported' % (s1, s2))
    
    return res


def grad(arg):
    """
    Gradient of an argument. Returns a FormVector
    """
    from .algorithms import get_domain
    domain = get_domain(arg)
    
    rank = len(arg.shape)
    if rank == 0:
        ret = FormVector(arg.dx(i) for i in range(domain.dim))
    elif rank == 1:
        ret = FormVector(grad(item) for item in arg)
    else:
        raise FormError('grad not implemented for higher order tensors')
    
    return ret


def nabla_grad(arg):
    """
    Gradient of an argument. Supports gradients of vectors which results in
    standard second order tensors (normal vector calculus rules)
    """
    return grad(arg).T


def div(vec):
    """
    Divergence of an argument which must be a FormVector
    """
    from .algorithms import get_domain
    
    if not isinstance(vec, FormVector):
        raise FormError('div argument must be a FormVector, got %r' % vec)
    
    domain = get_domain(vec)
    assert len(vec) == domain.dim
    
    rank = len(vec.shape)
    if rank == 1:
        ret = vec[0].dx(0)
        for i in range(1, domain.dim):
            ret += vec[i].dx(i)
    elif rank == 2:
        ret = FormVector(div(item) for item in vec)
    else:
        raise FormError('div not implemented for higher order tensors')
    
    return ret


def nabla_div(vec):
    """
    Divergence of an argument which must be a FormVector
    Supports gradients of second order tensors which results in
    vectors (normal vector calculus rules)
    """
    return div(vec.T)


def inner(arg1, arg2):
    """
    Inner product of two tensors. Equivalent to a dot product 
    for scalars and vectors
    """
    is_tensor1 = arg1.shape != () and arg1[0].shape != ()
    is_tensor2 = arg2.shape != () and arg2[0].shape != ()
     
    if not (is_tensor1 or is_tensor2):
        return dot(arg1, arg2)
    
    res = dot(arg1[0], arg2[0]) 
    for i in range(1, len(arg1)):
        res += dot(arg1[i], arg2[i])
    return res


def outer(arg1, arg2):
    """
    Outer product of two vectors. Returns a tensor
    """
    N, = arg1.shape
    M, = arg2.shape
    assert N == M
    T = FormVector()
    for i in range(N):
        T.append(FormVector(arg1[i]*arg2[j] for j in range(N)))
    return T


###############################################################################
# DG operators

def jump(u, n=None):
    if n is None:
        return u('+') - u('-')
    elif u.shape == ():
        return u('+')*n('+') + u('-')*n('-')
    else:
        return dot(u('+'), n('+')) + dot(u('-'), n('-'))


def avg(u):
    return (u('+') + u('-'))/2


###############################################################################
# Utility functions

def to_base_arg(arg):
    """
    Convert any Python numbers found to form Constants
    """
    # Handle Python numbers
    if isinstance(arg, (float, int, long)):
        arg = Constant(arg)
    return arg

