from . import FormError
from ..mesh import AbstractMesh, MeshFunction
from .vocabulary import Zero, Constant


###############################################################################
# The weak form

class Form(object):
    def __init__(self, args=None, measure=None):
        """
        A finite element weak form
        
        The Form class holds a collection of one or more integrals. An integral
        is an argument (e.g u*v) and a measure (e.g dx) The form class verifies
        that the argument is valid (can be assembled to a matrix or a vector)
        """
        from .algorithms import get_restrictions, get_unrestricted, expand
        self.sub_forms = []
        
        if isinstance(args, Form):
            # Copy this form (in FEniCS this would create a dolfin form from an
            # ufl form, but since we only have one type of form this is trivial)
            self.sub_forms = [(a, m) for a, m in args.sub_forms]
        
        elif args is not None:
            assert measure is not None
            
            ##############################################
            # Check DG restriction of form arguments 
            if measure.type != 'dS':
                ignore_restrictions = measure.parameters.get('ignore_restrictions', False)
                if not ignore_restrictions and get_restrictions(args):
                    raise FormError('Found restricted arguments in %s form' % measure.type)
            else:
                unrestricted = get_unrestricted(args)
                if unrestricted:
                    raise FormError('Found unrestricted arguments in dS form: %r' % unrestricted)
            
            ##############################################
            # Check linearity of form arguments
            if isinstance(args, Zero):
                # This signifies i.e an empty RHS (no linear form exists)
                self.sub_forms.append((args, measure))
            
            else:
                # Expand expressions such as "(u - u0)*v*dx" into two sub forms and then
                # check linearity of the sub-forms before storing them
                for exp_arg in expand(args):
                    # This will raise an error if there are more than one trial function
                    has_trial = exp_arg.has_trial_function
                    
                    # This will raise an error if there are more than one test function
                    has_test = exp_arg.has_test_function
                    
                    if has_trial and not has_test:
                        raise FormError('Form with trial function without test function is invalid')
                    
                    self.sub_forms.append((exp_arg, measure))
    
    def __add__(self, other):
        """
        Add another integral to this weak form
        """
        assert isinstance(other, Form)
        self.sub_forms.extend(other.sub_forms)
        return self
    
    def __sub__(self, other):
        """
        Subtract another integral from this weak form
        """
        assert isinstance(other, Form)
        for args, measure in other.sub_forms:
            self.sub_forms.append((Constant(-1)*args, measure))
        return self
    
    def __neg__(self):
        """
        Change sign of form
        """
        negated = Form()
        for args, measure in self.sub_forms:
            negated.sub_forms.append((-1 * args, measure))
        return negated
    
    def __repr__(self):
        return '<Form %r>' % (self.sub_forms,)
    
    @property
    def dim(self):
        """
        Calculate the dimension of the equation system
        """
        N = None
        for (args, _measure) in self.sub_forms:
            if isinstance(args, Zero):
                # This form is empty
                n = args.shape[0]
            
            else:
                # It has been checked in the constructor that there is zero or
                # one test functions in the args
                from .algorithms import get_test_functions, get_system_dim
                test_funcs = get_test_functions(args)
                if test_funcs:
                    n = get_system_dim(test_funcs[0])
                else:
                    n = 0
            
            assert N is None or N == n
            N = n
        
        return N
    
    @property
    def domain(self):
        from .algorithms import get_domain
        return get_domain(self)


###############################################################################
# Measures

class Measure(object):
    def __init__(self, measure_type, **parameters):
        self.type = measure_type
        self.parameters = {}
        self._update_params(parameters)
        
    def _update_params(self, parameters):
        for key, value in parameters.items():
            if key == 'domain':
                assert isinstance(value, AbstractMesh)
            elif key == 'degree':
                assert isinstance(value, (int, long)) and value >= 0
            elif key == 'subdomain_data':
                assert isinstance(value, MeshFunction)
            elif key == 'selected_subdomain':
                assert 'subdomain_data' in self.parameters or \
                       'subdomain_data' in parameters
                assert isinstance(value, int)
            elif key == 'ignore_restrictions':
                assert isinstance(value, bool)
            else:
                raise FormError('Unknown measure parameter %r' % key)
            self.parameters[key] = value
    
    def __rmul__(self, args):
        return Form(args, self)
    
    def __call__(self, *args, **kwargs):
        if args:
            assert len(args) == 1
            kwargs['selected_subdomain'] = args[0]
        res = Measure(self.type, **self.parameters)
        res._update_params(kwargs)
        return res
    
    def __repr__(self):
        return '<Measure %s>' % self.type
    
    def __getitem__(self, item):
        return self(subdomain_data=item)


dx = Measure('dx') # Integral over cells
ds = Measure('ds') # Integral over external facets 
dS = Measure('dS') # Internal over internal facets
