"""
Form algorithms and utility functions
"""
import itertools
from . import FormError
from .vocabulary import RestrictedArg, TestFunctionImpl, TrialFunctionImpl
from .vocabulary import Operator, Sum, Product, Division, Abs
from .vocabulary import Zero, Constant, Argument, Expression, SpecialFunction, FormVector
from .form import Form, dx


###############################################################################
# Externally available functions (user API)

def system(form):
    """
    Split form into the bilinear, a, and the linear form, L
    returns a, L
    """
    # Find bilinear and linear integrals
    a, L = [], []
    for args, measure in form.sub_forms:
        if args.has_trial_function:
            a.append(args*measure)
        else:
            L.append(args*measure)
    
    # Dimension of matrices
    N = form.dim
    
    # Create bilinear form
    if a:
        a_form = sum(a, Form())
    else:
        a_form = Form(Zero((N, N)), dx)
    
    if L:
        L_form = sum(L, Form())
    else:
        L_form = Form(Zero((N, 1)), dx)
    
    return a_form, -L_form


###############################################################################
# Internal functions

def visit_all(visitor, tree, only_leaves=False):
    """
    Run the visitor function on each node in the expression
    
    Leaf nodes are the normal arguments (non operators) in the expression
    """
    stack = [tree]
    while stack:
        item = stack.pop()
        
        children = get_children(item)
        if children is not None:
            # First treat non-leaf nodes. If only_leaves is False we run
            # the visitor also on these nodes. If the visitor returns False
            # (and not True or None) then we do not go deeper in this branch 
            deeper = True if only_leaves else (visitor(item) is not False)
            if deeper:
                stack.extend(children)
            continue
        
        else:
            # This is a leaf node, lets visit it
            visitor(item)


def visit_leaves(visitor, tree):
    """
    Run the visitor function on each leaf node in the expression
    
    Leaf nodes are the normal arguments (non operators) in the expression
    """
    return visit_all(visitor, tree, only_leaves=True)


def is_leaf(args):
    """
    Check if the argument is a leaf node in an expression
    """
    return isinstance(args, (Zero, Constant, Argument, Expression, SpecialFunction))


def get_children(args):
    """
    Returns list of children for parent nodes and None for non-parents
    """
    if isinstance(args, Operator):
        return args.arguments
    elif isinstance(args, FormVector):
        return args
    elif isinstance(args, RestrictedArg):
        return [args.argument]
    else:
        assert is_leaf(args), 'Found unexpected non-leaf argument %r' % args
        return None


def get_trial_functions(args):
    """
    Return all trial function objects found in the arguments
    """
    funcs = []
    def counter(arg):
        if isinstance(arg, TrialFunctionImpl):
            funcs.append(arg)
    visit_leaves(counter, args)
    return funcs


def get_test_functions(args):
    """
    Return all test function objects found in the arguments
    """
    funcs = []
    def counter(arg):
        if isinstance(arg, TestFunctionImpl):
            funcs.append(arg)
    visit_leaves(counter, args)
    return funcs


def get_restrictions(args):
    """
    Return all restrictions found in the arguments
    """
    restrictions = set()
    def counter(arg):
        if isinstance(arg, RestrictedArg):
            restrictions.add(arg.restriction)
    visit_all(counter, args)
    return list(restrictions)


def get_unrestricted(args):
    """
    Returns all leaf nodes in the arguments that are unrestricted 
    """
    unrestricted = []
    def checker(arg):
        if is_leaf(arg) and not isinstance(arg, Constant):
            unrestricted.append(arg)
        # Do not go deeper if this is a restriction
        deeper = not isinstance(arg, RestrictedArg) 
        return deeper
    
    visit_all(checker, args)
    return unrestricted


def _get_restriction(args, target_leaf_class):
    """
    Given a restricted DG form argument, return the restriction ('+' or '-') of
    the trial or test function (depending on which target_leaf_class is passed)
    
    The argument must have been expanded before calling such that there is one
    and only one object of the class in the arguments  
    """
    stack = [(None, args)]
    while stack:
        restriction, item = stack.pop()
        
        if isinstance(item, RestrictedArg):
            restriction = item.restriction
            stack.append((restriction, item.argument))
        
        children = get_children(item)
        if children is not None:
            for child in children:
                stack.append((restriction, child))
        elif isinstance(item, target_leaf_class):
            if restriction not in ('+', '-'):
                raise FormError('No restriction found on the trial function')
            return restriction
    
    raise FormError('No trial function found')


def get_trial_function_restriction(args):
    """
    Return the restriction ('+' or '-') on the trial function.
    """
    return _get_restriction(args, TrialFunctionImpl)
        

def get_test_function_restriction(args):
    """
    Return the restriction ('+' or '-') on the test function.
    """
    return _get_restriction(args, TestFunctionImpl)


def get_domain(form_or_arg):
    """
    Get the domain (mesh) of the input which can be a form or a form argument
    """
    domains = set()
    def domain_picker(arg):
        if hasattr(arg, 'domain') and arg.domain is not None:
            domains.add(arg.domain)
    
    if isinstance(form_or_arg, Form):
        for args, _measure in form_or_arg.sub_forms:
            visit_leaves(domain_picker, args)
    else:
        visit_leaves(domain_picker, form_or_arg)
    
    if len(domains) == 1:
        return domains.pop()
    else:
        print domains
        raise FormError('There must be one unique domain, found %d' % len(domains))


def get_system_dim(arg):
    """
    Given a test or trial function, return the dimensions of the 
    linear equation system. This handles mixed spaces by first
    finding the topmost parent and then asking its dimension
    """
    V = get_topmost_function_space(arg)
    return V.dim()


def get_topmost_function_space(arg):
    """
    Given a test or trial function, return the topmost function space
    """
    V = arg.function_space()
    while V.parent_space is not None:
        V = V.parent_space
    return V


def expand(args):
    """
    Expand arguments containing sums into multiple arguments  
    """
    expansions = []
    
    if isinstance(args, Sum):    
        # Gather all sub-combinations
        for a in args.arguments:
            expansions.extend(expand(a))
    
    elif isinstance(args, Product):
        # Gather all sub-combinations
        outer = []
        for a in args.arguments:
            outer.append(expand(a))
        
        # n-fold Cartesian product of sub-combinations
        all_combos = itertools.product(*outer)
        for combo in all_combos:
            val = combo[0]
            for c in combo[1:]:
                val *= c
            expansions.append(val)
    
    elif isinstance(args, Division):
        # Gather all sub-combinations in the numenator
        numerator, denominator = args.arguments
        for a in expand(numerator):
            expansions.append(Division(a, denominator))
    
    elif isinstance(args, Abs):
        # Cannot expand absolute value operator
        expansions.append(args)
    
    elif isinstance(args, RestrictedArg):
        for a in expand(args.argument):
            expansions.append(RestrictedArg(args.restriction, a))
    
    else:
        # Do not know how to expand this (should be a leaf node)
        assert is_leaf(args), 'Expected leaf node, found: %r' % args
        expansions.append(args)
    
    return expansions


def print_form(args, level=0):
    """
    Debug helper
    """
    children = get_children(args) 
    print ' ' * (level*2-1), args.__class__.__name__
    if children is not None: 
        for c in children:
            print_form(c, level+1)
