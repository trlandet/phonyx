from __future__ import division 
import numpy

# Construct table of Gauss-Legendre quadrature rules in 1D
GL_QUADRATURE_TABLE = {}
for n in range(1, 21):
    points, weights = numpy.polynomial.legendre.leggauss(n)
    GL_QUADRATURE_TABLE[n] = (points, weights)
    
# Dunavanant's barycentric triangle quadrature rules
BT_QUADRATURE_TABLE = {}
BT_QUADRATURE_TABLE[1] = ([[1/3, 1/3]],
                          [1.0])
BT_QUADRATURE_TABLE[2] = ([[2/3, 1/6], [1/6, 2/3], [1/6, 1/6]],
                          [1/3, 1/3, 1/3])
BT_QUADRATURE_TABLE[3] = ([[1/3, 1/3], [.6, .2], [.2, .6], [.2, .2]],
                          [-9/16, 25/48, 25/48, 25/48])
BT_QUADRATURE_TABLE[4] = ([[0.108103018168070, 0.445948490915965],
                           [0.445948490915965, 0.108103018168070],
                           [0.445948490915965, 0.445948490915965],
                           [0.816847572980459, 0.091576213509771],
                           [0.091576213509771, 0.816847572980459],
                           [0.091576213509771, 0.091576213509771]],
                          [0.223381589678011, 0.223381589678011, 0.223381589678011,
                           0.109951743655322, 0.109951743655322, 0.109951743655322])
BT_QUADRATURE_TABLE[5] = ([[1/3, 1/3],
                           [0.059715871789770, 0.470142064105115],
                           [0.470142064105115, 0.059715871789770],
                           [0.470142064105115, 0.470142064105115],
                           [0.797426985353087, 0.101286507323456],
                           [0.101286507323456, 0.797426985353087],
                           [0.101286507323456, 0.101286507323456]],
                          [0.225000000000000,
                           0.132394152788506, 0.132394152788506, 0.132394152788506,
                           0.125939180544827, 0.125939180544827, 0.125939180544827])
BT_QUADRATURE_TABLE[6] = ([[0.501426509658179, 0.249286745170910],
                           [0.249286745170910, 0.501426509658179],
                           [0.249286745170910, 0.249286745170910],
                           [0.873821971016996, 0.063089014491502],
                           [0.063089014491502, 0.873821971016996],
                           [0.063089014491502, 0.063089014491502],
                           [0.053145049844817, 0.310352451033784],
                           [0.053145049844817, 0.636502499121399],
                           [0.310352451033784, 0.053145049844817],
                           [0.310352451033784, 0.636502499121399],
                           [0.636502499121399, 0.310352451033784],
                           [0.636502499121399, 0.053145049844817]],
                          [0.116786275726379, 0.116786275726379, 0.116786275726379,
                           0.050844906370207, 0.050844906370207, 0.050844906370207,
                           0.082851075618374, 0.082851075618374, 0.082851075618374,
                           0.082851075618374, 0.082851075618374, 0.082851075618374])


def quadrature_degree(form, args, measure, dimension_reduction=0):
    """
    Return the needed quadrature order
    """
    gdim = form.domain.dim
    Pargs = args.polynomial_degree
    P = measure.parameters.get('degree', Pargs)
    
    topological_dimension = gdim - dimension_reduction
    if topological_dimension == 0:
        # This is a single point in space, quadrature order is irrelevant 
        return -1
    
    elif topological_dimension == 1:
        # This is a line - we can use Gauss-Legendre
        return int(numpy.ceil((P + 1) / 2.0))
    
    elif gdim - dimension_reduction == 2:
        # This is a triangle, we use Barycentric cubature rules
        return max(1, P)


def integrate_cell(cell, args, quadrature_degree, iv=None, iu=None):
    """
    Integrate the weak form specified by args over the cell
    """
    if cell.mesh.dim == 1:
        scale = cell.volume()/2
        points, weights = GL_QUADRATURE_TABLE[quadrature_degree]
        val = 0
        for point, weight in zip(points, weights):
            val += weight*args.eval_cell(cell, [point], iv, iu)
        return val*scale
    
    elif cell.mesh.dim == 2:
        scale = cell.volume()
        points, weights = BT_QUADRATURE_TABLE[quadrature_degree]
        val = 0
        for point, weight in zip(points, weights):
            val += weight*args.eval_cell(cell, point, iv, iu)
        return val*scale
    
    else:
        raise NotImplementedError('Cell quadrature for dimensions above 2 is not implemented')


def integrate_facet(cell, args, quadrature_degree, iv=None, iu=None, calc_neighbour_x=False):
    """
    Integrate the weak form specified by args over the facet
    """
    assert cell.local_facet is not None
    facet = cell.local_facet
    
    if facet.mesh.dim == 1:
        assert quadrature_degree == -1
        point = cell.local_coordinates(facet.vertices[0])
        
        if calc_neighbour_x:
            # Calculate the coordinate of the facet in the DG('-') cell
            cell2 = cell.local_cell_neighbour
            neighbour_x = cell2.local_coordinates(facet.vertices[0])
            cell.local_cell_neighbour_x = neighbour_x
        
        val = args.eval_cell(cell, point, iv, iu)
        
        if calc_neighbour_x:
            cell.local_cell_neighbour_x = None
        
        return val
    
    elif cell.mesh.dim == 2:
        # Local coordinates of 1D line quadrature points
        points_on_facet, weights = GL_QUADRATURE_TABLE[quadrature_degree]
        
        # Facet area
        verts = facet.vertices
        a = verts[0][0]-verts[1][0]
        b = verts[0][1]-verts[1][1]
        scale = (a**2 + b**2)**0.5/2
        
        # Find the barycentric coordinates of the quadrature points (in this cell)
        scale, points_in_cell = _calc_barycentric_coordinate_2d(points_on_facet, facet, cell)
        
        if not calc_neighbour_x:
            # Normal ds integral with no DG restrictions
            val = 0
            for point, weight in zip(points_in_cell, weights):
                val += weight*args.eval_cell(cell, point, iv, iu)
            return val*scale
        
        else:
            # Discontinuous Galerkin dS integral where we need to calculate local coordinates
            # in the neighbour cell in case we need to evaluate on the '-' side
              
            # Find the barycentric coordinates of the quadrature points (in the neighbour cell)
            cell2 = cell.local_cell_neighbour
            scale, neighbour_x = _calc_barycentric_coordinate_2d(points_on_facet, facet, cell2)
            
            val = 0
            for point, point2, weight in zip(points_in_cell, neighbour_x, weights):
                cell.local_cell_neighbour_x = point2
                val += weight*args.eval_cell(cell, point, iv, iu)
            
            cell.local_cell_neighbour_x = None
            return val*scale
    
    else:
        raise NotImplementedError('Cell quadrature for dimensions above 2 is not implemented')


def _calc_barycentric_coordinate_2d(points_on_facet, facet, cell):
    verts = facet.vertices
    a = verts[0][0]-verts[1][0]
    b = verts[0][1]-verts[1][1]
    scale = (a**2 + b**2)**0.5/2
    
    # Find global coordinates of 1D line quadrature points in 2D
    points_global = [(verts[0][0] - a*(p+1)/2, verts[0][1] - b*(p+1)/2) for p in points_on_facet]
    
    # Find 2D barycentric coordinates
    points_in_cell = [cell.local_coordinates(point) for point in points_global]
    
    return scale, points_in_cell
