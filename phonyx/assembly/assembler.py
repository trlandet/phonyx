import numpy
from ..linalg import GenericMatrix, GenericVector, Matrix, Vector
from ..form_language import Zero, get_system_dim, get_trial_function_restriction, get_test_function_restriction
from .local_assembler import assemble_local
from .quadrature import quadrature_degree, integrate_cell, integrate_facet
from ..timing import Timer


def assemble(form, use_local_assembler=False):
    """
    Assemble a weak form. The form must either be a bilinear form or a linear
    form, mixing the two is not supported. The function returns a Matrix or
    a Vector depending on the input 
    """
    N = form.dim
    
    # Find out if this is a N x N matrix, a N x 1 vector or a scalar
    Ms = set()
    for args, _measure in form.sub_forms:
        if isinstance(args, Zero):
            # Empty form
            Ms.add(args.shape[1])
            
        elif not args.has_trial_function:
            # No trial function -> linear form
            Ms.add(0)
        
        else:
            # Has trial function -> bilinear form
            Ms.add(get_system_dim(args.trial_function))
    
    # Sanity checks
    if len(Ms) != 1:
        raise ValueError('Found inconsistent form dimensions in assemble: %r' % Ms)
    M = Ms.pop()
    
    if N == 0:
        assert M == 0
        s = 0
        for args, measure in form.sub_forms:
            # Check for empty form
            if isinstance(args, Zero):
                assert args.shape == (N, M)
                continue
            
            if measure.type == 'dx':
                s += assemble_scalar_dx(form, args, measure)
            elif measure.type == 'ds':
                s += assemble_scalar_ds(form, args, measure)
            elif measure.type == 'ds':
                s += assemble_scalar_dS(form, args, measure)
            else:
                raise NotImplementedError('Cannot assemble %r measure' % measure.type)
        return s
    
    elif M > 0:
        A = Matrix(N, M)
        for args, measure in form.sub_forms:
            # Check for empty form
            if isinstance(args, Zero):
                assert args.shape == (N, M)
                continue
            
            if measure.type == 'dx':
                assemble_matrix_dx(form, args, measure, A)
            elif measure.type == 'ds':
                assemble_matrix_ds(form, args, measure, A)
            elif measure.type == 'dS':
                assemble_matrix_dS(form, args, measure, A)
            else:
                raise NotImplementedError('Cannot assemble %r measure' % measure.type)
        return A
     
    elif M == 0:
        b = Vector(N)
        
        if use_local_assembler:
            # Assemble each cell on its own
            for cell in form.domain.cells:
                b_local, dofs = assemble_local(form, cell, return_dofs=True)
                assert dofs[1] is None
                for i, idx in enumerate(dofs[0]):
                    b[idx] += b_local[i]
        
        else:
            # Assemble all cells together
            for args, measure in form.sub_forms:
                # Check for empty form
                if isinstance(args, Zero):
                    assert args.shape == (N, 1)
                    continue
                
                if measure.type == 'dx':
                    assemble_vector_dx(form, args, measure, b)
                elif measure.type == 'ds':
                    assemble_vector_ds(form, args, measure, b)
                elif measure.type == 'dS':
                    assemble_vector_dS(form, args, measure, b)
                else:
                    raise NotImplementedError('Cannot assemble %r measure' % measure.type)
        
        return b
    
    else:
        raise ValueError('Cannot assemble %d by %d matrix' % (M, N))


def assemble_system(A_form, b_form, bcs=None):
    """
    Assemble a linear and a bilinear form. Return a matrix and a vector with
    optional application of boundary conditions
    """
    A = assemble(A_form)
    b = assemble(b_form)
    assert isinstance(A, GenericMatrix) and isinstance(b, GenericVector)
    
    # Apply boundary conditions
    if bcs is not None:
        if hasattr(bcs, '__iter__'):
            for bc in bcs:
                bc.apply(A, b, keep_symmetry=True)
        else:
            bcs.apply(A, b, keep_symmetry=True)
    
    return A, b


def assemble_matrix_dx(form, args, dx, A):
    """
    Assemble the given sub-form into the matrix A
    Integrate over cells
    """
    timer = Timer('Assemble matrix dx')
    u = args.trial_function
    v = args.test_function
    V_u = u.function_space()
    V_v = v.function_space()
    
    element_u = V_u.element
    element_v = V_v.element
    N_u = element_u.num_basis_functions
    N_v = element_v.num_basis_functions
    
    mesh = form.domain
    Q = quadrature_degree(form, args, dx)
    
    # Build global matrix by summing cell contributions
    element_matrix = numpy.zeros((N_v, N_u), float)  
    for cell in mesh.cells:
        # Build element matrix for this cell
        element_matrix[:,:] = 0
        for iu in range(N_u):
            for iv in range(N_v):
                # Perform quadrature for the given test and trial functions 
                element_matrix[iv,iu] = integrate_cell(cell, args, Q, iv, iu)
        
        # Insert element matrix into global matrix 
        cell_dofs_u = V_u.dofmap().cell_dofs(cell.id)
        cell_dofs_v = V_v.dofmap().cell_dofs(cell.id)
        A.insert_matrix(cell_dofs_v, cell_dofs_u, element_matrix)
    timer.stop()


def assemble_matrix_ds(form, args, ds, A):
    """
    Assemble the given sub-form into the matrix A
    Integrate over external facets
    """
    timer = Timer('Assemble matrix ds')
    u = args.trial_function
    v = args.test_function
    V_u = u.function_space()
    V_v = v.function_space()
    
    element_u = V_u.element
    element_v = V_v.element
    N_u = element_u.num_basis_functions
    N_v = element_v.num_basis_functions
    
    mesh = form.domain
    Q = quadrature_degree(form, args, ds, 1)
    
    subdomain = ds.parameters.get('subdomain_data', None)
    if subdomain is not None:
        domain = subdomain.array()
        selected_domain = ds.parameters['selected_subdomain']
    
    # Build global matrix by summing cell contributions
    element_matrix = numpy.zeros((N_v, N_u), float)  
    for facet in mesh.facets(external=True, internal=False):
        if subdomain is not None and domain[facet.id] != selected_domain:
            continue
        
        cell = mesh.cell(facet.cell_ids[0])
        
        # Allow evals to look up the facet that is being integrated over
        cell.local_facet = facet
        
        # Build element matrix for this facet
        element_matrix[:,:] = 0
        for iu in range(N_u):
            for iv in range(N_v):
                # Perform quadrature for the given test and trial functions  
                element_matrix[iv,iu] = integrate_facet(cell, args, Q, iv, iu)
        
        # Remove local facet information from cell
        cell.local_facet = None
        
        # Insert element matrix into global matrix
        cell_dofs_u = V_u.dofmap().cell_dofs(cell.id)
        cell_dofs_v = V_v.dofmap().cell_dofs(cell.id)
        A.insert_matrix(cell_dofs_v, cell_dofs_u, element_matrix)
    timer.stop()


def assemble_matrix_dS(form, args, dS, A):
    """
    Assemble the given sub-form into the matrix A
    Integrate over internal facets
    """
    timer = Timer('Assemble matrix dS')
    u = args.trial_function
    v = args.test_function
    V_u = u.function_space()
    V_v = v.function_space()
    
    element_u = V_u.element
    element_v = V_v.element
    N_u = element_u.num_basis_functions
    N_v = element_v.num_basis_functions
    restriction_u = get_trial_function_restriction(args)
    restriction_v = get_test_function_restriction(args)
    
    mesh = form.domain
    Q = quadrature_degree(form, args, dS, 1)
    
    subdomain = dS.parameters.get('subdomain_data', None)
    if subdomain is not None:
        domain = subdomain.array()
        selected_domain = dS.parameters['selected_subdomain']
    
    # Build global matrix by summing cell contributions
    element_matrix = numpy.zeros((N_v, N_u), float)  
    for facet in mesh.facets(external=False, internal=True):
        if subdomain is not None and domain[facet.id] != selected_domain:
            continue
        
        # Connected cells
        cell_plus = mesh.cell(facet.cell_ids[0])
        cell_minus = mesh.cell(facet.cell_ids[1])
        cell_u = cell_plus if restriction_u == '+' else cell_minus
        cell_v = cell_plus if restriction_v == '+' else cell_minus
        
        # Allow evals to look up the facet that is being integrated over
        cell_plus.local_facet = facet
        cell_minus.local_facet = facet
        # Allow evals in RestrictedArg to get the cell for '-' restrictions
        cell_plus.local_cell_neighbour = cell_minus
        
        # Build element matrix for this facet
        element_matrix[:,:] = 0
        for iu in range(N_u):
            for iv in range(N_v):
                # Perform quadrature for the given test and trial functions  
                element_matrix[iv,iu] = integrate_facet(cell_plus, args, Q, iv, iu, calc_neighbour_x=True)
        
        # Remove local facet information from cells
        cell_plus.local_facet = None
        cell_minus.local_facet = None
        cell_plus.local_cell_neighbour = None
        
        # Insert element matrix into global matrix
        cell_dofs_u = V_u.dofmap().cell_dofs(cell_u.id)
        cell_dofs_v = V_v.dofmap().cell_dofs(cell_v.id)
        A.insert_matrix(cell_dofs_v, cell_dofs_u, element_matrix)
    timer.stop()


def assemble_vector_dx(form, args, dx, b):
    """
    Assemble the given sub-form into the vector b
    Integrate over cells
    """
    timer = Timer('Assemble vector dx')
    v = args.test_function
    
    V = v.function_space()
    element = V.element
    N = element.num_basis_functions
    mesh = form.domain
    Q = quadrature_degree(form, args, dx)
    
    # Build global vector by summing cell contributions  
    for cell in mesh.cells:
        cell_dofs = V.dofmap().cell_dofs(cell.id)
        for iv in range(N):
            # Perform quadrature for the given test function 
            b[cell_dofs[iv]] += integrate_cell(cell, args, Q, iv)
    timer.stop()


def assemble_vector_ds(form, args, ds, b):
    """
    Assemble the given sub-form into the vector b
    Integrate over external facets
    """
    timer = Timer('Assemble vector ds')
    v = args.test_function
    
    V = v.function_space()
    element = V.element
    N = element.num_basis_functions
    mesh = form.domain
    Q = quadrature_degree(form, args, ds, 1)
    
    subdomain = ds.parameters.get('subdomain_data', None)
    ignore_restrictions = ds.parameters.get('ignore_restrictions', False) 
    if subdomain is not None:
        domain = subdomain.array()
        selected_domain = ds.parameters['selected_subdomain']
    
    # Build global vector by summing facet contributions
    for facet in mesh.facets(external=True, internal=False):
        if subdomain is not None and domain[facet.id] != selected_domain:
            continue
        
        cell = mesh.cell(facet.cell_ids[0])
        cell.ignore_restrictions = ignore_restrictions
        
        # Allow evals to look up the facet that is being integrated over
        cell.local_facet = facet
        
        cell_dofs = V.dofmap().cell_dofs(cell.id)
        for iv in range(N):
            # Perform quadrature for the given test function 
            b[cell_dofs[iv]] += integrate_facet(cell, args, Q, iv)
        
        # Remove local facet information from cell
        cell.local_facet = None
        cell.ignore_restrictions = False
    timer.stop()


def assemble_vector_dS(form, args, dS, b):
    """
    Assemble the given sub-form into the vector b
    Integrate over internal facets
    """
    timer = Timer('Assemble vector dS')
    v = args.test_function
    V = v.function_space()
    
    element = V.element
    N = element.num_basis_functions
    restriction = get_test_function_restriction(args)
    
    mesh = form.domain
    Q = quadrature_degree(form, args, dS, 1)
    
    subdomain = dS.parameters.get('subdomain_data', None)
    if subdomain is not None:
        domain = subdomain.array()
        selected_domain = dS.parameters['selected_subdomain']
    
    # Build global vector by summing facet contributions  
    for facet in mesh.facets(external=False, internal=True):
        if subdomain is not None and domain[facet.id] != selected_domain:
            continue
        
        # Connected cells
        cell_plus = mesh.cell(facet.cell_ids[0])
        cell_minus = mesh.cell(facet.cell_ids[1])
        cell = cell_plus if restriction == '+' else cell_minus
        
        # Allow evals to look up the facet that is being integrated over
        cell_plus.local_facet = facet
        cell_minus.local_facet = facet
        # Allow evals in RestrictedArg to get the cell for '-' restrictions
        cell_plus.local_cell_neighbour = cell_minus
        
        cell_dofs = V.dofmap().cell_dofs(cell.id)
        for iv in range(N):
            # Perform quadrature for the given test function  
            b[cell_dofs[iv]] += integrate_facet(cell_plus, args, Q, iv, calc_neighbour_x=True)
        
        # Remove local facet information from cells
        cell_plus.local_facet = None
        cell_minus.local_facet = None
        cell_plus.local_cell_neighbour = None
    
    timer.stop()


def assemble_scalar_dx(form, args, dx):
    """
    Assemble the given sub-form as a scalar
    Integrate over cells
    """
    timer = Timer('Assemble scalar dx')
    mesh = form.domain
    Q = quadrature_degree(form, args, dx)
    
    # Sum the cell contributions
    s = 0
    for cell in mesh.cells:
        s += integrate_cell(cell, args, Q)
    
    timer.stop()
    return s


def assemble_scalar_ds(form, args, ds):
    """
    Assemble the given sub-form as a scalar
    Integrate over external facets
    """
    timer = Timer('Assemble scalar ds')
    mesh = form.domain
    Q = quadrature_degree(form, args, ds, 1)
    
    subdomain = ds.parameters.get('subdomain_data', None)
    ignore_restrictions = ds.parameters.get('ignore_restrictions', False) 
    if subdomain is not None:
        domain = subdomain.array()
        selected_domain = ds.parameters['selected_subdomain']
    
    # Sum facet contributions
    s = 0
    for facet in mesh.facets(external=True, internal=False):
        if subdomain is not None and domain[facet.id] != selected_domain:
            continue
        
        cell = mesh.cell(facet.cell_ids[0])
        cell.ignore_restrictions = ignore_restrictions
        
        # Allow evals to look up the facet that is being integrated over
        cell.local_facet = facet
        
        s += integrate_facet(cell, args, Q)
        
        # Remove local facet information from cell
        cell.local_facet = None
        cell.ignore_restrictions = False
    
    timer.stop()
    return s
