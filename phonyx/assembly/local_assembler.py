import numpy
from ..linalg import Matrix, Vector
from ..form_language import Zero, get_topmost_function_space
from ..form_language import get_trial_function_restriction, get_test_function_restriction
from .quadrature import quadrature_degree, integrate_cell, integrate_facet
from ..timing import Timer


def assemble_local(form, cell, return_dofs=False):
    """
    Assemble a weak form on a given cell. The form must either be a bilinear form,
    a linear form or a scalar form, mixing form shapes is not supported.
    
    The function returns a Matrix, a Vector depending on the input 
    """
    # Check the trial functions in the form to find out if this is a N x N matrix,
    # a length N vector or a scalar
    is_scalar = form.dim == 0
    trial_dofs = set()
    has_empty_form = False
    has_linear_form = False
    for args, _measure in form.sub_forms:
        if isinstance(args, Zero):
            # Empty form
            has_empty_form = True
            
        elif not args.has_trial_function:
            # No trial function -> linear form
            has_linear_form = True
        
        else:
            # Has trial function -> bilinear form
            u = args.trial_function
            V = get_topmost_function_space(u)
            arg_dofs = tuple(V.dofmap().cell_dofs(cell.id))
            trial_dofs.add(arg_dofs)
    
    if is_scalar:
        assert len(trial_dofs) == 0, 'Found inconsistent shapes in assemble_local'
        s = 0
        for args, measure in form.sub_forms:
            # Check for empty form
            if isinstance(args, Zero):
                continue
            
            raise NotImplementedError('Cannot assemble local scalar %r measure' % measure.type)
        
        if return_dofs:
            return s, (None, None)
        else:
            return s
    
    # Get the degrees of freedom in the test function space 
    test_dofs = set()
    for args, _measure in form.sub_forms:
        if not isinstance(args, Zero):
            v = args.test_function
            V = get_topmost_function_space(v)
            arg_dofs = tuple(V.dofmap().cell_dofs(cell.id))
            test_dofs.add(arg_dofs)
    
    assert len(test_dofs) == 1, 'Found multiple test spaces in form without a shared mixed space'
    test_dofs = test_dofs.pop()
    N = len(test_dofs) 
    
    if has_linear_form:
        assert len(trial_dofs) == 0, 'Found inconsistent shapes in assemble_local'
        
        b = numpy.zeros(N, float)
        for args, measure in form.sub_forms:
            # Check for empty form
            if isinstance(args, Zero):
                continue
            
            if measure.type == 'dx':
                assemble_local_vector_dx(form, args, measure, b, cell, test_dofs)
            elif measure.type == 'ds':
                assemble_local_vector_ds(form, args, measure, b, cell, test_dofs)
            elif measure.type == 'dS':
                assemble_local_vector_dS(form, args, measure, b, cell, test_dofs)
            else:
                raise NotImplementedError('Cannot assemble local linear %r measure' % measure.type)
            
        if return_dofs:
            return b, (test_dofs, None)
        else:
            return b
    
    elif len(trial_dofs) == 1:
        trial_dofs = trial_dofs.pop()
        M = len(trial_dofs)
        
        A = numpy.zeros((N, M), float)
        for args, measure in form.sub_forms:
            # Check for empty form
            if isinstance(args, Zero):
                continue
            
            if measure.type == 'dx':
                assemble_local_matrix_dx(form, args, measure, A, cell, test_dofs, trial_dofs)
            elif measure.type == 'ds':
                assemble_local_matrix_ds(form, args, measure, A, cell, test_dofs, trial_dofs)
            elif measure.type == 'dS':
                assemble_local_matrix_dS(form, args, measure, A, cell, test_dofs, trial_dofs)
            else:
                raise NotImplementedError('Cannot assemble local bilinear %r measure' % measure.type)
        
        if return_dofs:
            return A, (test_dofs, trial_dofs)
        else:
            return A
    
    else:
        assert len(trial_dofs) == 0, 'Found multiple trial spaces in form without a shared mixed space'
        if has_empty_form:
            raise ValueError('Cannot assemble local matrix with only empty form')


def assemble_local_matrix_dx(form, args, dx, A, cell, test_dofs, trial_dofs):
    """
    Assemble the given sub-form into the matrix A
    Integrate over the given cell
    """
    timer = Timer('Assemble local matrix dx')
    u = args.trial_function
    v = args.test_function
    V_u = u.function_space()
    V_v = v.function_space()
    
    element_u = V_u.element
    element_v = V_v.element
    N_u = element_u.num_basis_functions
    N_v = element_v.num_basis_functions
    
    Q = quadrature_degree(form, args, dx)
    cell_dofs_u = V_u.dofmap().cell_dofs(cell.id)
    cell_dofs_v = V_v.dofmap().cell_dofs(cell.id)
    
    for iu in range(N_u):
        idx_u = trial_dofs.index(cell_dofs_u[iu])
        for iv in range(N_v):
            idx_v = test_dofs.index(cell_dofs_v[iv])
            # Perform quadrature for the given test and trial functions
            A[idx_v, idx_u] += integrate_cell(cell, args, Q, iv, iu)
    timer.stop()


def assemble_local_matrix_ds(form, args, ds, A, cell, test_dofs, trial_dofs):
    """
    Assemble the given sub-form into the matrix A
    Integrate over external facets
    """
    timer = Timer('Assemble local matrix ds')
    u = args.trial_function
    v = args.test_function
    V_u = u.function_space()
    V_v = v.function_space()
    
    element_u = V_u.element
    element_v = V_v.element
    N_u = element_u.num_basis_functions
    N_v = element_v.num_basis_functions
    
    Q = quadrature_degree(form, args, ds, 1)
    cell_dofs_u = V_u.dofmap().cell_dofs(cell.id)
    cell_dofs_v = V_v.dofmap().cell_dofs(cell.id)
    
    subdomain = ds.parameters.get('subdomain_data', None)
    if subdomain is not None:
        domain = subdomain.array()
        selected_domain = ds.parameters['selected_subdomain']
    
    # Build global matrix by summing cell contributions
    for facet in cell.facets(external=True, internal=False):
        if subdomain is not None and domain[facet.id] != selected_domain:
            continue
        
        # Allow evals to look up the facet that is being integrated over
        cell.local_facet = facet
        
        # Build element matrix for this facet
        for iu in range(N_u):
            idx_u = trial_dofs.index(cell_dofs_u[iu])
            for iv in range(N_v):
                idx_v = test_dofs.index(cell_dofs_v[iv])
                # Perform quadrature for the given test and trial functions  
                A[idx_v, idx_u] += integrate_facet(cell, args, Q, iv, iu)
        
        # Remove local facet information from cell
        cell.local_facet = None
    
    timer.stop()


def assemble_local_matrix_dS(form, args, dS, A, cell, test_dofs, trial_dofs):
    """
    Assemble the given sub-form into the matrix A
    Integrate over internal facets
    """
    timer = Timer('Assemble local matrix dS')
    u = args.trial_function
    v = args.test_function
    V_u = u.function_space()
    V_v = v.function_space()
    
    element_u = V_u.element
    element_v = V_v.element
    N_u = element_u.num_basis_functions
    N_v = element_v.num_basis_functions
    restriction_u = get_trial_function_restriction(args)
    restriction_v = get_test_function_restriction(args)
    
    mesh = form.domain
    Q = quadrature_degree(form, args, dS, 1)
    cell_dofs_u = V_u.dofmap().cell_dofs(cell.id)
    cell_dofs_v = V_v.dofmap().cell_dofs(cell.id)
    
    subdomain = dS.parameters.get('subdomain_data', None)
    if subdomain is not None:
        domain = subdomain.array()
        selected_domain = dS.parameters['selected_subdomain']
    
    # Build global matrix by summing cell contributions  
    for facet in cell.facets(external=False, internal=True):
        if subdomain is not None and domain[facet.id] != selected_domain:
            continue
        
        # Connected cells
        cell_plus = mesh.cell(facet.cell_ids[0])
        cell_minus = mesh.cell(facet.cell_ids[1])
        
        cell_side = '+' if cell.id == facet.cell_ids[0] else '-'
        if restriction_v != cell_side or restriction_u != cell_side:
            # The test or trial function belongs to the neighbour cell -> skip
            continue
        
        # Allow evals to look up the facet that is being integrated over
        cell_plus.local_facet = facet
        cell_minus.local_facet = facet
        # Allow evals in RestrictedArg to get the cell for '-' restrictions
        cell_plus.local_cell_neighbour = cell_minus
        
        # Build element matrix for this facet
        for iu in range(N_u):
            idx_u = trial_dofs.index(cell_dofs_u[iu])
            for iv in range(N_v):
                idx_v = test_dofs.index(cell_dofs_v[iv])
                # Perform quadrature for the given test and trial functions  
                A[idx_v, idx_u] += integrate_facet(cell_plus, args, Q, iv, iu, calc_neighbour_x=True)
        
        # Remove local facet information from cells
        cell_plus.local_facet = None
        cell_minus.local_facet = None
        cell_plus.local_cell_neighbour = None
    timer.stop()


def assemble_local_vector_dx(form, args, dx, b, cell, test_dofs):
    """
    Assemble the given sub-form into the vector b
    Integrate over the given cell
    """
    timer = Timer('Assemble local vector dx')
    v = args.test_function
    V = v.function_space()
    element = V.element
    N = element.num_basis_functions
    Q = quadrature_degree(form, args, dx)
    
    cell_dofs = V.dofmap().cell_dofs(cell.id)
    for iv in range(N):
        # Perform quadrature for the given test function
        idx = test_dofs.index(cell_dofs[iv])
        b[idx] += integrate_cell(cell, args, Q, iv)
    timer.stop()


def assemble_local_vector_ds(form, args, ds, b, cell, test_dofs):
    """
    Assemble the given sub-form into the vector b
    Integrate over external facets
    """
    timer = Timer('Assemble local vector ds') 
    v = args.test_function
    
    V = v.function_space()
    element = V.element
    N = element.num_basis_functions
    Q = quadrature_degree(form, args, ds, 1)
    
    subdomain = ds.parameters.get('subdomain_data', None)
    if subdomain is not None:
        domain = subdomain.array()
        selected_domain = ds.parameters['selected_subdomain']
    
    for facet in cell.facets(external=True, internal=False):
        if subdomain is not None and domain[facet.id] != selected_domain:
            continue
        
        # Allow evals to look up the facet that is being integrated over
        cell.local_facet = facet
        
        cell_dofs = V.dofmap().cell_dofs(cell.id)
        for iv in range(N):
            # Perform quadrature for the given test function
            idx = test_dofs.index(cell_dofs[iv])
            b[idx] += integrate_facet(cell, args, Q, iv)
        
        # Remove local facet information from cell
        cell.local_facet = None
    timer.stop()


def assemble_local_vector_dS(form, args, dS, b, cell, test_dofs):
    """
    Assemble the given sub-form into the vector b
    Integrate over internal facets
    """
    timer = Timer('Assemble local vector dS')
    v = args.test_function
    V = v.function_space()
    
    element = V.element
    N = element.num_basis_functions
    restriction = get_test_function_restriction(args)
    mesh = form.domain
    Q = quadrature_degree(form, args, dS, 1)
    
    subdomain = dS.parameters.get('subdomain_data', None)
    if subdomain is not None:
        domain = subdomain.array()
        selected_domain = dS.parameters['selected_subdomain']
    
    for facet in cell.facets(external=False, internal=True):
        if subdomain is not None and domain[facet.id] != selected_domain:
            continue
        
        cell_side = '+' if cell.id == facet.cell_ids[0] else '-'
        
        if restriction != cell_side:
            # This test function belongs to the neighbour cell -> skip
            continue
        
        # Connected cells
        cell_plus = mesh.cell(facet.cell_ids[0])
        cell_minus = mesh.cell(facet.cell_ids[1])
        
        # Allow evals to look up the facet that is being integrated over
        cell_plus.local_facet = facet
        cell_minus.local_facet = facet
        # Allow evals in RestrictedArg to get the cell for '-' restrictions
        cell_plus.local_cell_neighbour = cell_minus
        
        cell_dofs = V.dofmap().cell_dofs(cell.id)
        for iv in range(N):
            # Perform quadrature for the given test function
            idx = test_dofs.index(cell_dofs[iv])  
            b[idx] += integrate_facet(cell_plus, args, Q, iv, calc_neighbour_x=True)
        
        # Remove local facet information from cells
        cell_plus.local_facet = None
        cell_minus.local_facet = None
        cell_plus.local_cell_neighbour = None
    
    timer.stop()
