from .mesh import coordinate_on_boundary
from .function_spaces import FunctionSpace, VectorFunctionSpace, leaf_function_spaces
from .timing import Timer

class DirichletBC(object):
    def __init__(self, V, g, sub_domain):
        """
        Constrain degrees of freedom in the function space V to have the value
        given by the argument g (a Constant or an Expression)
        
        The sub_domain argument must be a callable that takes two arguments,
        the vector "x" with the physical coordinate of a dof and the boolean
        "on_boundary" that is true for vertices on the external boundary.
        """
        self.function_space = V
        self.value = g
        self.sub_domain = sub_domain
        self.constrained_dofs = None
    
    def identify_affected_dofs(self):
        self.constrained_dofs = []
        for V in leaf_function_spaces(self.function_space):
            element = V.element
            mesh = V.mesh()
            
            # Loop over cells and locate dofs that have DirichletBCs
            constrained = []
            for cell in mesh.cells:
                dof_verts = [element.get_coordinate_for_basis_function(i, cell)
                             for i in range(element.num_basis_functions)]
                
                for vert in dof_verts:
                    on_boundary = coordinate_on_boundary(mesh, cell, vert)
                    include = self.sub_domain(vert, on_boundary)
                    if include:
                        dofs = V.dofmap().coordinate_dofmap[tuple(vert)]
                        for dof in dofs:
                            constrained.append((dof, cell.id, vert))
            self.constrained_dofs.append(constrained)
    
    def apply(self, A, b, keep_symmetry=False):
        """
        Modify the Matrix A and the Vector b to account for the boundary
        conditions at the constrained nodes
        """
        timer = Timer('DirichletBC apply')
        
        if self.constrained_dofs is None:
            self.identify_affected_dofs()
            
        def _apply(constrained, index=None):
            for dof, cell_id, pos in constrained:
                cell = mesh.cell(cell_id)
                x = cell.local_coordinates(pos)
                if index is None:
                    value = self.value.eval_cell(cell, x)
                else:
                    value = self.value[index].eval_cell(cell, x)
                
                if keep_symmetry:
                    col = A.set_col_to_zero(dof)
                    b[:] -= col*value
                
                A.set_row_to_zero(dof)
                A[dof,dof] = 1
                b[dof] = value
        
        if isinstance(self.function_space, FunctionSpace):
            mesh = self.function_space.mesh()
            _apply(self.constrained_dofs[0])
            
        else:
            assert isinstance(self.function_space, VectorFunctionSpace), 'BCs for mixed spaces not supported'
            mesh = self.function_space.sub_spaces[0].mesh()
            for i, constrained in enumerate(self.constrained_dofs):
                _apply(constrained, i)
        
        timer.stop()
