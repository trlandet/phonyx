###############################################################################
# Global configuration ala FEniCS
parameters = {
    # True or False
    'reorder_dofs_serial': False,
    
    # numpy, scipy or auto (which is numpy for small matrices)
    'linear_algebra_backend': 'auto' 
}


###############################################################################
# User facing API
from .timing import Timer, list_timings, TimingClear_clear, TimingClear_keep
from .timing import TimingType_wall, TimingType_user, TimingType_system
from .mesh import RectangleMesh, UnitSquareMesh, UnitTriangleMesh
from .mesh import IntervalMesh, UnitIntervalMesh, cells, facets, Cell, Facet
from .mesh import SubDomain, MeshFunction, FacetFunction
from .linalg import Matrix, Vector, LinearSolver, solve
from . import elements
from .function_spaces import FunctionSpace, MixedFunctionSpace, VectorFunctionSpace
from .form_language import TrialFunction, TestFunction, Function, Constant, Expression, Zero
from .form_language import TrialFunctions, TestFunctions
from .form_language import Circumradius, FacetNormal
from .form_language import as_vector, dot, grad, div, inner, outer, nabla_div, nabla_grad
from .form_language import jump, avg
from .form_language import system, Form, Measure, dx, ds, dS, FormError
from .boundary_conditions import DirichletBC
from .assembly import assemble, assemble_system, assemble_local
from .local_solver import LocalSolver
from .public_functions import project, interpolate, errornorm, norm
from .public_functions import near, PHONYX_EPS, DOLFIN_EPS
from .output import File
from .plotting import plot, interactive
