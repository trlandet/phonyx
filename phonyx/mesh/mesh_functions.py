import numpy
from ..timing import Timer

class MeshFunction(object):
    def __init__(self, data_type, mesh, dim):
        assert data_type in ('int', 'size_t', 'double', 'bool')
        assert dim == mesh.topology().dim() - 1
        self.data_type = data_type
        self.mesh = mesh
        self.topological_dim = dim
        self.topological_dim_type = 'facet'
        
        N = mesh.num_facets()
        dtype = {'int': int, 'size_t': int, 'double': float, 'bool': bool}[data_type]
        self._data = numpy.zeros(N, dtype=dtype)
    
    def array(self):
        return self._data
    
    def set_all(self, value):
        """
        Set all elements to the given value
        """
        self._data[:] = value


class FacetFunction(MeshFunction):
    def __init__(self, data_type, mesh):
        MeshFunction.__init__(self, data_type, mesh, mesh.topology().dim()-1)


class SubDomain(object):
    def mark(self, mesh_function, sub_domain_number):
        """
        Set subdomain markers (int) on mesh entities (facets or cells)
        """
        if mesh_function.topological_dim_type == 'cell':
            self.mark_cells(mesh_function, sub_domain_number)
        elif mesh_function.topological_dim_type == 'facet':
            self.mark_facets(mesh_function, sub_domain_number)
        else:
            raise NotImplementedError('Cannot mark mesh function of type %r' % mesh_function.dim_type)
    
    def mark_facets(self, mesh_function, sub_domain_marker):
        """
        Set subdomain markers (int) on facets
        """
        assert mesh_function.topological_dim_type == 'facet'
        timer = Timer('Marking facets')
        
        mesh = mesh_function.mesh
        topo = mesh.topology()
        data = mesh_function.array()
        Nvertex = len(mesh.vertices)
        Nfacet = len(data)
        
        # Find vertices on boundary facets
        on_boundary_facet = numpy.zeros(Nvertex, bool)
        on_non_boundary_facet = numpy.zeros(Nvertex, bool)
        for facet_id in range(Nfacet):
            obf = len(topo.facet_to_cell[facet_id]) == 1
            for vertex_id in topo.facet_to_vertex[facet_id]:
                if obf:
                    on_boundary_facet[vertex_id] = True
                else:
                    on_non_boundary_facet[vertex_id] = True
        
        marks = numpy.zeros(Nvertex, bool)
        marks2 = numpy.zeros(Nvertex, bool)
        for vertex_id, vertex in enumerate(mesh.vertices):
            if on_boundary_facet[vertex_id]:
                marks[vertex_id] = self.inside(vertex, True)
            if on_non_boundary_facet[vertex_id]:
                marks2[vertex_id] = self.inside(vertex, False)
        
        Nfacet = len(data)
        for facet_id in range(Nfacet):
            vertex_ids = topo.facet_to_vertex[facet_id]
            all1 = all(marks[vid] for vid in vertex_ids)
            any1 = any(marks[vid] for vid in vertex_ids)
            all2 = all(marks2[vid] for vid in vertex_ids)
            any2 = any(marks2[vid] for vid in vertex_ids)
            
            if not (any1 or any2):
                continue
            
            boundary_facet = len(topo.facet_to_cell[facet_id]) == 1
            if boundary_facet and all1:
                data[facet_id] = sub_domain_marker
            elif not boundary_facet and all2:
                data[facet_id] = sub_domain_marker
        
        timer.stop()

def coordinate_on_boundary(mesh, cell, coordinate, eps=1e-5):
    """
    Return true if the given coordinates is on a facet of a given cell and if
    this facet is a part of the domain boundary, otherwise return False
    """
    eps2 = eps*cell.diameter()
    
    # Get all facets which are connected to the cell via at least one vertex
    # and also form a part of the domain boundary
    boundary_facets = []
    seen = []
    topo = mesh.topology()
    for vertex_id in topo.cell_to_vertex[cell.id]:
        for facet_id in topo.vertex_to_facet[vertex_id]:
            if facet_id in seen:
                continue
            seen.append(facet_id)
            facet = mesh.facet(facet_id)
            if facet.on_boundary:
                boundary_facets.append(facet.vertices)
    
    if not boundary_facets:
        # None of the adjacent facets are parts of the domain boundary
        return False
    
    c = coordinate
    if mesh.dim == 1:
        # Check if the coordinate is close to a facet (a point)
        for verts in boundary_facets:
            if abs(c[0] - verts[0][0]) < eps2:
                return True
    
    elif mesh.dim == 2:
        # Check if the coordinate is close to a facet (a line segment)
        for verts in boundary_facets:
            v1 = (verts[1][0] - verts[0][0], verts[1][1] - verts[0][1])
            v2 = (c[0]        - verts[0][0], c[1]        - verts[0][1])
            
            # Check that the points are on the same line
            cross = v1[0]*v2[1] - v1[1]*v2[0]
            if abs(cross) > eps2:
                continue
            
            # Check that the point is in the same line segment
            dot = v1[0]*v2[0] + v1[1]*v2[1]
            if -eps2 < dot < v1[0]**2 + v1[1]**2 + eps2:
                return True 
    
    return False
