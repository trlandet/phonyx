import numpy
from .entities import AbstractMesh
from ..timing import Timer


class IntervalMesh(AbstractMesh):
    def __init__(self, N, start, end):
        AbstractMesh.__init__(self)
        timer = Timer('Building mesh')
        
        self.dim = 1
        self.num_cells = N
        self.vertices = numpy.zeros((N+1, 1), float)
        self.vertices[:,0] = numpy.linspace(start, end, N+1)
        
        # Vertex to vertex&cell connectivity
        topo = self.topology()
        for i in range(N+1):
            if i == 0:
                topo.vertex_to_vertex.append((i+1,))
                topo.vertex_to_cell.append((i,))
            elif i == N:
                topo.vertex_to_vertex.append((i-1,))
                topo.vertex_to_cell.append((i-1,))
            else:
                topo.vertex_to_vertex.append((i-1, i+1))
                topo.vertex_to_cell.append((i-1, i))

        # Cell to cell connectivity
        for i in range(N):
            if i == 0:
                topo.cell_to_cell.append((i+1,))
            elif i == N-1:
                topo.cell_to_cell.append((i-1,))
            else:
                topo.cell_to_cell.append((i-1, i+1))
            topo.cell_to_vertex.append((i, i+1))
        
        # Vertices and facets are the same in 1D
        topo.cell_to_facet = topo.cell_to_vertex    
        topo.facet_to_cell = topo.vertex_to_cell
        topo.facet_to_vertex = [(i,) for i in range(N+1)]
        topo.vertex_to_facet = [(i,) for i in range(N+1)]
        
        timer.stop()


class UnitIntervalMesh(IntervalMesh):
    def __init__(self, N):
        super(UnitIntervalMesh, self).__init__(N, 0.0, 1.0)
