import numpy
from .entities import AbstractMesh
from ..timing import Timer

class RectangleMesh(AbstractMesh):
    def __init__(self, startx, starty, endx, endy, Nx, Ny, diagonal='right'):
        AbstractMesh.__init__(self)
        timer = Timer('Building mesh')
        
        self.dim = 2
        self.p0 = (startx, starty)
        self.p1 = (endx, endy)
        self.Nx = Nx
        self.Ny = Ny
        assert diagonal == 'right'
        
        # Number of vertices
        N = (Nx + 1) * (Ny + 1)
        # Number of facets
        Nf = 3*Nx*Ny + Nx + Ny 
        
        # Build vertices
        self.vertices = numpy.zeros((N, 2), float)
        x = numpy.linspace(self.p0[0], self.p1[0], Nx+1, float)
        y = numpy.linspace(self.p0[1], self.p1[1], Ny+1, float)
        i = 0
        for yi in y:
            for xi in x:
                self.vertices[i] = (xi, yi)
                i += 1
        
        # Helper to number facets
        class FacetNumberer(object):
            next = 0
            facets_by_node_numbers = {}
            
            @classmethod
            def get_facet(cls, n0, n1):
                # The lowest node number should come first
                if n1 < n0:
                    n0, n1 = n1, n0
                
                key = (n0, n1)
                if key in cls.facets_by_node_numbers:
                    # We have seen this facet before
                    facet_number = cls.facets_by_node_numbers[key]
                else:
                    # We must create a new facet
                    facet_number = cls.next
                    cls.facets_by_node_numbers[key] = facet_number
                    cls.next += 1
                    
                    # First, update topology information
                    topo.facet_to_cell.append([])
                    topo.facet_to_vertex.append((n0, n1))
                    topo.vertex_to_facet[n0].append(facet_number)
                    topo.vertex_to_facet[n1].append(facet_number)
                
                return facet_number
        
        # Build MeshTopology
        topo = self.topology()
        topo.vertex_to_cell = [[] for _ in range(N)]#solve(A, U.vector(), b)
        topo.vertex_to_facet = [[] for _ in range(N)]
        self.num_cells = 2*Nx*Ny
        for ic in range(self.num_cells):
            ix = ic % (2*Nx)
            iy = ic // (2*Nx)
            b = ix //2
            
            if ix % 2 == 0:
                # Below the diagonal
                n0 = b + iy*(Nx+1)
                n1 = b + 1 + iy*(Nx+1)
                n2 = b + 1 + (iy + 1)*(Nx+1)
                
                f0 = FacetNumberer.get_facet(n1, n2)
                f1 = FacetNumberer.get_facet(n2, n0)
                f2 = FacetNumberer.get_facet(n0, n1)
            else:
                # Above the diagonal
                n0 = b + iy*(Nx+1)
                n1 = b + (iy + 1)*(Nx+1)
                n2 = b + 1 + (iy + 1)*(Nx+1)
                
                f0 = FacetNumberer.get_facet(n1, n2)
                f1 = FacetNumberer.get_facet(n2, n0)
                f2 = FacetNumberer.get_facet(n0, n1)
            
            topo.cell_to_facet.append((f0, f1, f2))
            topo.cell_to_vertex.append((n0, n1, n2))
            topo.facet_to_cell[f0].append(ic)
            topo.facet_to_cell[f1].append(ic)
            topo.facet_to_cell[f2].append(ic)
            topo.vertex_to_cell[n0].append(ic)
            topo.vertex_to_cell[n1].append(ic)
            topo.vertex_to_cell[n2].append(ic)
        
        timer.stop()


class UnitSquareMesh(RectangleMesh):
    def __init__(self, Nx, Ny, diagonal='right'):
        super(UnitSquareMesh, self).__init__(0.0, 0.0, 1.0, 1.0, Nx, Ny, diagonal)


class UnitTriangleMesh(AbstractMesh):
    def __init__(self):
        AbstractMesh.__init__(self)
        timer = Timer('Building mesh')
        
        self.dim = 2
        self.num_cells = 1
        
        # Build vertices
        self.vertices = numpy.array([(0, 0), (1, 0), (0, 1)], float)
        
        # Build MeshTopology
        topo = self.topology()
        topo.cell_to_facet = [(0, 1, 2)]
        topo.cell_to_vertex = [(0, 1, 2)]
        topo.facet_to_cell = [(0,), (0,), (0,)]
        topo.facet_to_vertex = [(1, 2), (0, 2), (1, 0)]
        topo.vertex_to_cell = [(0,), (0,), (0,)]
        topo.vertex_to_facet = [(1, 2), (0, 2), (1, 0)]
        
        timer.stop()
