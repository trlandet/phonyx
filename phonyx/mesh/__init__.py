from .entities import AbstractMesh, FacetIterator, Cell, Facet
from .r1 import IntervalMesh, UnitIntervalMesh
from .r2 import RectangleMesh, UnitSquareMesh, UnitTriangleMesh
from .mesh_functions import SubDomain, MeshFunction, FacetFunction, coordinate_on_boundary


def cells(mesh):
    """
    Return an iterator over all the cells in the Mesh
    """
    return mesh.cells


def facets(mesh_or_cell):
    """
    Return an iterator over all the facets in the Mesh or Cell depending on the
    input argument type
    """
    if isinstance(mesh_or_cell, AbstractMesh):
        return mesh_or_cell.facets()
    
    else:
        assert isinstance(mesh_or_cell, Cell)
        cell = mesh_or_cell
        mesh = cell.mesh
        topo = mesh.topology()
        return FacetIterator(mesh, topo.cell_to_facet[cell.id])
