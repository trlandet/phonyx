from __future__ import division
import numpy


class AbstractMesh(object):
    def __init__(self):
        self._topology = MeshTopology()
        self._geometry = MeshGeometry(self)
        self.cache = MeshEntityCache()
        
        # To be initialized by subclasses
        self.dim = None
        self.num_cells = None
        self.vertices = None
    
    @property
    def cells(self):
        """
        Return an iterator over the mesh cells
        """
        return CellIterator(self, range(self.num_cells))
    
    def cell(self, cell_id):
        """
        Return the cell with the given cell id
        """        
        return Cell(self, cell_id)
    
    def facet(self, facet_id):
        """
        Return the facet with the given id
        """
        return Facet(self, facet_id)
    
    # For compatibility with dolfin
    def coordinates(self):
        """
        Return the coordinates of all simplex corners in the mesh
        """
        return self.vertices
    
    def facets(self, external=True, internal=True):
        """
        Return an iterator over the mesh facets
        """
        facet_ids = []
        for facet_id, cell_ids in enumerate(self._topology.facet_to_cell):
            if external and len(cell_ids) == 1:
                facet_ids.append(facet_id)
            elif internal and len(cell_ids) == 2:
                facet_ids.append(facet_id)
        
        return FacetIterator(self, facet_ids)
    
    def geometry(self):
        """
        Return the MeshGeometry of this mesh
        """
        return self._geometry
    
    def topology(self):
        """
        Return the MeshTopology of this mesh
        """
        return self._topology
    
    def num_facets(self):
        """
        The number of facets in the mesh
        """
        return len(self._topology.facet_to_cell)
    
    def cell_at_coordinate(self, y):
        """
        Return a cell that contains the global coordinate y[:]
        """
        # Slow linear time alorithms ...
        if self.dim == 1:
            for cell in self.cells:
                x = cell.local_coordinates(y)
                if -1 <= x[0] <= 1:
                    return cell
        else:
            for cell in self.cells:
                x = cell.local_coordinates(y)
                if 0 <= x[0] <= 1 and 0 <= x[1] <= 1:
                    return cell
                
    def hmin(self):
        """
        Return the smallest cell diameter in the mesh
        """
        hmin = 1e100
        for cell in self.cells:
            hmin = min(hmin, cell.diameter())
        return hmin
    
    def hash(self):
        verts = tuple(tuple(coords) for coords in self.vertices)
        return hash(verts)


class MeshGeometry(object):
    """
    This class is just here for dolfin API compatibility
    (to be able to get the dimension of the coordinate system)
    """
    
    def __init__(self, mesh):
        self.mesh = mesh
    
    def dim(self):
        "The dimension of the coordinate system"
        return self.mesh.dim


class MeshTopology(object):
    def __init__(self):
        """
        Container for the mesh connectivity
        """
        self.cell_to_cell = []
        self.facet_to_facet = []
        self.vertex_to_vertex = []
        
        self.cell_to_facet = []
        self.cell_to_vertex = []
        
        self.facet_to_cell = []
        self.facet_to_vertex = []
        
        self.vertex_to_cell = []
        self.vertex_to_facet = []
        
    def dim(self):
        """
        The topological dimension of the mesh (1 or 2)
        """
        return len(self.facet_to_vertex[0])
    

class MeshEntityCache(object):
    def __init__(self):
        """
        Some derived property of mesh entities like normals
        are required in assembly. To avoid calculating these
        over and over in the quadrature loops the mesh cache
        stores them to save time
        """
        self.clear()
    
    def clear(self):
        """
        Clear the mesh entity cache
        
        Must be called after changes to the mesh geometry
        unless no derived properties have been accessed 
        """
        self.facet_normal = {}


class Point(list):
    def coordinates(self):
        return list(self)
    
    def x(self):
        return self[0]
    
    def y(self):
        return self[1]
    
    def z(self):
        return self[2]


class Cell(object):
    def __init__(self, mesh, cell_id):
        """
        Represents one cell in the mesh
        
        Holds no actual data, this class just knows how to look 
        up information in the mesh
        """
        self.mesh = mesh
        self.id = cell_id
        self.num_vertices = mesh.dim + 1
        
        # Use by assembler routines when integrating over facets
        self.local_facet = None # Used by ds and dS measures
        self.local_cell_neighbour = None # Only used by dS measure
        self.local_cell_neighbour_x = None # Only used by dS measure
        self.ignore_restrictions = False # Used by projections into DGT
    
    def diameter(self):
        """
        Return the cell diameter calculated as two times the circumradius
        http://mathworld.wolfram.com/Circumradius.html
        """
        verts = self.vertices
        if len(verts) == 2:
            assert verts[0][0] < verts[1][0]
            return verts[1][0] - verts[0][0]
        
        elif len(verts) == 3:
            a = self.facet_area(0)
            b = self.facet_area(1)
            c = self.facet_area(2)
            R = a*b*c/((a + b +c)*(b + c - a)*(c + a - b)*(a + b - c))**0.5
            return 2*R
        
        else:
            raise NotImplementedError('Diameter only implemented in 1D and 2D')
    
    def index(self):
        return self.id
    
    def entities(self, top_dim):
        if top_dim == 0:
            return self.mesh.topology().cell_to_vertex[self.id]
        elif top_dim == 1:
            return self.mesh.topology().cell_to_facet[self.id]
    
    def facets(self, external=True, internal=True):
        """
        Return an iterator over the cell facets
        """
        topo = self.mesh.topology()
        facet_ids = []
        for facet_id in topo.cell_to_facet[self.id]:
            cell_ids = topo.facet_to_cell[facet_id]
            if external and len(cell_ids) == 1:
                facet_ids.append(facet_id)
            elif internal and len(cell_ids) == 2:
                facet_ids.append(facet_id)
        
        return FacetIterator(self.mesh, facet_ids)
    
    def local_coordinates(self, y):
        """
        Given physical coordinates y[:] return local coordinates x[:] 
        """
        verts = self.vertices
        ndim = len(verts[0])
        x = [0]*ndim
        
        if ndim == 1:
            # 1D - map to reference line element (-1, 1)
            assert verts[0][0] <= y[0] <= verts[1][0]
            x[0] = (y[0] - verts[0])/(verts[1] - verts[0])*2 - 1
            
        elif ndim == 2:
            # 2D - map to Barycentric coordinates
            # https://en.wikipedia.org/wiki/Barycentric_coordinate_system
            x0, y0 = y
            x1, y1 = verts[0]
            x2, y2 = verts[1]
            x3, y3 = verts[2]
            
            D = (y2 - y3)*(x1 - x3) + (x3 - x2)*(y1 - y3) 
            x[0] = ((y2 - y3)*(x0 - x3) + (x3 - x2)*(y0 - y3))/D
            x[1] = ((y3 - y1)*(x0 - x3) + (x1 - x3)*(y0 - y3))/D
        
        else:
            raise NotImplementedError('Local coordinates only implemented in 1D and 2D')
        
        return x
    
    def physical_coordinates(self, x):
        """
        Given local coordinates x[:] return physical coordinates y[:]
        """
        verts = self.vertices
        ndim = len(verts[0])
        y = [0]*ndim
        
        if ndim == 1:
            # 1D - map from reference line element (-1, 1)
            assert verts[0][0] < verts[1][0]
            y[0] = verts[0][0] + (verts[1][0] - verts[0][0])*(1+x[0])/2
        
        elif ndim == 2:
            # 2D - map from Barycentric coordinates
            # https://en.wikipedia.org/wiki/Barycentric_coordinate_system
            l1, l2 = x
            l3 = 1 - l1 - l2
            y[0] = verts[0][0]*l1 + verts[1][0]*l2 + verts[2][0]*l3
            y[1] = verts[0][1]*l1 + verts[1][1]*l2 + verts[2][1]*l3
        
        else:
            raise NotImplementedError('Physical coordinates only implemented in 1D and 2D')
        
        return y
    
    def midpoint(self):
        """
        Return the midpoint of the cell
        """
        return Point(numpy.mean(self.vertices, 0))
    
    def normal(self, local_facet_number=None):
        """
        Give the outwards normal on the given facet number. If the facet
        is not specified then the facet in self.local_facet is used   
        """
        if local_facet_number is None:
            facet = self.local_facet
        else:
            facet_ids = self.mesh.topology().cell_to_facet[self.id] 
            facet = self.mesh.facet(facet_ids[local_facet_number])
        
        cache = self.mesh.cache
        if facet.id in cache.facet_normal:
            normal, cell_id = cache.facet_normal[facet.id]
            if cell_id == self.id:
                return Point(normal)
            else:
                return Point([-n for n in normal])
        
        facet_verts = facet.vertices
        if len(facet_verts) == 1:
            # Calculate normal for facet (point) in 1D mesh
            cell_verts = self.vertices
            assert cell_verts[0][0] < cell_verts[1][0]
            if facet_verts[0][0] == cell_verts[0][0]:
                normal = [-1]
            else:
                assert facet_verts[0][0] == cell_verts[1][0]
                normal = [1]
        
        elif len(facet_verts) == 2:
            # Calculate normal for facet (line) in 2D mesh
            # Tangential vector components
            t0 = facet_verts[0][0] - facet_verts[1][0]
            t1 = facet_verts[0][1] - facet_verts[1][1]
            # Normal vector components
            n0, n1 = t1, -t0
            # Vector from facet to cell midpoint
            mp = numpy.mean(self.vertices, 0)
            m0 = mp[0] - facet_verts[0][0]
            m1 = mp[1] - facet_verts[0][1]
            # Swap direction of normal if it is not outwards pointing
            if m0*n0 + m1*n1 > 0:
                n0, n1 = -n0, -n1
            # Scale to unit length
            length = (n0**2 + n1**2)**0.5
            normal = [n0/length, n1/length]
            
        cache.facet_normal[facet.id] = normal, self.id
        return Point(normal)
    
    def volume(self):
        vertices = self.vertices
        if len(vertices) == 2:
            # Length of a line segment
            return abs(vertices[0] - vertices[1])
        
        elif len(vertices) == 3:
            # Area of a triangle from coordinates
            # http://www.mathopenref.com/coordtrianglearea.html
            A = vertices[0][0]*(vertices[1][1] - vertices[2][1])
            A += vertices[1][0]*(vertices[2][1] - vertices[0][1])
            A += vertices[2][0]*(vertices[0][1] - vertices[1][1])
            return abs(A)/2
        
        else:
            raise NotImplementedError('Cell volume only implemented in 1D and 2D')
        
    def facet_area(self, facet_index):
        """
        Compute the area/length of given facet with respect to the cell
        """
        facet_id = self.mesh.topology().cell_to_facet[self.id][facet_index]
        return self.mesh.facet(facet_id)._area()
    
    @property
    def vertices(self):
        """
        Global coordinates of connected vertices
        """
        ids = self.mesh.topology().cell_to_vertex[self.id]
        return [self.mesh.vertices[id] for id in ids]
    
    def get_vertex_coordinates(self):
        """
        Return one dimensional array consistent with dolfin API
        """
        verts = self.vertices
        N = len(verts)
        M = len(verts[0])
        result = numpy.zeros(N*M)
        for i, v in enumerate(verts):
            result[i*M:(i+1)*M] = v
        return result
    
    def __repr__(self):
        return '<phonyx.mesh.Cell %d %r>' % (self.id, self.vertices,)


class Facet(object):
    def __init__(self, mesh, facet_id):
        """
        Represents one facet in the mesh
        
        Holds no actual data, this class just knows how to look 
        up information in the mesh
        """
        self.mesh = mesh
        self.id = facet_id
        self.num_vertices = mesh.dim
        
    def entities(self, top_dim):
        if top_dim == 0:
            return self.mesh.topology().facet_to_vertex[self.id]
        if top_dim == self.mesh.dim:
            return self.mesh.topology().facet_to_cell[self.id]
        
    def index(self):
        return self.id
    
    @property
    def cell_ids(self):
        return self.mesh.topology().facet_to_cell[self.id]
    
    @property
    def on_boundary(self):
        return len(self.mesh.topology().facet_to_cell[self.id]) == 1
    
    @property
    def vertices(self):
        """
        Global coordinates of connected vertices
        """
        vertex_ids = self.mesh.topology().facet_to_vertex[self.id]
        return [self.mesh.vertices[vid] for vid in vertex_ids]
    
    def _area(self):
        """
        Area or length of facet (zero for 1D)
        """
        if self.num_vertices == 1:
            return 0
        
        elif self.num_vertices == 2:
            verts = self.vertices
            a = verts[0][0]-verts[1][0]
            b = verts[0][1]-verts[1][1]
            return (a**2 + b**2)**0.5


class CellIterator(object):
    def __init__(self, mesh, ids):
        """
        Implements iteration over cells in the mesh
        
        IMPORTANT: the cell object you get back is the same each time!
        To avoid creating a new object for each iteration the object
        is reused and just the attributes are changed
        """
        self.the_cell = Cell(mesh, None)
        self._iter_index = -1
        self._ids = ids
    
    def __iter__(self):
        return self
    
    def next(self):
        """
        Go to the next cell
        """
        self._iter_index += 1
        if self._iter_index >= len(self._ids):
            raise StopIteration
        
        # Go to the next cell
        self.the_cell.id = self._ids[self._iter_index]
        return self.the_cell


class FacetIterator(object):
    def __init__(self, mesh, ids):
        """
        Implements iteration over facets in the mesh
        
        IMPORTANT: the facet object you get back is the same each time!
        To avoid creating a new object for each iteration the object
        is reused and just the attributes are changed
        """
        self.the_facet = Facet(mesh, None)
        self._iter_index = -1
        self._ids = ids
    
    def __iter__(self):
        return self
    
    def next(self):
        """
        Go to the next facet
        """
        self._iter_index += 1
        if self._iter_index >= len(self._ids):
            raise StopIteration
        
        # Go to the next cell
        self.the_facet.id = self._ids[self._iter_index]
        return self.the_facet

