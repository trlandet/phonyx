from phonyx import FunctionSpace, VectorFunctionSpace
from phonyx import TrialFunction, TestFunction, Function
from phonyx import inner, dx, ds, dS, avg, assemble, assemble_system, solve, Timer


def project(function, V, bcs=None):
    """
    Project a function or an expression into a function space V
    """
    u = TrialFunction(V)
    v = TestFunction(V)
    U = Function(V)
    
    # Setup variational problem "u = function"
    f = function
    if V.name == 'Discontinuous Lagrange Trace':
        # Both '+' and '-' are identical for trace space u and v so we can select
        # either of them. For the function f the sides are not necessarily equal,
        # so we require the user to restrict the function before calling project
        ds2 = ds(ignore_restrictions=True)
        a = inner(u, v)*ds + inner(u, v)('+')*dS
        L = inner(f, v)*ds2 + inner(f, v('+'))*dS
    
    else:
        # Cell integrals
        a = inner(u, v)*dx
        L = inner(f, v)*dx
    
    # Assemble and solve variational problem
    A, b = assemble_system(a, L, bcs)
    solve(A, U.vector(), b)
    
    return U


def interpolate(function, V):
    """
    Interpolate a function or an expression into a function space V 
    """
    U = Function(V)
    mesh = V.mesh()
    gdim = mesh.geometry().dim()
    
    if function.shape == ():
        assert isinstance(V, FunctionSpace), \
               'The shapes of the function and the function space do not match'
    else:
        assert isinstance(V, VectorFunctionSpace), \
               'The shapes of the function and the function space do not match'
    
    def _interpolate(f, V):
        "Interpolate a scalar function or expression"
        dofmap = V.dofmap()
        cell_ids_and_coords = dofmap.tabulate_all_coordinates_with_cell_ids()
        for i, (cell_id, coord) in enumerate(cell_ids_and_coords):
            dof = i + dofmap.start_at
            cell = mesh.cell(cell_id)
            x = cell.local_coordinates(coord)
            U.vector()[dof] = f.eval_cell(cell, x)
    
    timer = Timer('Interpolate')
    if function.shape != ():
        for d in range(function.shape[0]):
            _interpolate(function[d], V.sub(d))
    else:
        _interpolate(function, V)
    timer.stop()
    
    return U


def errornorm(u, uh, norm_type='l2', degree_rise=3):
    """
    Calculate the error norm of u (Function or Expression)
    and uh (Function)
    """
    mesh = uh.domain
    rank = len(uh.shape)
    degree = uh.polynomial_degree + degree_rise
    
    if rank == 0:
        V = FunctionSpace(mesh, 'DG', degree)
    elif rank == 1:
        V = VectorFunctionSpace(mesh, 'DG', degree)
    
    e = interpolate(u, V)
    e.vector()[:] -= interpolate(uh, V).vector()
    
    return norm(e, norm_type)


def norm(v, norm_type='L2', mesh=None):
    """
    Compute the norm of a function
    """
    assert norm_type.lower() == 'l2' and mesh is None
    n = assemble(v**2*dx)
    return n**0.5 


PHONYX_EPS = DOLFIN_EPS = 3e-16
def near(a, b):
    """
    Is a near b (to within PHONYX_EPS)
    """
    return abs(a-b) <= PHONYX_EPS
