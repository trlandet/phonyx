from __future__ import division
from .assembly import assemble, assemble_local
from .form_language import get_topmost_function_space
import numpy

class LocalSolver(object):
    def __init__(self, a_form, L_form):
        """
        This class allows solution of local problems on each cell which can
        be significantly faster than solving global problems. The solution
        will be exact if the specified problem is truely local, otherwise it
        is an approximation
        """
        self.a_form = a_form
        self.L_form = L_form
        
        # Get the topmost function space of the test function
        v = a_form.sub_forms[0][0].test_function
        self.V_test = get_topmost_function_space(v)
        
    def solve_local_rhs(self, U):
        """
        For each cell, solve the local problem A_e x_e = b_e
        where A_e and b_e are assembled locally.
        """
        self._solve_local(U)
    
    def solve_global_rhs(self, U):
        """
        For each cell, solve the local problem A_e x_e = b_e
        where A_e is assembled locally and b_e is taken from 
        a globally assembled RHS vector.
        """
        b = assemble(self.L_form)
        self._solve_local(U, b)
    
    def _solve_local(self, U, b=None):
        V_trial = U.function_space()
        V_test = self.V_test
        assert V_trial.mesh() == V_test.mesh()
        mesh = V_trial.mesh()
        
        U_global = U.vector()
        L = self.L_form
        
        for cell in mesh.cells:
            dofs_trial = V_trial.dofmap().cell_dofs(cell.id)
            dofs_test = V_test.dofmap().cell_dofs(cell.id)
            N = len(dofs_test)
            M = len(dofs_trial)
            shape = (N, M)
            assert N == M, 'Local system is not square (%d, %d)' % shape
            
            # Get local LHS
            A_e = assemble_local(self.a_form, cell)
            assert A_e.shape == shape, 'Got shape %r expected %r' % (A_e.shape, shape)
            
            # Get local RHS
            if b is None:
                b_e = assemble_local(L, cell)
                assert b_e.shape == (N,)
            else:
                b_e = numpy.zeros(N, float)
                for i, iv in enumerate(dofs_test):
                    b_e[i] = b[iv]
            
            try:
                x_e = numpy.linalg.solve(A_e, b_e)
            except numpy.linalg.LinAlgError:
                from phonyx.tests.helpers import print_nice_matrix
                print 'Local solve error with matrix'
                print_nice_matrix(A_e)
                raise
            
            for j, ju in enumerate(dofs_trial):
                U_global[ju] = x_e[j]
